#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ
#Область ОбработчикиСобытий

Процедура ПередЗаписью(Отказ, Замещение)
	
	Если ОбменДанными.Загрузка
	 Или Не ПроведениеБольничнаяАптека.ЭтоПроведениеДокумента(ДополнительныеСвойства)
	 Или Документы.РасчетСебестоимостиТоваров.ДвиженияЗаписываютсяРасчетомСебестоимости(ЭтотОбъект) Тогда
		Возврат;
	КонецЕсли;
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);
	
	Документы.РасчетСебестоимостиТоваров.СохранитьДвиженияСформированныеРасчетомСебестоимости(ЭтотОбъект, Замещение);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытий

#КонецЕсли