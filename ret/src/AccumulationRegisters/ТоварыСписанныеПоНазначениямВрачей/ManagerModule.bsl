#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЙ ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область СлужебныйПрограммныйИнтерфейс

#Область РасчетСебестоимостиТоваров

Функция ХозяйственныеОперацииКорректировки() Экспорт
	
	Операции = Новый Массив;
	Операции.Добавить(Перечисления.ХозяйственныеОперации.СписаниеТоваровПоНазначениюВрача);
	
	Возврат Операции;
	
КонецФункции

Процедура СформироватьДвижениеКорректировки(ПараметрыРасчета, Выборка) Экспорт
	
	ИмяРегистра = СоздатьНаборЗаписей().Метаданные().Имя;
	ЗаполняемыеПоля = Новый Структура;
	ЗаполняемыеПоля.Вставить("Период");
	ЗаполняемыеПоля.Вставить("АналитикаУчетаНоменклатуры");
	ЗаполняемыеПоля.Вставить("АналитикаОписанияОтбораНоменклатуры");
	ЗаполняемыеПоля.Вставить("ЕдиницаИзмерения");
	ЗаполняемыеПоля = ОбщегоНазначенияКлиентСервер.КлючиСтруктурыВСтроку(ЗаполняемыеПоля);
	
	Запись = Документы.РасчетСебестоимостиТоваров.ДобавитьЗаписьВТаблицуДвижений(ПараметрыРасчета, ИмяРегистра, Выборка, ЗаполняемыеПоля);
	Запись.НазначениеВрача    = Выборка.НазначениеВрача;
	Запись.АналитикаВидаУчета = Выборка.КорАналитикаВидаУчета;
	Запись.Стоимость          = Выборка.СтоимостьКорректировка + Выборка.СуммаДопРасходовКорректировка;
	
КонецПроцедуры

#КонецОбласти // РасчетСебестоимостиТоваров

#КонецОбласти // СлужебныйПрограммныйИнтерфейс

#КонецЕсли