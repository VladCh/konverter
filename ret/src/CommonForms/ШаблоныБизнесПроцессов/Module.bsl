
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	ШаблонСписка = "
	|<ГруппаФормы Имя='СтраницаСписка%1' Родитель='Шаблоны'>
	|		<Свойство Имя='Вид'>Страница</Свойство>
	|		<ТаблицаФормы Имя='Список%1'>
	|			<Свойство Имя='ПутьКДанным'>Список%1</Свойство>
	|			<Свойство Имя='ПоложениеСтрокиПоиска'>Нет</Свойство>
	|			<Свойство Имя='ПоложениеСостоянияПросмотра'>Нет</Свойство>
	|			<Свойство Имя='ПоложениеУправленияПоиском'>Нет</Свойство>
	|			<ПолеФормы Имя='Список%1Наименование'>
	|				<Свойство Имя='Вид'>ПолеВвода</Свойство>
	|				<Свойство Имя='ПутьКДанным'>Список%1.Наименование</Свойство>
	|			</ПолеФормы>
	|		</ТаблицаФормы>
	|	</ГруппаФормы>
	|";
	
	ТекстовоеОписаниеЭлементов = "
	|<Форма>
	|	<Элементы>
	|";
	
	НовыеРеквизиты = Новый Массив;
	Шаблоны = Новый Соответствие;
	
	Для Каждого БизнесПроцесс Из Метаданные.БизнесПроцессы Цикл
		Если Не (ОбщегоНазначения.ЕстьРеквизитОбъекта("Шаблон", БизнесПроцесс)
		   И ОбщегоНазначения.ОбъектМетаданныхДоступенПоФункциональнымОпциям(БизнесПроцесс)) Тогда
			Продолжить
		КонецЕсли;
		
		Шаблон = Метаданные.НайтиПоТипу(БизнесПроцесс.Реквизиты.Шаблон.Тип.Типы()[0]);
		ИмяШаблона = Шаблон.Имя;
		
		Строка = БизнесПроцессыСШаблонами.Добавить();
		Строка.ИдентификаторБизнесПроцесса = ОбщегоНазначения.ИдентификаторОбъектаМетаданных(БизнесПроцесс.ПолноеИмя());
		Строка.Наименование = БизнесПроцесс.Синоним;
		Строка.ТипШаблона = ИмяШаблона;
		
		Если Шаблоны.Получить(ИмяШаблона) <> Неопределено Тогда
			Продолжить;
		КонецЕсли;
		
		Шаблоны.Вставить(ИмяШаблона, Шаблон);
		
		НовыеРеквизиты.Добавить(Новый РеквизитФормы("Список" + ИмяШаблона, Новый ОписаниеТипов("ДинамическийСписок")));
		
		ТекстовоеОписаниеЭлементов = ТекстовоеОписаниеЭлементов + СтрШаблон(ШаблонСписка, ИмяШаблона);
		
	КонецЦикла;
	
	ТекстовоеОписаниеЭлементов = ТекстовоеОписаниеЭлементов
	+ "
	|	</Элементы>
	|</Форма>
	|";
	
	ИзменитьРеквизиты(НовыеРеквизиты);
	Для Каждого Шаблон Из Шаблоны Цикл
		
		Список = ЭтотОбъект["Список" + Шаблон.Ключ];
		Список.ОсновнаяТаблица = Шаблон.Значение.ПолноеИмя();
		
	КонецЦикла;
	
	УправляемаяФорма.СоздатьЭлементы(ЭтотОбъект, УправляемаяФорма.ПрочитатьОписаниеФормыИзСтроки(ТекстовоеОписаниеЭлементов));
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ
#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура БизнесПроцессыПриАктивизацииСтроки(Элемент)
	
	ТекущаяСтрока = Элементы.БизнесПроцессы.ТекущиеДанные;
	ТипШаблона = ТекущаяСтрока.ТипШаблона;
	
	Элементы.Шаблоны.ТекущаяСтраница = Элементы["СтраницаСписка" + ТипШаблона];
	Если ТипШаблона = "ШаблоныСоставныхБизнесПроцессов" Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(ЭтотОбъект["Список" + ТипШаблона], "ТипШаблона", ТекущаяСтрока.ИдентификаторБизнесПроцесса);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийЭлементовФормы
