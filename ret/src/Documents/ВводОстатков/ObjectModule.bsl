#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ
#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, ТекстЗаполнения, СтандартнаяОбработка)
	
	ТипДанныхЗаполнения = ТипЗнч(ДанныеЗаполнения);
	Если ТипДанныхЗаполнения = Тип("Структура") Тогда
		ЗаполнитьЗначенияСвойств(ЭтотОбъект, ДанныеЗаполнения);
	КонецЕсли;
	
	ИнициализироватьДокумент();
	
	Если ТипОперации = Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваров
	 Или ТипОперации = Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваровВОтделениях Тогда
		ОбщегоНазначенияБольничнаяАптека.ЗаполнитьРеквизитыПоСкладу(ЭтотОбъект);
	КонецЕсли;
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	
	ИнициализироватьДокумент();
	
	Если ТипОперации = Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваров
	 Или ТипОперации = Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваровВОтделениях Тогда
		ОбщегоНазначенияБольничнаяАптека.ЗаполнитьРеквизитыПоСкладу(ЭтотОбъект);
		ЗапасыСервер.ЗаполнитьСтатусыУчетаНоменклатуры(ЭтотОбъект, ЗапасыСервер.ПолучитьПараметрыУчетаНоменклатуры(ЭтотОбъект));
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	НепроверяемыеРеквизиты = Новый Массив;
	
	Если ТипОперации <> Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваров
	   И ТипОперации <> Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваровВОтделениях Тогда
		НепроверяемыеРеквизиты.Добавить("Склад");
		НепроверяемыеРеквизиты.Добавить("ИсточникФинансирования");
		НепроверяемыеРеквизиты.Добавить("Товары");
	КонецЕсли;
	
	Если ТипОперации <> Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваров Тогда
		НепроверяемыеРеквизиты.Добавить("Подразделение");
	КонецЕсли;
	
	Если ТипОперации <> Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваровВОтделениях Тогда
		НепроверяемыеРеквизиты.Добавить("Отделение");
	КонецЕсли;
	
	Если ТипОперации <> Перечисления.ТипыОперацийВводаОстатков.ОстаткиВАвтономныхКассахККМПоРозничнойВыручке Тогда
		НепроверяемыеРеквизиты.Добавить("КассыККМ");
	КонецЕсли;
	
	Если ТипОперации = Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваров
	 Или ТипОперации = Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваровВОтделениях Тогда
		
		ПараметрыУчетаНоменклатуры = ЗапасыСервер.ПолучитьПараметрыУчетаНоменклатуры(ЭтотОбъект);
		ЗапасыСервер.ПроверитьЗаполнениеСерийНоменклатуры(ЭтотОбъект, ПараметрыУчетаНоменклатуры, НепроверяемыеРеквизиты, Отказ);
		ОбработкаТабличнойЧастиСервер.ПроверитьЗаполнениеКоличества(ЭтотОбъект, НепроверяемыеРеквизиты, Отказ);
		
	КонецЕсли;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, НепроверяемыеРеквизиты);
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);
	
	ДополнительныеСвойства.Вставить("ЭтоНовый", ЭтоНовый());
	ДополнительныеСвойства.Вставить("РежимЗаписи", РежимЗаписи);
	
	ОбработкаТабличнойЧастиСервер.ОкруглитьКоличествоШтучныхТоваров(ЭтотОбъект, РежимЗаписи);
	
	ЗапасыСервер.ОчиститьНеиспользуемыеРеквизиты(ЭтотОбъект, ЗапасыСервер.ПолучитьПараметрыУчетаНоменклатуры(ЭтотОбъект));
	
	Если РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		Справочники.ПартииНоменклатуры.ЗаполнитьПартиюВКоллекции(ЭтотОбъект, Контрагент, Новый Структура("ЗаменятьВыбраннуюПартию", Ложь));
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	ПроведениеБольничнаяАптека.СформироватьДвиженияПоРегистрам(ЭтотОбъект, Отказ, РежимПроведения);
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	
	ПроведениеБольничнаяАптека.СформироватьДвиженияПоРегистрам(ЭтотОбъект, Отказ);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытий

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

////////////////////////////////////////////////////////////////////////////////
// Инициализация и заполнение
#Область ИнициализацияИЗаполнение

Процедура ИнициализироватьДокумент()
	
	Автор = Пользователи.ТекущийПользователь();
	Ответственный = Пользователи.ТекущийПользователь();
	ЦенаВключаетНДС = Истина;
	
	ЗаполнитьПоляПоУмолчанию();
	
КонецПроцедуры

Процедура ЗаполнитьПоляПоУмолчанию()
	
	Организация = ЗначениеНастроекБольничнаяАптекаПовтИсп.ПолучитьОрганизациюПоУмолчанию(Организация);
	
КонецПроцедуры

#КонецОбласти // ИнициализацияИЗаполнение

////////////////////////////////////////////////////////////////////////////////
// Прочее
#Область Прочее

// Возвращает список регистров для контроля при записи движений
//
Функция СписокРегистровДляКонтроля() Экспорт
	
	РегистрыДляКонтроля = Новый Массив;
	
	Если Не ДополнительныеСвойства.ЭтоНовый Тогда
		// Приходы в регистр (сторно расхода из регистра) контролируем при перепроведении и отмене проведения
		РегистрыДляКонтроля.Добавить(Движения.СебестоимостьТоваров);
	КонецЕсли;
	
	Возврат РегистрыДляКонтроля;
	
КонецФункции

#КонецОбласти // Прочее

#КонецОбласти // СлужебныеПроцедурыИФункции

#КонецЕсли