#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ
#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, ТекстЗаполнения, СтандартнаяОбработка)
	
	ТипДанныхЗаполнения = ТипЗнч(ДанныеЗаполнения);
	
	Если ТипДанныхЗаполнения = Тип("Структура") Тогда
		ЗаполнитьДокументПоОтбору(ДанныеЗаполнения);
	ИначеЕсли ТипДанныхЗаполнения = Тип("ДокументСсылка.ЗаказПоставщику") Тогда
		ЗаполнитьДокументНаОснованииЗаказаПоставщику(ДанныеЗаполнения);
	ИначеЕсли ТипДанныхЗаполнения = Тип("ДокументСсылка.СпецификацияКДоговору") Тогда
		ЗаполнитьДокументНаОснованииСпецификацииКДоговору(ДанныеЗаполнения);
	ИначеЕсли ТипДанныхЗаполнения = Тип("ДокументСсылка.УведомлениеОПриемкеМДЛП") Тогда
		ЗаполнитьДокументНаОснованииУведомленияОПриемке(ДанныеЗаполнения);
	КонецЕсли;
	
	ИнициализироватьДокумент();
	
	ЗаполнитьПоЗначениямАвтозаполнения();
	
	ОбщегоНазначенияБольничнаяАптека.ЗаполнитьРеквизитыПоСкладу(ЭтотОбъект);
	
	Если Товары.Количество() > 0 Тогда
		ЗапасыСервер.ЗаполнитьСтатусыУчетаНоменклатуры(ЭтотОбъект, ЗапасыСервер.ПолучитьПараметрыУчетаНоменклатуры(ЭтотОбъект));
	КонецЕсли;
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	
	Согласован = Ложь;
	
	ДатаВходящегоДокумента = Дата(1, 1, 1);
	НомерВходящегоДокумента = "";
	ПредъявленСчетФактура = Ложь;
	ДатаВходящегоСчетаФактуры = Дата(1, 1, 1);
	НомерВходящегоСчетаФактуры = "";
	
	ПоступлениеПоЗаказу = Ложь;
	ЗаказПоставщику = Неопределено;
	Для Каждого ТекущаяСтрока Из Товары Цикл
		ТекущаяСтрока.КодСтроки = 0;
		ТекущаяСтрока.НоменклатураЗаказа = Неопределено;
	КонецЦикла;
	
	ИнициализироватьДокумент();
	
	ОбщегоНазначенияБольничнаяАптека.ЗаполнитьРеквизитыПоСкладу(ЭтотОбъект);
	
	Если Товары.Количество() > 0 Тогда
		ЗапасыСервер.ЗаполнитьСтатусыУчетаНоменклатуры(ЭтотОбъект, ЗапасыСервер.ПолучитьПараметрыУчетаНоменклатуры(ЭтотОбъект));
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	НепроверяемыеРеквизиты = Новый Массив;
	
	ОбработкаТабличнойЧастиСервер.ПроверитьЗаполнениеКоличества(ЭтотОбъект, НепроверяемыеРеквизиты, Отказ);
	
	ВсеРеквизиты = Неопределено;
	РеквизитыОперации = Неопределено;
	Документы.ПоступлениеТоваров.ЗаполнитьИменаРеквизитовПоТипуОперации(ХозяйственнаяОперация, ВсеРеквизиты, РеквизитыОперации);
	ОбщегоНазначенияБольничнаяАптекаКлиентСервер.ЗаполнитьНепроверяемыеРеквизиты(НепроверяемыеРеквизиты, ВсеРеквизиты, РеквизитыОперации);
	
	ОперацияНеОблагаетсяНДС = ЭтоОперацияБезвозмездногоПоступления(ХозяйственнаяОперация)
		Или ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ЗакупкаЧерезПодотчетноеЛицо;
	Если Не ПредъявленСчетФактура Или ОперацияНеОблагаетсяНДС Тогда
		НепроверяемыеРеквизиты.Добавить("НомерВходящегоСчетаФактуры");
		НепроверяемыеРеквизиты.Добавить("ДатаВходящегоСчетаФактуры");
	КонецЕсли;
	
	Если Не ПоступлениеПоЗаказу Тогда
		НепроверяемыеРеквизиты.Добавить("ЗаказПоставщику");
	КонецЕсли;
	
	ПараметрыУчетаНоменклатуры = ЗапасыСервер.ПолучитьПараметрыУчетаНоменклатуры(ЭтотОбъект);
	ЗапасыСервер.ПроверитьЗаполнениеСерийНоменклатуры(ЭтотОбъект, ПараметрыУчетаНоменклатуры, НепроверяемыеРеквизиты, Отказ);
	
	ЗакупкиСервер.ПроверитьКорректностьЗаполненияНоменклатурыПоставщика(ЭтотОбъект, Отказ);
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, НепроверяемыеРеквизиты);
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);
	
	ПроведениеБольничнаяАптека.УстановитьРежимПроведения(ЭтотОбъект, РежимЗаписи, РежимПроведения);
	
	ДополнительныеСвойства.Вставить("ЭтоНовый", ЭтоНовый());
	ДополнительныеСвойства.Вставить("РежимЗаписи", РежимЗаписи);
	
	ОбработкаТабличнойЧастиСервер.ОкруглитьКоличествоШтучныхТоваров(ЭтотОбъект, РежимЗаписи);
	
	СуммаДокумента = ЦенообразованиеБольничнаяАптекаКлиентСервер.ПолучитьСуммуДокумента(Товары, ЦенаВключаетНДС);
	
	ОбщегоНазначенияБольничнаяАптека.ИзменитьПризнакСогласованностиДокумента(
		ЭтотОбъект,
		РежимЗаписи);
	
	ЗапасыСервер.ОчиститьНеиспользуемыеРеквизиты(ЭтотОбъект, ЗапасыСервер.ПолучитьПараметрыУчетаНоменклатуры(ЭтотОбъект));
	
	ВсеРеквизиты = Неопределено;
	РеквизитыОперации = Неопределено;
	Документы.ПоступлениеТоваров.ЗаполнитьИменаРеквизитовПоТипуОперации(ХозяйственнаяОперация, ВсеРеквизиты, РеквизитыОперации);
	ОбщегоНазначенияБольничнаяАптека.ОчиститьНеиспользуемыеРеквизиты(ЭтотОбъект, ВсеРеквизиты, РеквизитыОперации);
	
	ОперацияНеОблагаетсяНДС = ЭтоОперацияБезвозмездногоПоступления(ХозяйственнаяОперация)
		Или ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ЗакупкаЧерезПодотчетноеЛицо;
	Если ОперацияНеОблагаетсяНДС Тогда
		НалогообложениеНДС = Перечисления.ТипыНалогообложенияНДС.ПродажаНеОблагаетсяНДС;
	КонецЕсли;
	
	Если РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		Справочники.ПартииНоменклатуры.ЗаполнитьПартиюВКоллекции(ЭтотОбъект, Контрагент);
		ЗакупкиСервер.СвязатьНоменклатуруСНоменклатуройПоставщика(Товары, Отказ);
	КонецЕсли;
	
	ЗаполнитьСписокЗависимыхЗаказов();
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	ПроведениеБольничнаяАптека.СформироватьДвиженияПоРегистрам(ЭтотОбъект, Отказ, РежимПроведения);
	
	РегистрыСведений.СостоянияЗаказовПоставщикам.ОтразитьСостояниеЗаказа(ДополнительныеСвойства.ЗависимыеЗаказы);
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	
	ПроведениеБольничнаяАптека.СформироватьДвиженияПоРегистрам(ЭтотОбъект, Отказ);
	
	РегистрыСведений.СостоянияЗаказовПоставщикам.ОтразитьСостояниеЗаказа(ДополнительныеСвойства.ЗависимыеЗаказы);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытий

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

////////////////////////////////////////////////////////////////////////////////
// Инициализация и заполнение документа
#Область ИнициализацияИЗаполнение

Процедура ИнициализироватьДокумент()
	
	Автор = Пользователи.ТекущийПользователь();
	Ответственный = Пользователи.ТекущийПользователь();
	
	Если Не ЗначениеЗаполнено(Валюта) Тогда
		Валюта = ЗначениеНастроекБольничнаяАптекаПовтИсп.ПолучитьВалютуРегламентированногоУчета();
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(ВалютаВзаиморасчетов) Тогда
		ВалютаВзаиморасчетов = Валюта;
	КонецЕсли;
	
	ЗаполнитьПоляПоУмолчанию();
	
КонецПроцедуры

Процедура ЗаполнитьПоляПоУмолчанию()
	
	Организация = ЗначениеНастроекБольничнаяАптекаПовтИсп.ПолучитьОрганизациюПоУмолчанию(Организация);
	ПодразделениеОрганизации = ЗначениеНастроекБольничнаяАптекаПовтИсп.ПолучитьПодразделениеПоУмолчанию(ПодразделениеОрганизации, Организация);
	Склад = ЗначениеНастроекБольничнаяАптекаПовтИсп.ПолучитьСкладПоУмолчанию(Склад, ПодразделениеОрганизации);
	
КонецПроцедуры

Процедура ЗаполнитьПоЗначениямАвтозаполнения()
	
	ОбщегоНазначенияБольничнаяАптека.ЗаполнитьПоЗначениямАвтозаполнения(ЭтотОбъект, Неопределено, "Организация, Склад");
	ОбщегоНазначенияБольничнаяАптека.ЗаполнитьПоЗначениямАвтозаполнения(ЭтотОбъект, Неопределено, "ПодразделениеОрганизации", "Организация");
	
КонецПроцедуры

Процедура ЗаполнитьДокументПоОтбору(ДанныеЗаполнения)
	
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, ДанныеЗаполнения);
	
	ОбщегоНазначенияБольничнаяАптека.ПроверитьЗаполнениеДоговораКонтрагента(ЭтотОбъект);
	ОбщегоНазначенияБольничнаяАптека.ПроверитьЗаполнениеПодразделенияОрганизации(ЭтотОбъект);
	
	Если ЗначениеЗаполнено(ДоговорКонтрагента) Тогда
		Валюта = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ДоговорКонтрагента, "ВалютаВзаиморасчетов");
		ВалютаВзаиморасчетов = Валюта;
	КонецЕсли;
	
КонецПроцедуры

Процедура ЗаполнитьДокументНаОснованииЗаказаПоставщику(ДокументОснование)
	
	Запрос = Новый Запрос;
	Запрос.Текст = "
	|ВЫБРАТЬ
	|	ДанныеДокумента.Ссылка                                   КАК ЗаказПоставщику,
	|	ДанныеДокумента.Организация                              КАК Организация,
	|	ДанныеДокумента.ПодразделениеОрганизации                 КАК ПодразделениеОрганизации,
	|	ДанныеДокумента.Склад                                    КАК Склад,
	|	ДанныеДокумента.Партнер                                  КАК Партнер,
	|	ДанныеДокумента.Контрагент                               КАК Контрагент,
	|	ДанныеДокумента.ДоговорКонтрагента                       КАК ДоговорКонтрагента,
	|	ДанныеДокумента.ДоговорКонтрагента.ВалютаВзаиморасчетов  КАК ВалютаВзаиморасчетов,
	|	ДанныеДокумента.Валюта                                   КАК Валюта,
	|	ДанныеДокумента.ЦенаВключаетНДС                          КАК ЦенаВключаетНДС,
	|	ДанныеДокумента.ИсточникФинансирования                   КАК ИсточникФинансирования,
	|	ДанныеДокумента.НалогообложениеНДС                       КАК НалогообложениеНДС,
	|	НЕ ДанныеДокумента.Проведен                              КАК ЕстьОшибкиПроведен,
	|	ВЫБОР
	|		КОГДА ДанныеДокумента.Статус В (&ДопустимыеСтатусы)
	|				ИЛИ ДанныеДокумента.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыЗаказовПоставщикам.Закрыт)
	|			ТОГДА ЛОЖЬ
	|		ИНАЧЕ ИСТИНА
	|	КОНЕЦ                                                    КАК ЕстьОшибкиСтатус,
	|	ДанныеДокумента.Статус                                   КАК Статус
	|ИЗ
	|	Документ.ЗаказПоставщику КАК ДанныеДокумента
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &ДокументОснование
	|";
	
	ДопустимыеСтатусы = Новый Массив();
	ДопустимыеСтатусы.Добавить(Перечисления.СтатусыЗаказовПоставщикам.КПоступлению);
	
	Запрос.УстановитьПараметр("ДопустимыеСтатусы", ДопустимыеСтатусы);
	Запрос.УстановитьПараметр("ДокументОснование", ДокументОснование);
	
	РеквизитыЗаказа = Запрос.Выполнить().Выбрать();
	РеквизитыЗаказа.Следующий();
	
	ОбщегоНазначенияБольничнаяАптека.ПроверитьВозможностьВводаНаОсновании(
		РеквизитыЗаказа.ЗаказПоставщику,
		РеквизитыЗаказа.ЕстьОшибкиПроведен,
		РеквизитыЗаказа.Статус,
		РеквизитыЗаказа.ЕстьОшибкиСтатус,
		ДопустимыеСтатусы);
	
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, РеквизитыЗаказа);
	ПоступлениеПоЗаказу = Истина;
	
	ЗаполнитьПоОстаткамЗаказа();
	
КонецПроцедуры

Процедура ЗаполнитьПоОстаткамЗаказа() Экспорт
	
	Менеджер = ОбщегоНазначения.МенеджерОбъектаПоСсылке(ЗаказПоставщику);
	РезультатЗапроса = Менеджер.ПолучитьРезультатЗапросаПоОстаткам(ЗаказПоставщику, Ссылка);
	
	Если РезультатЗапроса.Пустой() Тогда
		
		ТекстСообщения = НСтр("ru = 'Нет данных для заполнения по документу ""%Спецификация%"" .'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Спецификация%", ЗаказПоставщику);
		ВызватьИсключение ТекстСообщения;
		
	КонецЕсли;
	
	ТаблицаКурсовВалют = Новый ТаблицаЗначений;
	ТаблицаКурсовВалют.Колонки.Добавить("Валюта",    Новый ОписаниеТипов("СправочникСсылка.Валюты"));
	ТаблицаКурсовВалют.Колонки.Добавить("Дата",      Новый ОписаниеТипов("Дата"));
	ТаблицаКурсовВалют.Колонки.Добавить("Курс",      Новый ОписаниеТипов("Число"));
	ТаблицаКурсовВалют.Колонки.Добавить("Кратность", Новый ОписаниеТипов("Число"));
	
	КурсНовойВалюты = РаботаСКурсамиВалют.ПолучитьКурсВалюты(Валюта, Дата);
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	ДействияПересчетСумм = Новый Структура;
	ДействияПересчетСумм.Вставить(Действия.Действие_ПересчитатьСумму());
	ПараметрыПересчетаСуммы = Действия.ПолучитьПараметрыПересчетаСуммыНДС(ЭтотОбъект);
	ДействияПересчетСумм.Вставить(Действия.Действие_ПересчитатьСуммуНДС(), ПараметрыПересчетаСуммы);
	ДействияПересчетСумм.Вставить(Действия.Действие_ПересчитатьСуммуСНДС(), ПараметрыПересчетаСуммы);
	
	КэшированныеЗначения = ОбработкаТабличнойЧастиКлиентСервер.ПолучитьСтруктуруКэшированныхЗначений();
	
	Выборка = РезультатЗапроса.Выбрать();
	Пока Выборка.Следующий() Цикл
		
		СтрокаТовара = Товары.Добавить();
		ЗаполнитьЗначенияСвойств(СтрокаТовара, Выборка);
		
		НеобходимПересчетСумм = (Выборка.Количество <> Выборка.КоличествоПоДокументу);
		
		Если Выборка.ЦенаВключаетНДС <> ЦенаВключаетНДС Тогда
			Если Выборка.ЦенаВключаетНДС Тогда
				СтрокаТовара.Сумма = Выборка.СуммаСНДС - Выборка.СуммаНДС;
			Иначе
				СтрокаТовара.Сумма = Выборка.СуммаСНДС
			КонецЕсли;
			СтрокаТовара.Цена = СтрокаТовара.Сумма / Выборка.КоличествоВЕдиницахИзмеренияПоДокументу;
		КонецЕсли;
		
		Если Выборка.Валюта <> Валюта Тогда
			
			НеобходимПересчетСумм = Истина;
			ПараметрыОтбора = Новый Структура("Валюта, Дата", Выборка.Валюта, НачалоДня(Выборка.Дата));
			КурсыВалюты = ТаблицаКурсовВалют.НайтиСтроки(ПараметрыОтбора);
			
			Если КурсыВалюты.Количество() = 0 Тогда
				КурсВалюты = ТаблицаКурсовВалют.Добавить();
				КурсВалюты.Валюта = Выборка.Валюта;
				КурсВалюты.Дата = НачалоДня(Выборка.Дата);
				
				ПараметрыКурса = РаботаСКурсамиВалют.ПолучитьКурсВалюты(Выборка.Валюта, Выборка.Дата);
				КурсВалюты.Курс = ПараметрыКурса.Курс;
				КурсВалюты.Кратность = ПараметрыКурса.Кратность;
			Иначе
				КурсВалюты = КурсыВалюты[0];
				
			КонецЕсли;
			
			СтрокаТовара.Цена = РаботаСКурсамиВалютКлиентСервер.ПересчитатьПоКурсу(СтрокаТовара.Цена, КурсВалюты, КурсНовойВалюты);
			
		КонецЕсли;
		
		Если НеобходимПересчетСумм Тогда
			ОбработкаТабличнойЧастиСервер.ОбработатьСтрокуТабличнойЧасти(СтрокаТовара, ДействияПересчетСумм, КэшированныеЗначения);
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ЗаполнитьДокументНаОснованииСпецификацииКДоговору(ДокументОснование)
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	ДанныеДокумента.Ссылка                                   КАК ЗаказПоставщику,
	|	ДанныеДокумента.Организация                              КАК Организация,
	|	ДанныеДокумента.ПодразделениеОрганизации                 КАК ПодразделениеОрганизации,
	|	ДанныеДокумента.Партнер                                  КАК Партнер,
	|	ДанныеДокумента.Контрагент                               КАК Контрагент,
	|	ДанныеДокумента.ДоговорКонтрагента                       КАК ДоговорКонтрагента,
	|	ДанныеДокумента.ДоговорКонтрагента.ВалютаВзаиморасчетов  КАК ВалютаВзаиморасчетов,
	|	ДанныеДокумента.Валюта                                   КАК Валюта,
	|	ДанныеДокумента.ЦенаВключаетНДС                          КАК ЦенаВключаетНДС,
	|	ДанныеДокумента.ИсточникФинансирования                   КАК ИсточникФинансирования,
	|	ДанныеДокумента.НалогообложениеНДС                       КАК НалогообложениеНДС,
	|	НЕ ДанныеДокумента.Проведен                              КАК ЕстьОшибкиПроведен,
	|	ВЫБОР
	|		КОГДА ДанныеДокумента.Статус В (&ДопустимыеСтатусы)
	|				ИЛИ ДанныеДокумента.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыСпецификацийКДоговорам.Закрыт)
	|			ТОГДА ЛОЖЬ
	|		ИНАЧЕ ИСТИНА
	|	КОНЕЦ                                                    КАК ЕстьОшибкиСтатус,
	|	ДанныеДокумента.Статус                                   КАК Статус
	|ИЗ
	|	Документ.СпецификацияКДоговору КАК ДанныеДокумента
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &ДокументОснование
	|");
	
	Запрос.УстановитьПараметр("ДокументОснование", ДокументОснование);
	
	ДопустимыеСтатусы = Новый Массив();
	ДопустимыеСтатусы.Добавить(Перечисления.СтатусыСпецификацийКДоговорам.КВыполнению);
	Запрос.УстановитьПараметр("ДопустимыеСтатусы", ДопустимыеСтатусы);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	РеквизитыЗаказа = РезультатЗапроса.Выбрать();
	РеквизитыЗаказа.Следующий();
	
	ОбщегоНазначенияБольничнаяАптека.ПроверитьВозможностьВводаНаОсновании(
		РеквизитыЗаказа.ЗаказПоставщику,
		РеквизитыЗаказа.ЕстьОшибкиПроведен,
		РеквизитыЗаказа.Статус,
		РеквизитыЗаказа.ЕстьОшибкиСтатус,
		ДопустимыеСтатусы);
	
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, РеквизитыЗаказа);
	ПоступлениеПоЗаказу = Истина;
	
КонецПроцедуры

Процедура ЗаполнитьДокументНаОснованииУведомленияОПриемке(ДокументОснование)
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	ДанныеДокумента.Ссылка                                   КАК Основание,
	|	ДанныеДокумента.ИдентификаторОрганизации                 КАК ИдентификаторОрганизации,
	|	ДанныеДокумента.ИдентификаторКонтрагента                 КАК ИдентификаторКонтрагента,
	|	ДанныеДокумента.НомерДокумента                           КАК НомерВходящегоДокумента,
	|	ДанныеДокумента.ДатаДокумента                            КАК ДатаВходящегоДокумента,
	|	ИСТИНА                                                   КАК ЦенаВключаетНДС,
	|	НЕ ДанныеДокумента.Проведен                              КАК ЕстьОшибкиПроведен
	|ИЗ
	|	Документ.УведомлениеОПриемкеМДЛП КАК ДанныеДокумента
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &ДокументОснование
	|;
	|
	|//////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	УпаковкиТовара.ИдентификаторСтроки  КАК ИдентификаторСтроки,
	|	КОЛИЧЕСТВО(1)                       КАК Количество
	|ПОМЕСТИТЬ  ПринятыеУпаковкиТовара
	|ИЗ
	|	Документ.УведомлениеОПриемкеМДЛП.НомераУпаковок КАК УпаковкиТовара
	|ГДЕ
	|	УпаковкиТовара.Ссылка = &ДокументОснование
	|	И НЕ УпаковкиТовара.СостояниеПодтверждения В (
	|		ЗНАЧЕНИЕ(Перечисление.СостоянияПодтвержденияМДЛП.ОтозваноПоставщиком),
	|		ЗНАЧЕНИЕ(Перечисление.СостоянияПодтвержденияМДЛП.ОтклоненоПокупателем),
	|		ЗНАЧЕНИЕ(Перечисление.СостоянияПодтвержденияМДЛП.НеТребуется)
	|	)
	|СГРУППИРОВАТЬ ПО
	|	УпаковкиТовара.ИдентификаторСтроки
	|;
	|
	|//////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаТовары.ИдентификаторСтроки     КАК ИдентификаторСтроки,
	|	ТаблицаТовары.Номенклатура            КАК Номенклатура,
	|	ТаблицаТовары.Серия                   КАК СерияНоменклатуры,
	|	ПринятыеУпаковкиТовара.Количество     КАК КоличествоВЕдиницахИзмерения,
	|	ТаблицаТовары.Количество              КАК КоличествоУпаковокПоДокументу,
	|	ТаблицаТовары.Номенклатура.Упаковка   КАК ЕдиницаИзмерения,
	|	КоэффициентыУпаковок.Коэффициент      КАК Коэффициент,
	|	ТаблицаТовары.Количество * КоэффициентыУпаковок.Коэффициент  КАК Количество,
	|	ТаблицаТовары.Цена                    КАК Цена,
	|	ТаблицаТовары.Сумма                   КАК Сумма,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СуммаНДС = 0
	|			ТОГДА ЗНАЧЕНИЕ(Перечисление.СтавкиНДС.БезНДС)
	|		ИНАЧЕ ТаблицаТовары.Номенклатура.СтавкаНДС
	|	КОНЕЦ                                 КАК СтавкаНДС,
	|	ТаблицаТовары.СуммаНДС                КАК СуммаНДС,
	|	ТаблицаТовары.Сумма                   КАК СуммаСНДС
	|ИЗ
	|	Документ.УведомлениеОПриемкеМДЛП.Товары КАК ТаблицаТовары
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		РегистрСведений.ЕдиницыИзмеренияНоменклатуры КАК КоэффициентыУпаковок
	|	ПО
	|		ТаблицаТовары.Номенклатура = КоэффициентыУпаковок.Номенклатура
	|		И ТаблицаТовары.Номенклатура.Упаковка = КоэффициентыУпаковок.ЕдиницаИзмерения
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		ПринятыеУпаковкиТовара КАК ПринятыеУпаковкиТовара
	|	ПО
	|		ТаблицаТовары.ИдентификаторСтроки = ПринятыеУпаковкиТовара.ИдентификаторСтроки
	|ГДЕ
	|	ТаблицаТовары.Ссылка = &ДокументОснование
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ТаблицаТовары.ИдентификаторСтроки          КАК ИдентификаторСтроки,
	|	ТаблицаТовары.Номенклатура                 КАК Номенклатура,
	|	ТаблицаТовары.Серия                        КАК СерияНоменклатуры,
	|	ТаблицаТовары.Количество                   КАК КоличествоВЕдиницахИзмерения,
	|	ТаблицаТовары.Количество                   КАК КоличествоУпаковокПоДокументу,
	|	ТаблицаТовары.Номенклатура.Упаковка        КАК ЕдиницаИзмерения,
	|	КоэффициентыУпаковок.Коэффициент           КАК Коэффициент,
	|	ТаблицаТовары.Количество * КоэффициентыУпаковок.Коэффициент  КАК Количество,
	|	ТаблицаТовары.Цена                         КАК Цена,
	|	ТаблицаТовары.Сумма                        КАК Сумма,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СуммаНДС = 0
	|			ТОГДА ЗНАЧЕНИЕ(Перечисление.СтавкиНДС.БезНДС)
	|		ИНАЧЕ ТаблицаТовары.Номенклатура.СтавкаНДС
	|	КОНЕЦ                                      КАК СтавкаНДС,
	|	ТаблицаТовары.СуммаНДС * ТаблицаТовары.Количество  КАК СуммаНДС,
	|	ТаблицаТовары.Сумма                        КАК СуммаСНДС
	|ИЗ
	|	Документ.УведомлениеОПриемкеМДЛП.СоставТранспортныхУпаковок КАК ТаблицаТовары
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		РегистрСведений.ЕдиницыИзмеренияНоменклатуры КАК КоэффициентыУпаковок
	|	ПО
	|		ТаблицаТовары.Номенклатура = КоэффициентыУпаковок.Номенклатура
	|		И ТаблицаТовары.Номенклатура.Упаковка = КоэффициентыУпаковок.ЕдиницаИзмерения
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		Документ.УведомлениеОПриемкеМДЛП.ТранспортныеУпаковки КАК ПринятыеУпаковкиТовара
	|	ПО
	|		ТаблицаТовары.Ссылка = ПринятыеУпаковкиТовара.Ссылка
	|		И ТаблицаТовары.ИдентификаторСтроки = ПринятыеУпаковкиТовара.ИдентификаторСтроки
	|		И НЕ ПринятыеУпаковкиТовара.СостояниеПодтверждения В (
	|			ЗНАЧЕНИЕ(Перечисление.СостоянияПодтвержденияМДЛП.ОтозваноПоставщиком),
	|			ЗНАЧЕНИЕ(Перечисление.СостоянияПодтвержденияМДЛП.ОтклоненоПокупателем),
	|			ЗНАЧЕНИЕ(Перечисление.СостоянияПодтвержденияМДЛП.НеТребуется)
	|		)
	|ГДЕ
	|	ТаблицаТовары.Ссылка = &ДокументОснование
	|");
	
	Запрос.УстановитьПараметр("ДокументОснование", ДокументОснование);
	РезультатЗапроса = Запрос.ВыполнитьПакет();
	
	РеквизитыШапки = РезультатЗапроса[0].Выбрать();
	РеквизитыШапки.Следующий();
	
	ОбщегоНазначенияБольничнаяАптека.ПроверитьВозможностьВводаНаОсновании(РеквизитыШапки.Основание, РеквизитыШапки.ЕстьОшибкиПроведен);
	
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, РеквизитыШапки);
	
	Субъект = ИнтеграцияМДЛПБольничнаяАптека.СубъектОбращенияПоИдентификатору(РеквизитыШапки.ИдентификаторОрганизации, Истина);
	Организация = Субъект.Организация;
	Склад       = Субъект.МестоДеятельности;
	
	Субъект = ИнтеграцияМДЛПБольничнаяАптека.СубъектОбращенияПоИдентификатору(РеквизитыШапки.ИдентификаторКонтрагента, Ложь);
	Контрагент = Субъект.Организация;
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	ДействияПересчетСумм = Новый Структура;
	ДействияПересчетСумм.Вставить(Действия.Действие_ПересчитатьСумму());
	ПараметрыПересчетаСуммы = Действия.ПолучитьПараметрыПересчетаСуммыНДС(ЭтотОбъект);
	ДействияПересчетСумм.Вставить(Действия.Действие_ПересчитатьСуммуНДС(), ПараметрыПересчетаСуммы);
	ДействияПересчетСумм.Вставить(Действия.Действие_ПересчитатьСуммуСНДС(), ПараметрыПересчетаСуммы);
	
	КэшированныеЗначения = Неопределено;
	
	ВыборкаТовары = РезультатЗапроса[РезультатЗапроса.ВГраница()].Выбрать();
	Пока ВыборкаТовары.Следующий() Цикл
		
		СтрокаТовара = Товары.Добавить();
		ЗаполнитьЗначенияСвойств(СтрокаТовара, ВыборкаТовары);
		
		НеобходимПересчетСумм = (ВыборкаТовары.КоличествоВЕдиницахИзмерения <> ВыборкаТовары.КоличествоУпаковокПоДокументу);
		
		Если НеобходимПересчетСумм Тогда
			ОбработкаТабличнойЧастиСервер.ОбработатьСтрокуТабличнойЧасти(СтрокаТовара, ДействияПересчетСумм, КэшированныеЗначения);
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти // ИнициализацияИЗаполнение

////////////////////////////////////////////////////////////////////////////////
// Прочее
#Область Прочее

Функция СписокРегистровДляКонтроля() Экспорт
	
	РегистрыДляКонтроля = Новый Массив;
	
	Если Не ДополнительныеСвойства.ЭтоНовый Тогда
		// Приходы в регистр (сторно расхода из регистра) контролируем при перепроведении и отмене проведения
		РегистрыДляКонтроля.Добавить(Движения.СебестоимостьТоваров);
	КонецЕсли;
	
	Если ДополнительныеСвойства.РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		РегистрыДляКонтроля.Добавить(Движения.ЗаказыПоставщикам);
	КонецЕсли;
	
	Возврат РегистрыДляКонтроля;
	
КонецФункции

Функция ЭтоОперацияБезвозмездногоПоступления(Операция)
	
	БезвозмездноеПоступление = Новый Массив;
	БезвозмездноеПоступление.Добавить(Перечисления.ХозяйственныеОперации.БезвозмездноеПоступлениеВнутриведомственное);
	БезвозмездноеПоступление.Добавить(Перечисления.ХозяйственныеОперации.БезвозмездноеПоступлениеМежбюджетное);
	БезвозмездноеПоступление.Добавить(Перечисления.ХозяйственныеОперации.БезвозмездноеПоступлениеПрочее);
	
	Возврат БезвозмездноеПоступление.Найти(Операция) <> Неопределено;
	
КонецФункции

Процедура ЗаполнитьСписокЗависимыхЗаказов()
	
	СписокЗаказов = Новый Массив;
	Если ПоступлениеПоЗаказу И ТипЗнч(ЗаказПоставщику) = Тип("ДокументСсылка.ЗаказПоставщику") Тогда
		СписокЗаказов.Добавить(ЗаказПоставщику);
	КонецЕсли;
	
	Если Не ЭтоНовый() Тогда
		РеквизитыДоЗаписи = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Ссылка, "ПоступлениеПоЗаказу, ЗаказПоставщику");
		Если РеквизитыДоЗаписи.ПоступлениеПоЗаказу И ТипЗнч(РеквизитыДоЗаписи.ЗаказПоставщику) = Тип("ДокументСсылка.ЗаказПоставщику") Тогда
			Если СписокЗаказов.Найти(РеквизитыДоЗаписи.ЗаказПоставщику) = Неопределено Тогда
				СписокЗаказов.Добавить(РеквизитыДоЗаписи.ЗаказПоставщику);
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	
	ДополнительныеСвойства.Вставить("ЗависимыеЗаказы", СписокЗаказов);
	
КонецПроцедуры

#КонецОбласти // Прочее

#КонецОбласти // СлужебныеПроцедурыИФункции

#КонецЕсли