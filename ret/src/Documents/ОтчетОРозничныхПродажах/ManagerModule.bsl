#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

// Имена реквизитов, от значений которых зависят параметры учета номенклатуры
//
// Возвращаемое значение:
//   Строка - имена реквизитов, перечисленные через запятую
//
Функция ИменаРеквизитовДляЗаполненияПараметровУчетаНоменклатуры() Экспорт
	
	Возврат "Склад";
	
КонецФункции

// Возвращает параметры учета для номенклатуры, указанной в документе
//
// Параметры
//   Объект - Структура - структура значений реквизитов объекта, необходимых для заполнения параметров указания серий
// Возвращаемое значение
//   Структура - Состав полей задается в функции НоменклатураКлиентСервер.ПараметрыУчетаНоменклатуры
//
Функция ПараметрыУчетаНоменклатуры(Объект) Экспорт
	
	ПараметрыУчета = ЗапасыСервер.ПараметрыУчетаНоменклатуры();
	ПараметрыУчета.ПолноеИмяОбъекта = ПустаяСсылка().Метаданные().ПолноеИмя();
	
	ПараметрыУчетаНаСкладе = СкладыСервер.ПараметрыУчетаНоменклатуры(Объект.Склад);
	ПараметрыУчета.ИспользоватьСерии = ПараметрыУчетаНаСкладе.ИспользоватьСерииНоменклатуры;
	ПараметрыУчета.ИспользоватьПартии = ПараметрыУчетаНаСкладе.ИспользоватьПартии;
	ПараметрыУчета.Склад = Объект.Склад;
	
	Возврат ПараметрыУчета;
	
КонецФункции

// Возвращает текст запроса для расчета статусов указания параметров учета номенклатуры
//
// Параметры
//   ПараметрыУчетаНоменклатуры - Структура - состав полей задается в функции ЗапасыСервер.ПараметрыУчетаНоменклатуры
//
// Возвращаемое значение
//   Строка - текст запроса
//
Функция ТекстЗапросаРасчетаСтатусовУчетаНоменклатуры(ПараметрыУчетаНоменклатуры) Экспорт
	
	Возврат ЗапасыСервер.ТекстЗапросаРасчетаСтатусовУчетаНоменклатуры(ПараметрыУчетаНоменклатуры);
	
КонецФункции

#КонецОбласти // ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

////////////////////////////////////////////////////////////////////////////////
// Проведение
#Область Проведение

// Инициализирует таблицы значений, содержащие данные для проведения документа.
// Таблицы значений сохраняет в свойствах структуры "ДополнительныеСвойства".
//
Процедура СформироватьТаблицыДвиженийДляПроведения(ДополнительныеСвойства, Регистры = Неопределено) Экспорт
	
	ОсновныеДанныеДокумента = ПодготовитьОсновныеДанныеДляПроведения(ДополнительныеСвойства);
	
	ИнициализироватьКлючиАналитикиВидаУчета(ОсновныеДанныеДокумента);
	ИнициализироватьКлючиАналитикиУчетаНоменклатуры(ОсновныеДанныеДокумента);
	
	ПроведениеБольничнаяАптека.ДобавитьТекстЗапросаДвижений(ДополнительныеСвойства, ТекстЗапросаВтТаблицаТовары());
	ПроведениеБольничнаяАптека.ДобавитьТекстЗапросаДвижений(ДополнительныеСвойства, ТекстЗапросаТоварыНаСкладах(), Метаданные.РегистрыНакопления.ТоварыНаСкладах);
	ПроведениеБольничнаяАптека.ДобавитьТекстЗапросаДвижений(ДополнительныеСвойства, ТекстЗапросаСвободныеОстатки(), Метаданные.РегистрыНакопления.СвободныеОстатки);
	ПроведениеБольничнаяАптека.ДобавитьТекстЗапросаДвижений(ДополнительныеСвойства, ТекстЗапросаВтАналитика());
	ПроведениеБольничнаяАптека.ДобавитьТекстЗапросаДвижений(ДополнительныеСвойства, ТекстЗапросаСебестоимостьТоваров(), Метаданные.РегистрыНакопления.СебестоимостьТоваров);
	ПроведениеБольничнаяАптека.ДобавитьТекстЗапросаДвижений(ДополнительныеСвойства, ТекстЗапросаВыручкаИСебестоимостьПродаж(), Метаданные.РегистрыНакопления.ВыручкаИСебестоимостьПродаж);
	ПроведениеБольничнаяАптека.ДобавитьТекстЗапросаДвижений(ДополнительныеСвойства, ТекстЗапросаВтСуммаПлатежнымиКартами());
	ПроведениеБольничнаяАптека.ДобавитьТекстЗапросаДвижений(ДополнительныеСвойства, ТекстЗапросаДенежныеСредстваВКассахККМ(), Метаданные.РегистрыНакопления.ДенежныеСредстваВКассахККМ);
	ПроведениеБольничнаяАптека.ДобавитьТекстЗапросаДвижений(ДополнительныеСвойства, ТекстЗапросаТоварыКОформлениюИзлишковНедостач(), Метаданные.РегистрыНакопления.ТоварыКОформлениюИзлишковНедостач);
	
	Запрос = Новый Запрос(ПроведениеБольничнаяАптека.ПолучитьТекстЗапросаДвижений(ДополнительныеСвойства, Регистры));
	
	Для Каждого ДанныеДокумента Из ОсновныеДанныеДокумента Цикл
		Запрос.УстановитьПараметр(ДанныеДокумента.Ключ, ДанныеДокумента.Значение);
	КонецЦикла;
	
	ПроведениеБольничнаяАптека.ЗаполнитьТаблицыДвижений(ДополнительныеСвойства, Запрос.ВыполнитьПакет(), Регистры);
	
КонецПроцедуры

Функция ПодготовитьОсновныеДанныеДляПроведения(ДополнительныеСвойства)
	
	ЗапрашиваемыеДанные = Новый Структура;
	ЗапрашиваемыеДанные.Вставить("Ссылка");
	ЗапрашиваемыеДанные.Вставить("Период", "Дата");
	ЗапрашиваемыеДанные.Вставить("Организация");
	ЗапрашиваемыеДанные.Вставить("ПодразделениеОрганизации");
	ЗапрашиваемыеДанные.Вставить("Склад");
	ЗапрашиваемыеДанные.Вставить("ТипКассы", "КассаККМ.ТипКассы");
	ЗапрашиваемыеДанные.Вставить("Валюта");
	ЗапрашиваемыеДанные.Вставить("СтатусКассовойСмены");
	ЗапрашиваемыеДанные.Вставить("ДокументОснование", "ИнвентаризацияТоваров");
	
	ОсновныеДанныеДокумента = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(
		ПроведениеБольничнаяАптека.ПолучитьСсылкуНаДокументДляПроведения(ДополнительныеСвойства),
		ЗапрашиваемыеДанные);
	
	ОсновныеДанныеДокумента.Вставить("ВестиУчетПоИсточникамФинансирования", ПолучитьФункциональнуюОпцию("ИспользоватьИсточникиФинансирования"));
	ОсновныеДанныеДокумента.Вставить("ХозяйственнаяОперация", Перечисления.ХозяйственныеОперации.РеализацияВРозницу);
	ОсновныеДанныеДокумента.Вставить("ЗаполненПоИнвентаризации", ЗначениеЗаполнено(ОсновныеДанныеДокумента.ДокументОснование));
	
	СтруктураКурсаДокумента = РаботаСКурсамиВалют.ПолучитьКурсВалюты(ОсновныеДанныеДокумента.Валюта, ОсновныеДанныеДокумента.Период);
	КоэффициентПересчетаВВалютуРегл = СтруктураКурсаДокумента.Курс / СтруктураКурсаДокумента.Кратность;
	ОсновныеДанныеДокумента.Вставить("КоэффициентПересчетаВВалютуРегл", КоэффициентПересчетаВВалютуРегл);
		
	Если ОсновныеДанныеДокумента.ТипКассы = Перечисления.ТипыКассККМ.ФискальныйРегистратор Тогда
		ПолноеПроведение = (ОсновныеДанныеДокумента.СтатусКассовойСмены = Перечисления.СтатусыКассовойСмены.ЗакрытаЧекиЗаархивированы);
	Иначе
		ПолноеПроведение = Истина;
	КонецЕсли;
	ОсновныеДанныеДокумента.Вставить("ПолноеПроведение", ПолноеПроведение);
	
	ЗапасыСервер.ПриПодготовкеОсновныхДанныхДляПроведения(ДополнительныеСвойства, ОсновныеДанныеДокумента);
	
	Возврат ОсновныеДанныеДокумента;
	
КонецФункции

Функция ТекстЗапросаВтТаблицаТовары()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаТовары.НомерСтроки                             КАК НомерСтроки,
	|	ТаблицаТовары.Ссылка                                  КАК Ссылка,
	|	&Склад                                                КАК Склад,
	|	&Организация                                          КАК Организация,
	|	ЗНАЧЕНИЕ(Справочник.МестаХранения.ПустаяСсылка)       КАК МестоХранения,
	|	ТаблицаТовары.Номенклатура                            КАК Номенклатура,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияСерий В (&СтатусУчетПоСериям, &СтатусУчетСебестоимостиПоСериям)
	|			ТОГДА ТаблицаТовары.СерияНоменклатуры
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ                                                 КАК СерияНоменклатуры,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияПартий В (&СтатусУчетПоПартиям, &СтатусУчетСебестоимостиПоПартиям)
	|			ТОГДА ТаблицаТовары.Партия
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.ПартииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ                                                 КАК Партия,
	|	ВЫБОР 
	|		КОГДА &ВестиУчетПоИсточникамФинансирования
	|			ТОГДА ТаблицаТовары.ИсточникФинансирования
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.ИсточникиФинансирования.ПустаяСсылка)
	|	КОНЕЦ                                                 КАК ИсточникФинансирования,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияСерий В (&СтатусУчетСебестоимостиПоСериям)
	|			ТОГДА ТаблицаТовары.СерияНоменклатуры
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ                                                 КАК СерияНоменклатурыДляСебестоимости,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияПартий В (&СтатусУчетСебестоимостиПоПартиям)
	|			ТОГДА ТаблицаТовары.Партия
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.ПартииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ                                                 КАК ПартияДляСебестоимости,
	|	ТаблицаТовары.Количество                              КАК Количество,
	|	ТаблицаТовары.СуммаСНДС                               КАК СуммаСНДС,
	|	ТаблицаТовары.СуммаСНДС - ТаблицаТовары.СуммаНДС      КАК СуммаБезНДС,
	|	ВЫРАЗИТЬ(ТаблицаТовары.Сумма * &КоэффициентПересчетаВВалютуРегл КАК ЧИСЛО(15,2))
	|	                                                      КАК СуммаСНДСРегл,
	|	ВЫРАЗИТЬ((ТаблицаТовары.СуммаСНДС - ТаблицаТовары.СуммаНДС) * &КоэффициентПересчетаВВалютуРегл КАК ЧИСЛО(15,2))
	|	                                                      КАК СуммаБезНДСРегл
	|
	|ПОМЕСТИТЬ ВтТаблицаТовары
	|
	|ИЗ
	|	Документ.ОтчетОРозничныхПродажах.Товары КАК ТаблицаТовары
	|ГДЕ
	|	ТаблицаТовары.Ссылка = &Ссылка
	|;
	|
	|/////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаТоварыНаСкладах()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаТовары.НомерСтроки               КАК НомерСтроки,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)  КАК ВидДвижения,
	|	&Период                                 КАК Период,
	|	ТаблицаТовары.Организация               КАК Организация,
	|	ТаблицаТовары.Склад                     КАК Склад,
	|	ТаблицаТовары.МестоХранения             КАК МестоХранения,
	|	ТаблицаТовары.Номенклатура              КАК Номенклатура,
	|	ТаблицаТовары.СерияНоменклатуры         КАК СерияНоменклатуры,
	|	ТаблицаТовары.Партия                    КАК Партия,
	|	ТаблицаТовары.ИсточникФинансирования    КАК ИсточникФинансирования,
	|	ТаблицаТовары.Количество                КАК Количество
	|ИЗ
	|	ВтТаблицаТовары КАК ТаблицаТовары
	|ГДЕ
	|	ТаблицаТовары.Количество > 0
	|	И &ПолноеПроведение
	|	
	|ОБЪЕДИНИТЬ ВСЕ
	|	
	|ВЫБРАТЬ
	|	ТаблицаТовары.НомерСтроки               КАК НомерСтроки,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)  КАК ВидДвижения,
	|	&Период                                 КАК Период,
	|	ТаблицаТовары.Организация               КАК Организация,
	|	ТаблицаТовары.Склад                     КАК Склад,
	|	ТаблицаТовары.МестоХранения             КАК МестоХранения,
	|	ТаблицаТовары.Номенклатура              КАК Номенклатура,
	|	ТаблицаТовары.СерияНоменклатуры         КАК СерияНоменклатуры,
	|	ТаблицаТовары.Партия                    КАК Партия,
	|	ТаблицаТовары.ИсточникФинансирования    КАК ИсточникФинансирования,
	|	-ТаблицаТовары.Количество               КАК Количество
	|ИЗ
	|	ВтТаблицаТовары КАК ТаблицаТовары
	|ГДЕ
	|	ТаблицаТовары.Количество < 0
	|	И &ПолноеПроведение
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаСвободныеОстатки()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаТовары.НомерСтроки               КАК НомерСтроки,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)  КАК ВидДвижения,
	|	&Период                                 КАК Период,
	|	ТаблицаТовары.Организация               КАК Организация,
	|	ТаблицаТовары.Склад                     КАК Склад,
	|	ТаблицаТовары.МестоХранения             КАК МестоХранения,
	|	ТаблицаТовары.Номенклатура              КАК Номенклатура,
	|	ТаблицаТовары.СерияНоменклатуры         КАК СерияНоменклатуры,
	|	ТаблицаТовары.Партия                    КАК Партия,
	|	ТаблицаТовары.ИсточникФинансирования    КАК ИсточникФинансирования,
	|	ТаблицаТовары.Количество                КАК ВНаличии
	|ИЗ
	|	ВтТаблицаТовары КАК ТаблицаТовары
	|ГДЕ
	|	&ПолноеПроведение
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаВтАналитика()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ТаблицаТовары.Номенклатура                                               КАК Номенклатура,
	|	ТаблицаТовары.СерияНоменклатурыДляСебестоимости                          КАК СерияНоменклатуры,
	|	ТаблицаТовары.ПартияДляСебестоимости                                     КАК Партия,
	|	ТаблицаТовары.ИсточникФинансирования                                     КАК ИсточникФинансирования,
	|	АналитикаУчетаНоменклатуры.КлючАналитики                                 КАК АналитикаУчетаНоменклатуры,
	|	АналитикаВидаУчета.КлючАналитики                                         КАК АналитикаВидаУчета,
	|	ЗНАЧЕНИЕ(Перечисление.РазделыУчетаСебестоимостиТоваров.ТоварыНаСкладах)  КАК РазделУчета
	|ПОМЕСТИТЬ ВтАналитика
	|ИЗ
	|	ВтТаблицаТовары КАК ТаблицаТовары
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры
	|		ПО
	|			ТаблицаТовары.Номенклатура                        = АналитикаУчетаНоменклатуры.Номенклатура
	|			И ТаблицаТовары.СерияНоменклатурыДляСебестоимости = АналитикаУчетаНоменклатуры.СерияНоменклатуры
	|			И ТаблицаТовары.ПартияДляСебестоимости            = АналитикаУчетаНоменклатуры.Партия
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.АналитикаВидаУчета КАК АналитикаВидаУчета
	|		ПО
	|			АналитикаВидаУчета.Организация                = ТаблицаТовары.Организация
	|			И АналитикаВидаУчета.Склад                    = ТаблицаТовары.Склад
	|			И АналитикаВидаУчета.ИсточникФинансирования   = ТаблицаТовары.ИсточникФинансирования
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	АналитикаУчетаНоменклатуры,
	|	АналитикаВидаУчета,
	|	РазделУчета
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаСебестоимостьТоваров()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаТовары.НомерСтроки                       КАК НомерСтроки,
	|	&Период                                         КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)          КАК ВидДвижения,
	|	Аналитика.АналитикаУчетаНоменклатуры            КАК АналитикаУчетаНоменклатуры,
	|	Аналитика.АналитикаВидаУчета                    КАК АналитикаВидаУчета,
	|	Аналитика.РазделУчета                           КАК РазделУчета,
	|	ТаблицаТовары.Количество                        КАК Количество,
	|	&ХозяйственнаяОперация                          КАК ХозяйственнаяОперация,
	|	&Ссылка                                         КАК ДокументДвижения,
	|	НЕОПРЕДЕЛЕНО                                    КАК КорАналитикаУчетаНоменклатуры,
	|	НЕОПРЕДЕЛЕНО                                    КАК КорАналитикаВидаУчета,
	|	НЕОПРЕДЕЛЕНО                                    КАК КорРазделУчета,
	|	0                                               КАК КорКоличество
	|ИЗ
	|	ВтТаблицаТовары КАК ТаблицаТовары
	|		ЛЕВОЕ СОЕДИНЕНИЕ
	|			ВтАналитика КАК Аналитика
	|		ПО
	|			ТаблицаТовары.Номенклатура = Аналитика.Номенклатура
	|			И ТаблицаТовары.СерияНоменклатурыДляСебестоимости = Аналитика.СерияНоменклатуры
	|			И ТаблицаТовары.ПартияДляСебестоимости = Аналитика.Партия
	|			И ТаблицаТовары.ИсточникФинансирования = Аналитика.ИсточникФинансирования
	|ГДЕ
	|	ИСТИНА
	//|	И &ПолноеПроведение
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|	
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаВыручкаИСебестоимостьПродаж()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаТовары.НомерСтроки                         КАК НомерСтроки,
	|	&Период                                           КАК Период,
	|	&ХозяйственнаяОперация                            КАК ХозяйственнаяОперация,
	|	Аналитика.АналитикаУчетаНоменклатуры              КАК АналитикаУчетаНоменклатуры,
	|	ТаблицаТовары.Организация                         КАК Организация,
	|	ТаблицаТовары.Склад                               КАК Склад,
	|	&ПодразделениеОрганизации                         КАК ПодразделениеОрганизации,
	|	НЕОПРЕДЕЛЕНО                                      КАК Контрагент,
	|	НЕОПРЕДЕЛЕНО                                      КАК ДоговорКонтрагента,
	|	ТаблицаТовары.ИсточникФинансирования              КАК ИсточникФинансирования,
	|	ТаблицаТовары.Количество                          КАК Количество,
	|	ТаблицаТовары.СуммаСНДСРегл                       КАК Сумма,
	|	ТаблицаТовары.СуммаБезНДСРегл                     КАК СуммаБезНДС,
	|	&Ссылка                                           КАК ДокументДвижения
	|ИЗ
	|	ВтТаблицаТовары КАК ТаблицаТовары
	|		ЛЕВОЕ СОЕДИНЕНИЕ
	|			ВтАналитика КАК Аналитика
	|		ПО
	|			ТаблицаТовары.Номенклатура                        = Аналитика.Номенклатура
	|			И ТаблицаТовары.СерияНоменклатурыДляСебестоимости = Аналитика.СерияНоменклатуры
	|			И ТаблицаТовары.ПартияДляСебестоимости            = Аналитика.Партия
	|			И ТаблицаТовары.ИсточникФинансирования            = Аналитика.ИсточникФинансирования
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса
	
КонецФункции

Функция ТекстЗапросаВтСуммаПлатежнымиКартами()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	СУММА(ТабличнаяЧасть.Сумма) КАК Сумма
	|ПОМЕСТИТЬ ТаблицаСуммаПлатежнымиКартами
	|ИЗ
	|	Документ.ОтчетОРозничныхПродажах.ОплатаПлатежнымиКартами КАК ТабличнаяЧасть
	|ГДЕ
	|	ТабличнаяЧасть.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаДенежныеСредстваВКассахККМ()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ДанныеДокумента.Дата                    КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)  КАК ВидДвижения,
	|	ДанныеДокумента.КассаККМ.Владелец       КАК Организация,
	|	ДанныеДокумента.КассаККМ                КАК КассаККМ,
	|	ДанныеДокумента.СуммаДокумента - ЕСТЬNULL(ТаблицаСуммаПлатежнымиКартами.Сумма, 0) КАК Сумма
	|ИЗ
	|	Документ.ОтчетОРозничныхПродажах КАК ДанныеДокумента
	|		ЛЕВОЕ СОЕДИНЕНИЕ ТаблицаСуммаПлатежнымиКартами КАК ТаблицаСуммаПлатежнымиКартами
	|		ПО (ИСТИНА)
	|ГДЕ
	|	ДанныеДокумента.СуммаДокумента - ЕСТЬNULL(ТаблицаСуммаПлатежнымиКартами.Сумма, 0) <> 0
	|	И ДанныеДокумента.Ссылка = &Ссылка
	|	И &ПолноеПроведение
	|;
	|
	|/////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаТоварыКОформлениюИзлишковНедостач()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	&Период                                 КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)  КАК ВидДвижения,
	|	ТаблицаТовары.Организация               КАК Организация,
	|	&Склад                                  КАК Склад,
	|	&ДокументОснование                      КАК ДокументОснование,
	|	ТаблицаТовары.НомерСтроки               КАК НомерСтроки,
	|	ТаблицаТовары.Номенклатура              КАК Номенклатура,
	|	ТаблицаТовары.СерияНоменклатуры         КАК СерияНоменклатуры,
	|	ТаблицаТовары.Партия                    КАК Партия,
	|	ТаблицаТовары.ИсточникФинансирования    КАК ИсточникФинансирования,
	|	0                                       КАК КОформлениюОприходования,
	|	ТаблицаТовары.Количество                КАК КОформлениюСписания
	|ИЗ
	|	ВтТаблицаТовары КАК ТаблицаТовары
	|ГДЕ
	|	&ЗаполненПоИнвентаризации
	|	И ТаблицаТовары.Количество > 0
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;
	|
	|/////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Процедура ИнициализироватьКлючиАналитикиУчетаНоменклатуры(Реквизиты)
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.Текст = "
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ТаблицаТовары.Номенклатура КАК Номенклатура,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияСерий В (&СтатусУчетСебестоимостиПоСериям)
	|			ТОГДА ТаблицаТовары.СерияНоменклатуры
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ КАК СерияНоменклатуры,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияПартий В (&СтатусУчетСебестоимостиПоПартиям)
	|			ТОГДА ТаблицаТовары.Партия
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.ПартииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ КАК Партия
	|ПОМЕСТИТЬ втТаблицаАналитики
	|ИЗ
	|	Документ.ОтчетОРозничныхПродажах.Товары КАК ТаблицаТовары
	|ГДЕ
	|	ТаблицаТовары.Ссылка = &Ссылка
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Номенклатура,
	|	СерияНоменклатуры,
	|	Партия
	|";
	
	Запрос.УстановитьПараметр("Ссылка", Реквизиты.Ссылка);
	Запрос.УстановитьПараметр("СтатусУчетСебестоимостиПоСериям", Реквизиты.СтатусУчетСебестоимостиПоСериям);
	Запрос.УстановитьПараметр("СтатусУчетСебестоимостиПоПартиям", Реквизиты.СтатусУчетСебестоимостиПоПартиям);
	Запрос.Выполнить();
	
	Справочники.КлючиАналитикиУчетаНоменклатуры.ИнициализироватьКлючиАналитики(Запрос.МенеджерВременныхТаблиц);
	
КонецПроцедуры

Процедура ИнициализироватьКлючиАналитикиВидаУчета(Реквизиты)
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.Текст = "
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	&Организация КАК Организация,
	|	&Склад КАК Склад,
	|	ВЫБОР
	|		КОГДА &ВестиУчетПоИсточникамФинансирования
	|			ТОГДА ТаблицаТовары.ИсточникФинансирования
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.ИсточникиФинансирования.ПустаяСсылка)
	|	КОНЕЦ КАК ИсточникФинансирования
	|ПОМЕСТИТЬ втТаблицаАналитики
	|ИЗ
	|	Документ.ОтчетОРозничныхПродажах.Товары КАК ТаблицаТовары
	|ГДЕ
	|	ТаблицаТовары.Ссылка = &Ссылка
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Организация,
	|	Склад,
	|	ИсточникФинансирования
	|";
	
	Запрос.УстановитьПараметр("Ссылка", Реквизиты.Ссылка);
	Запрос.УстановитьПараметр("Организация", Реквизиты.Организация);
	Запрос.УстановитьПараметр("Склад", Реквизиты.Склад);
	Запрос.УстановитьПараметр("ВестиУчетПоИсточникамФинансирования", Реквизиты.ВестиУчетПоИсточникамФинансирования);
	Запрос.Выполнить();
	
	Справочники.КлючиАналитикиВидаУчета.ИнициализироватьКлючиАналитики(Запрос.МенеджерВременныхТаблиц);
	
КонецПроцедуры

#КонецОбласти // Проведение

////////////////////////////////////////////////////////////////////////////////
// Печать
#Область Печать

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	УправлениеПечатьюБольничнаяАптека.ДобавитьКомандыПечати(ПустаяСсылка().Метаданные().ПолноеИмя(), КомандыПечати);
	
КонецПроцедуры

// Возвращает список доступных печатных форм документа
//
Функция ДоступныеПечатныеФормы() Экспорт
	
	ПечатныеФормы = УправлениеПечатьюБольничнаяАптека.СоздатьКоллекциюДоступныхПечатныхФорм();
	
	Обработки.ПечатьКМ3.ДобавитьПечатнуюФорму(ПечатныеФормы);
	
	Возврат ПечатныеФормы;
	
КонецФункции

Функция ПолучитьДанныеДляПечати(МассивОбъектов, ПараметрыПечати = Неопределено) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = ТекстЗапросаДанныеДляПечати();
	Запрос.УстановитьПараметр("ТекущийДокумент", МассивОбъектов);
	
	РезультатыЗапросов = Запрос.ВыполнитьПакет();
	
	ДанныеДляПечати = Новый Структура;
	ДанныеДляПечати.Вставить("РезультатПоШапке", РезультатыЗапросов[РезультатыЗапросов.ВГраница() - 1]);
	ДанныеДляПечати.Вставить("ДанныеПоЧекам"   , РезультатыЗапросов[РезультатыЗапросов.ВГраница()]);
	
	Возврат ДанныеДляПечати;
	
КонецФункции

Функция ТекстЗапросаДанныеДляПечати()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ОтчетОРозничныхПродажах.Ссылка                         КАК Ссылка,
	|	ОтчетОРозничныхПродажах.Номер                          КАК НомерДокумента,
	|	ОтчетОРозничныхПродажах.Дата                           КАК ДатаДокумента,
	|	ОтчетОРозничныхПродажах.Ответственный.ФизическоеЛицо   КАК КассирККМ,
	|	ОтчетОРозничныхПродажах.Валюта                         КАК Валюта,
	|	ОтчетОРозничныхПродажах.Организация                    КАК Организация,
	|	ОтчетОРозничныхПродажах.Организация.Представление      КАК Поставщик,
	|	ОтчетОРозничныхПродажах.Склад                          КАК Склад,
	|	ОтчетОРозничныхПродажах.Склад.Наименование             КАК СкладПредставление,
	|	ОтчетОРозничныхПродажах.КассаККМ                       КАК КассаККМ,
	|	ОтчетОРозничныхПродажах.КассаККМ.ТипКассы              КАК ТипКассы,
	|	ОтчетОРозничныхПродажах.КассаККМ.Представление         КАК Покупатель,
	|	ОтчетОРозничныхПродажах.КассаККМ.СерийныйНомер         КАК СерийныйНомерККМ,
	|	ОтчетОРозничныхПродажах.КассаККМ.РегистрационныйНомер  КАК РегистрационныйНомерККМ,
	|	ОтчетОРозничныхПродажах.КассаККМ.Наименование          КАК ПредставлениеККМ
	|ИЗ
	|	Документ.ОтчетОРозничныхПродажах КАК ОтчетОРозничныхПродажах
	|ГДЕ
	|	ОтчетОРозничныхПродажах.Ссылка В(&ТекущийДокумент)
	|
	|УПОРЯДОЧИТЬ ПО
	|	Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ДокОтчетОРозничныхПродажах.Ссылка                        КАК Документ,
	|	ДокЧек.ЧекККМ                                            КАК ЧекККМ,
	|	ЖурналФискальныхОпераций.ФискальнаяОперацияНомерЧекаККМ  КАК НомерЧека,
	|	ДокЧек.СуммаДокумента                                    КАК СуммаДокумента
	|ИЗ
	|	Документ.ОтчетОРозничныхПродажах КАК ДокОтчетОРозничныхПродажах
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|			Документ.ЧекККМВозврат КАК ДокЧек
	|		ПО
	|			ДокОтчетОРозничныхПродажах.Ссылка = ДокЧек.КассоваяСмена
	|			И ДокЧек.Проведен
	|			И ДокЧек.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыЧековККМ.Пробит)
	|		ЛЕВОЕ СОЕДИНЕНИЕ
	|			РегистрСведений.ЖурналФискальныхОпераций КАК ЖурналФискальныхОпераций
	|		ПО
	|			ЖурналФискальныхОпераций.ДокументОснование = ДокЧек.ЧекККМ
	|ГДЕ
	|	ДокОтчетОРозничныхПродажах.Ссылка В(&ТекущийДокумент)
	|
	|УПОРЯДОЧИТЬ ПО
	|	Документ
	|ИТОГИ ПО
	|	Документ
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

#КонецОбласти // Печать

////////////////////////////////////////////////////////////////////////////////
// Команды формы
#Область КомандыФормы

// Заполняет список команд ввода на основании.
// 
// Параметры:
//   КомандыСоздатьНаОсновании - ТаблицаЗначений - Таблица команд для вывода в подменю. Для изменения.
//
Процедура ДобавитьКомандыСоздатьНаОсновании(КомандыСоздатьНаОсновании, НастройкиФормы) Экспорт
	
	ВводНаОснованииБольничнаяАптека.ДобавитьКомандыСоздатьНаОсновании(ПустаяСсылка().Метаданные().ПолноеИмя(), КомандыСоздатьНаОсновании, НастройкиФормы);
	
КонецПроцедуры

// Заполняет список команд отчетов.
// 
// Параметры:
//   КомандыОтчетов - ТаблицаЗначений - Таблица команд для вывода в подменю. Для изменения.
//
Процедура ДобавитьКомандыОтчетов(КомандыОтчетов, НастройкиФормы) Экспорт
	
	МенюОтчетыБольничнаяАптека.ДобавитьОбщиеКоманды(ПустаяСсылка().Метаданные().ПолноеИмя(), КомандыОтчетов, НастройкиФормы);
	
КонецПроцедуры

#КонецОбласти // КомандыФормы

#КонецОбласти // СлужебныеПроцедурыИФункции

////////////////////////////////////////////////////////////////////////////////
// СТАНДАРТНЫЕ ПОДСИСТЕМЫ
#Область СтандартныеПодсистемы

// СтандартныеПодсистемы.ВерсионированиеОбъектов

// Определяет настройки объекта для подсистемы ВерсионированиеОбъектов.
//
// Параметры:
//  Настройки - Структура - настройки подсистемы.
//
Процедура ПриОпределенииНастроекВерсионированияОбъектов(Настройки) Экспорт
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов

#КонецОбласти // СтандартныеПодсистемы

////////////////////////////////////////////////////////////////////////////////
// ОБНОВЛЕНИЕ ИНФОРМАЦИОННОЙ БАЗЫ
#Область ОбновлениеИнформационнойБазы

Процедура ЗаполнитьКассовыеСменыДанныеДляОбновления(Параметры) Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	Ссылка
	|ИЗ
	|	Документ.ОтчетОРозничныхПродажах
	|ГДЕ
	|	КассоваяСмена = ЗНАЧЕНИЕ(Документ.КассоваяСмена.ПустаяСсылка)
	|	И КассаККМ.ТипКассы = ЗНАЧЕНИЕ(Перечисление.ТипыКассККМ.ФискальныйРегистратор)
	|	И СтатусКассовойСмены = ЗНАЧЕНИЕ(Перечисление.СтатусыКассовойСмены.Открыта)
	|	И НЕ ПометкаУдаления
	|");
	
	ОбновлениеИнформационнойБазы.ОтметитьКОбработке(Параметры, Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Ссылка"));
	
КонецПроцедуры

Процедура ЗаполнитьКассовыеСменыОтложено(Параметры) Экспорт
	
	МетаданныеОбъекта = ПустаяСсылка().Метаданные();
	ПолноеИмяОбъекта = МетаданныеОбъекта.ПолноеИмя();
	МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Выборка = ОбновлениеИнформационнойБазы.ВыбратьСсылкиДляОбработки(Параметры.Очередь, ПолноеИмяОбъекта);
	Пока Выборка.Следующий() Цикл
		
		НачатьТранзакцию();
		
		Попытка
			
			Блокировка = Новый БлокировкаДанных;
			ЭлементБлокировки = Блокировка.Добавить(ПолноеИмяОбъекта);
			ЭлементБлокировки.УстановитьЗначение("Ссылка", Выборка.Ссылка);
			Блокировка.Заблокировать();
			
			ОтчетОРозничныхПродажах = Выборка.Ссылка.ПолучитьОбъект();
			
			Если ОтчетОРозничныхПродажах = Неопределено
			 Или ЗначениеЗаполнено(ОтчетОРозничныхПродажах.КассоваяСмена)
			 Или ОтчетОРозничныхПродажах.СтатусКассовойСмены <> Перечисления.СтатусыКассовойСмены.Открыта Тогда
				ОбновлениеИнформационнойБазы.ОтметитьВыполнениеОбработки(Выборка.Ссылка);
				ЗафиксироватьТранзакцию();
				Продолжить;
			КонецЕсли;
			
			НоваяКассоваяСмена = Документы.КассоваяСмена.СоздатьДокумент();
			РеквизитыКассыККМ = Справочники.КассыККМ.РеквизитыКассыККМ(Выборка.КассаККМ);
			ЗаполнитьЗначенияСвойств(НоваяКассоваяСмена, РеквизитыКассыККМ);
			
			НоваяКассоваяСмена.Дата                   = Выборка.НачалоКассовойСмены;
			НоваяКассоваяСмена.Статус                 = Перечисления.СтатусыКассовойСмены.Открыта;
			НоваяКассоваяСмена.НачалоКассовойСмены    = Выборка.НачалоКассовойСмены;
			НоваяКассоваяСмена.ОкончаниеКассовойСмены = '00010101';
			НоваяКассоваяСмена.ДополнительныеСвойства.Вставить("НеСинхронизироватьКассовуюСменуСОтчетомОРозничныхПродажах", Истина);
			
			НоваяКассоваяСмена.Записать(РежимЗаписиДокумента.Проведение);
			
			ОтчетОРозничныхПродажах.КассоваяСмена = НоваяКассоваяСмена.Ссылка;
			
			ОбновлениеИнформационнойБазы.ЗаписатьДанные(ОтчетОРозничныхПродажах);
			
			ЗафиксироватьТранзакцию();
			
		Исключение
			
			ОтменитьТранзакцию();
			
			ТекстСообщения = НСтр("ru = 'Не удалось обработать документ: %Ссылка% по причине: %Причина%'");
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Ссылка%",  Выборка.Ссылка);
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Причина%", ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
			
			ЗаписьЖурналаРегистрации(
				ОбновлениеИнформационнойБазы.СобытиеЖурналаРегистрации(),
				УровеньЖурналаРегистрации.Предупреждение,
				МетаданныеОбъекта,
				Выборка.Ссылка,
				ТекстСообщения);
			
			ВызватьИсключение;
			
		КонецПопытки;
		
	КонецЦикла;
	
	Параметры.ОбработкаЗавершена = Не ОбновлениеИнформационнойБазы.ЕстьДанныеДляОбработки(Параметры.Очередь, ПолноеИмяОбъекта);
	
КонецПроцедуры

Процедура ЗаполнитьНалогообложениеНДСДанныеДляОбновления(Параметры) Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	Ссылка
	|ИЗ
	|	Документ.ОтчетОРозничныхПродажах
	|ГДЕ
	|	НалогообложениеНДС = ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПустаяСсылка)
	|	И НЕ ПометкаУдаления
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	Ссылка
	|ИЗ
	|	Документ.ЧекККМ
	|ГДЕ
	|	НалогообложениеНДС = ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПустаяСсылка)
	|	И НЕ ПометкаУдаления
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	Ссылка
	|ИЗ
	|	Документ.ЧекККМВозврат
	|ГДЕ
	|	НалогообложениеНДС = ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПустаяСсылка)
	|	И НЕ ПометкаУдаления
	|");
	
	ОбновлениеИнформационнойБазы.ОтметитьКОбработке(Параметры, Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Ссылка"));
	
КонецПроцедуры

Процедура ЗаполнитьНалогообложениеНДСОтложено(Параметры) Экспорт
	
	ОбработкаЗавершена = Истина;
	
	ОбрабатываемыеОбъекты = Новый Массив;
	ОбрабатываемыеОбъекты.Добавить(Метаданные.Документы.ЧекККМ);
	ОбрабатываемыеОбъекты.Добавить(Метаданные.Документы.ЧекККМВозврат);
	ОбрабатываемыеОбъекты.Добавить(Метаданные.Документы.ОтчетОРозничныхПродажах);
	
	Для Каждого МетаданныеОбъекта Из ОбрабатываемыеОбъекты Цикл
		ПолноеИмяОбъекта = МетаданныеОбъекта.ПолноеИмя();
		МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
		
		Выборка = ОбновлениеИнформационнойБазы.ВыбратьСсылкиДляОбработки(Параметры.Очередь, ПолноеИмяОбъекта);
		Пока Выборка.Следующий() Цикл
			
			НачатьТранзакцию();
			
			Попытка
				
				Блокировка = Новый БлокировкаДанных;
				ЭлементБлокировки = Блокировка.Добавить(ПолноеИмяОбъекта);
				ЭлементБлокировки.УстановитьЗначение("Ссылка", Выборка.Ссылка);
				Блокировка.Заблокировать();
				
				Документ = Выборка.Ссылка.ПолучитьОбъект();
				
				Если Документ = Неопределено
				 Или ЗначениеЗаполнено(Документ.НалогообложениеНДС) Тогда
					ОбновлениеИнформационнойБазы.ОтметитьВыполнениеОбработки(Выборка.Ссылка);
					ЗафиксироватьТранзакцию();
					Продолжить;
				КонецЕсли;
				
				Документ.НалогообложениеНДС = Перечисления.ТипыНалогообложенияНДС.ПродажаОблагаетсяНДС;
				
				ОбновлениеИнформационнойБазы.ЗаписатьДанные(Документ);
				
				ЗафиксироватьТранзакцию();
				
			Исключение
				
				ОтменитьТранзакцию();
				
				ТекстСообщения = НСтр("ru = 'Не удалось обработать документ: %Ссылка% по причине: %Причина%'");
				ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Ссылка%",  Выборка.Ссылка);
				ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Причина%", ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
				
				ЗаписьЖурналаРегистрации(
					ОбновлениеИнформационнойБазы.СобытиеЖурналаРегистрации(),
					УровеньЖурналаРегистрации.Предупреждение,
					МетаданныеОбъекта,
					Выборка.Ссылка,
					ТекстСообщения);
				
				ВызватьИсключение;
				
			КонецПопытки;
			
		КонецЦикла;
		
		ОбработкаЗавершена = ОбработкаЗавершена И Не ОбновлениеИнформационнойБазы.ЕстьДанныеДляОбработки(Параметры.Очередь, ПолноеИмяОбъекта);
		
	КонецЦикла;
	
	Параметры.ОбработкаЗавершена = ОбработкаЗавершена;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли