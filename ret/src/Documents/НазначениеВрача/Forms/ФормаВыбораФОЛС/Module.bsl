
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ
#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура ГруппаПКУПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(ОписанияАптечныхТоваров, "ГруппаПКУ", ГруппаПКУ,,, ЗначениеЗаполнено(ГруппаПКУ));
	
КонецПроцедуры

&НаКлиенте
Процедура ОписанияАптечныхТоваровВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ОбработатьВыборЛС();
	
КонецПроцедуры

&НаКлиенте
Процедура ОписанияАптечныхТоваровВыборЗначения(Элемент, Значение, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ОбработатьВыборЛС();
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийЭлементовФормы

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ОбработатьВыборЛС()
	
	ТекущаяСтрока = Элементы.ОписанияАптечныхТоваров.ТекущиеДанные;
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("ТорговоеНаименование"    , ТекущаяСтрока.ТорговоеНаименование);
	ПараметрыФормы.Вставить("ДействующиеВеществаМНН"  , ТекущаяСтрока.ДействующиеВеществаМНН);
	ПараметрыФормы.Вставить("ФормаВыпуска"            , ТекущаяСтрока.ФормаВыпуска);
	
	Оповещение = Новый ОписаниеОповещения("ОбработатьВыборЛСЗавершение", ЭтотОбъект);
	ОткрытьФорму("Обработка.ПодборПоФормальнымОписаниямЛекарственныхСредств.Форма.ФормаВыбора", ПараметрыФормы, ЭтотОбъект,,,, Оповещение);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработатьВыборЛСЗавершение(ДанныеВыбора, ДополнительныеПараметры) Экспорт
	
	Если ТипЗнч(ДанныеВыбора) = Тип("Структура") Тогда
		Закрыть(ДанныеВыбора);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти // СлужебныеПроцедурыИФункции
