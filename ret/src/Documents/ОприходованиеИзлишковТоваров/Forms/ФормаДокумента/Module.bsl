
////////////////////////////////////////////////////////////////////////////////
// ОПИСАНИЕ ПЕРЕМЕННЫХ
#Область ОписаниеПеременных

&НаКлиенте
Перем КэшированныеЗначения;

#КонецОбласти // ОписаниеПеременных

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформлениеФормы();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);
	
	// СтандартныеПодсистемы.ВерсионированиеОбъектов
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКоманды.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	// ПодключаемоеОборудование
	ПодключаемоеОборудованиеСервер.НастроитьФормуДляИспользованияПодключаемогоОборудования(ЭтотОбъект);
	// Конец ПодключаемоеОборудование
	
	// ИнтеграцияС1СДокументооборотом
	Если ОбщегоНазначения.ПодсистемаСуществует("ИнтеграцияС1СДокументооборотом") Тогда
		МодульИнтеграцияС1СДокументооборот = ОбщегоНазначения.ОбщийМодуль("ИнтеграцияС1СДокументооборот");
		МодульИнтеграцияС1СДокументооборот.ПриСозданииНаСервере(ЭтотОбъект);
	КонецЕсли;
	// Конец ИнтеграцияС1СДокументооборотом
	
	НастройкаФормБольничнаяАптека.ФормаДокумента_ПриСозданииНаСервере(ЭтотОбъект);
	НастройкаФормБольничнаяАптека.НастроитьОтображениеИтогов(Элементы.ГруппаСуммаВсего);
	
	Элементы.ГруппаОснование.Видимость = ЗначениеЗаполнено(Объект.ИнвентаризацияТоваровНаСкладе);
	
	// БуферОбменаТоварами
	УстановитьДоступностьКомандБуфераОбмена(ЭтотОбъект, Не ОбработкаТабличнойЧастиСервер.БуферОбменаПустой());
	// Конец БуферОбменаТоварами
	
	ВалютаДокумента = ЗначениеНастроекБольничнаяАптекаПовтИсп.ПолучитьВалютуРегламентированногоУчета();
	
	Если Объект.Ссылка.Пустая() Тогда
		ПриСозданииНовогоПриЧтенииНаСервере();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	// СтандартныеПодсистемы.ДатыЗапретаИзменения
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.ДатыЗапретаИзменения
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	НастройкаФормБольничнаяАптека.ФормаДокумента_ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	
	ПриСозданииНовогоПриЧтенииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	// ПодключаемоеОборудование
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(Неопределено, ЭтотОбъект, "СканерШтрихкода");
	// Конец ПодключаемоеОборудование
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// ПодключаемоеОборудование
	Если ПодключаемоеОборудованиеКлиент.ОбрабатыватьОповещение(ЭтотОбъект, Источник) Тогда
		Если ПодключаемоеОборудованиеКлиент.ОбработатьПолучениеДанныхОтСканераШтрихкода(ЭтотОбъект, ИмяСобытия, Параметр) Тогда
			ОбработатьШтрихкоды(ОбщегоНазначенияБольничнаяАптекаКлиентСервер.ПолучитьДанныеШтрихкода(Параметр, 1));
		КонецЕсли;
	КонецЕсли;
	// Конец ПодключаемоеОборудование
	
	Если ИмяСобытия = "ВведенШтрихкод" И Источник = УникальныйИдентификатор Тогда
		ОбработатьШтрихкоды(ОбщегоНазначенияБольничнаяАптекаКлиентСервер.ПолучитьДанныеШтрихкода(Параметр, 1));
	КонецЕсли;
	
	Если Источник = "РегистрацияШтрихкодов"
	   И ИмяСобытия = "ЗарегистрированыШтрихкоды"
	   И Параметр.КлючВладельца = УникальныйИдентификатор Тогда
		Если Параметр.ЗарегистрированныеШтрихкоды.Количество() > 0 Тогда
			ОбновитьСтрокиНенайденныхШтрихКодов(Параметр.ЗарегистрированныеШтрихкоды);
		КонецЕсли;
	КонецЕсли;
	
	// БуферОбменаТоварами
	Если ОбработкаТабличнойЧастиКлиент.ОбрабатыватьОповещениеОтБуфераОбмена(ЭтотОбъект, ИмяСобытия, Источник) Тогда
		ДоступностьБуфераОбмена = ОбработкаТабличнойЧастиКлиент.ОпределитьДоступностьВставкиИзБуфераОбменаПоСобытию(ИмяСобытия);
		УстановитьДоступностьКомандБуфераОбмена(ЭтотОбъект, ДоступностьБуфераОбмена);
	КонецЕсли;
	// Конец БуферОбменаТоварами
	
	// ИнтеграцияМДЛП
	Если (ИмяСобытия = "ИзменениеСостоянияМДЛП" Или ИмяСобытия = "Запись_УведомлениеОПовторномВводеВОборотМДЛП")
	   И Параметр.Основание = Объект.Ссылка Тогда
		СформироватьТекстУведомленияМДЛП();
	КонецЕсли;
	Если ИмяСобытия = "ВыполненОбменМДЛП" Тогда
		СформироватьТекстУведомленияМДЛП();
	КонецЕсли;
	// Конец ИнтеграцияМДЛП
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	
	Если ПодборТоваровКлиент.ОбработатьПодборТоваровВДокументПоступления(ЭтотОбъект, ИсточникВыбора) Тогда
		ОбработатьПодбор(ВыбранноеЗначение.АдресТоваровВХранилище, КэшированныеЗначения);
	Иначе
		// БуферОбменаТоварами
		Если ОбработкаТабличнойЧастиКлиент.НужноОбработатьВставкуИзБуфераОбмена(ЭтотОбъект, ИсточникВыбора) Тогда
			ВставитьТоварыИзБуфераОбмена(ВыбранноеЗначение);
		КонецЕсли;
		// Конец БуферОбменаТоварами
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	
	ОценкаПроизводительностиБольничнаяАптекаКлиент.НачатьЗамерПроведенияДокумента(Объект.Ссылка, Отказ, ПараметрыЗаписи);
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	НастройкаФормБольничнаяАптека.ФормаДокумента_ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	
	// ИнтеграцияМДЛП
	СформироватьТекстУведомленияМДЛП();
	// Конец ИнтеграцияМДЛП
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	// ПодключаемоеОборудование
	МенеджерОборудованияКлиент.НачатьОтключениеОборудованиеПриЗакрытииФормы(Неопределено, ЭтотОбъект);
	// Конец ПодключаемоеОборудование
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНДЫ ФОРМЫ
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Заполнить(Команда)
	
	ОчиститьСообщения();
	Если Объект.ИнвентаризацияТоваровНаСкладе.Пустая() Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Не указан документ инвентаризации'"), ЭтотОбъект);
		Возврат;
	КонецЕсли;
	
	Если ПолучитьФункциональнуюОпциюФормы("ИспользоватьИсточникиФинансирования") И Не ЗначениеЗаполнено(Объект.ИсточникФинансирования) Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Поле ""Источник финансирования"" не заполнено'"),, "Объект.ИсточникФинансирования");
		Возврат;
	КонецЕсли;
	
	ЗаполнитьНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПоискПоШтрихкодуВыполнить()
	
	ОбработкаТабличнойЧастиКлиент.ПоказатьВводШтрихкода(УникальныйИдентификатор);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаПодбор(Команда)
	
	ПараметрыПодбора = Новый Структура;
	ПараметрыПодбора.Вставить("Документ", Объект.Ссылка);
	ПараметрыПодбора.Вставить("Организация", Объект.Организация);
	ПараметрыПодбора.Вставить("МестоХраненияОстатка", "Склад");
	ПараметрыПодбора.Вставить("Склад", Объект.Склад);
	ПараметрыПодбора.Вставить("ИсточникФинансирования", Объект.ИсточникФинансирования);
	
	ТипыНоменклатуры = ПодборТоваровКлиентСервер.ПолучитьОтборПоТипуНоменклатурыИзПараметровВыбора(Элементы.ТоварыНоменклатура.ПараметрыВыбора);
	ПараметрыПодбора.Вставить("ОтборПоТипуНоменклатуры", ТипыНоменклатуры);
	
	ПодборТоваровКлиент.ОткрытьПодборТоваровВДокументПоступления(ЭтотОбъект, ПараметрыПодбора);
	
КонецПроцедуры

&НаКлиенте
Процедура ПерезаполнитьНенайденныеШтрихкоды(Команда)
	
	ОбновитьСтрокиНенайденныхШтрихКодов();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработатьНенайденныеШтрихкоды(Команда)
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьНенайденныеШтрихкоды(Объект.Товары, ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьДанныеИзТСД(Команда)
	
	Оповещение = Новый ОписаниеОповещения("ОбработатьЗагрузкуДанныхИзТСД", ЭтотОбъект);
	МенеджерОборудованияКлиент.НачатьЗагрузкуДанныеИзТСД(Оповещение, УникальныйИдентификатор);
	
КонецПроцедуры

&НаКлиенте
Процедура ПолучитьВес(Команда)
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	Если ТекущаяСтрока = Неопределено Тогда
		ПоказатьПредупреждение(, НСтр("ru='Необходимо выбрать строку, для которой необходимо получить вес.'"));
		Возврат;
	КонецЕсли;
	
	Оповещение = Новый ОписаниеОповещения("ОбработатьПолучениеВеса", ЭтотОбъект, ТекущаяСтрока);
	МенеджерОборудованияКлиент.НачатьПолученияВесаСЭлектронныхВесов(Оповещение, УникальныйИдентификатор);
	
КонецПроцедуры

&НаКлиенте
Процедура РазбитьСтроку(Команда)
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	Оповещение = Новый ОписаниеОповещения("ПослеРазбиенияСтроки", ЭтотОбъект, ТекущаяСтрока);
	ОбработкаТабличнойЧастиКлиент.РазбитьСтрокуТЧ(Объект.Товары, ТекущаяСтрока, Оповещение);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиКомандФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ
#Область ОбработчикиСобытийЭлементовФормы

////////////////////////////////////////////////////////////////////////////////
// Шапка
#Область Шапка

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	ОбработатьИзменениеОрганизации();
	
КонецПроцедуры

&НаКлиенте
Процедура ПодразделениеОрганизацииПриИзменении(Элемент)
	
	ОбработатьИзменениеПодразделения();
	
КонецПроцедуры

&НаКлиенте
Процедура СкладПриИзменении(Элемент)
	
	Если Склад <> Объект.Склад Тогда
		ОбработатьИзменениеСклада();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СтатьяДоходовПриИзменении(Элемент)
	
	ОбработатьИзменениеСтатьиДоходов(КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ТекстУведомленияМДЛПОбработкаНавигационнойСсылки(Элемент, НавигационнаяСсылкаФорматированнойСтроки, СтандартнаяОбработка)
	
	ИнтеграцияМДЛПКлиент.ТекстУведомленияМДЛПОбработкаНавигационнойСсылки(ЭтотОбъект, НавигационнаяСсылкаФорматированнойСтроки, СтандартнаяОбработка);
	
КонецПроцедуры

#КонецОбласти // Шапка

////////////////////////////////////////////////////////////////////////////////
// Список "Товары"
#Область Товары

&НаКлиенте
Процедура ТоварыНоменклатураПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	
	ТекущаяСтрока.Штрихкод = "";
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ПроверитьСериюНоменклатурыПоВладельцу(), ТекущаяСтрока.СерияНоменклатуры);
	СтруктураДействий.Вставить(Действия.Действие_ПроверитьУпаковкуПоВладельцу(), ТекущаяСтрока.ЕдиницаИзмерения);
	СтруктураДействий.Вставить(Действия.Действие_ПроверитьПартиюПоВладельцу(), ТекущаяСтрока.Партия);
	СтруктураДействий.Вставить(Действия.Действие_ЗаполнитьЕдиницуИзмерения());
	СтруктураДействий.Вставить(Действия.Действие_ЗаполнитьПараметрыУчета(), ПараметрыУчетаНоменклатуры);
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьКоэффициент());
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьКоличествоЕдиниц());
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьСумму());
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТабличнойЧасти(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыСерияНоменклатурыПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ПроверитьПартиюПоВладельцу(), ТекущаяСтрока.Партия);
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТабличнойЧасти(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыЕдиницаИзмеренияПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьКоэффициент());
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьКоличествоЕдиниц());
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТабличнойЧасти(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыКоличествоПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	ПриИзмененииКоличестваВСтрокеСпискаТовары(ТекущаяСтрока);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыПартияНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ТекущаяСтрока = Элемент.Родитель.ТекущиеДанные;
	ОтборПартий = Новый Структура;
	ОтборПартий.Вставить("Документ"     , Объект.Ссылка);
	ОтборПартий.Вставить("Организация"  , Объект.Организация);
	ОтборПартий.Вставить("Склад"        , Объект.Склад);
	
	ПараметрыВыбораПартии = ОбработкаТабличнойЧастиКлиент.ПолучитьПараметрыВыбораПартии(ОтборПартий, ТекущаяСтрока);
	ПараметрыВыбораПартии.МожноСоздаватьПартию = Истина;
	
	ОбработкаТабличнойЧастиКлиент.ВыбратьПартиюНоменклатуры(ЭтотОбъект, Элемент, ПараметрыВыбораПартии, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыЦенаПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьСумму());
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТабличнойЧасти(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ТоварыСуммаПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Товары.ТекущиеДанные;
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьЦену());
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТабличнойЧасти(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

#КонецОбласти // Товары

#КонецОбласти // ОбработчикиСобытийЭлементовФормы

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ПриСозданииНовогоПриЧтенииНаСервере()
	
	ПараметрыУчетаНоменклатуры = Новый ФиксированнаяСтруктура(ЗапасыСервер.ПолучитьПараметрыУчетаНоменклатуры(Объект));
	
	Склад = Объект.Склад;
	ТипСклада = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Склад, "ТипСклада");
	УстановитьДоступностьЭлементовПоТипуСклада();
	
	ОсновнойСклад = ЗначениеНастроекБольничнаяАптекаПовтИсп.ПолучитьСкладПоУмолчанию(Неопределено, Объект.ПодразделениеОрганизации);
	
	ЗаполнитьСлужебныеРеквизиты();
	
	// ИнтеграцияМДЛП
	СформироватьТекстУведомленияМДЛП();
	// Конец ИнтеграцияМДЛП
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСлужебныеРеквизиты(КэшированныеЗначения = Неопределено)
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ЗаполнитьСлужебныеРеквизитыСтатьиДоходов());
	
	ДанныеОбъекта = Новый Структура;
	ДанныеОбъекта.Вставить("СтатьяДоходов", Объект.СтатьяДоходов);
	ДанныеОбъекта.Вставить("АналитикаДоходовКонтролироватьЗаполнениеАналитики");
	
	ОбработкаТабличнойЧастиСервер.ОбработатьСтрокуТабличнойЧасти(ДанныеОбъекта, СтруктураДействий, КэшированныеЗначения);
	
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, ДанныеОбъекта);
	
КонецПроцедуры

&НаСервере
Процедура УстановитьУсловноеОформлениеФормы()
	
	ОбработкаТабличнойЧастиСервер.УстановитьОформлениеСерийНоменклатуры(ЭтотОбъект);
	ОбработкаТабличнойЧастиСервер.УстановитьОформлениеПартий(ЭтотОбъект);
	
	ПланыВидовХарактеристик.СтатьиДоходов.УстановитьУсловноеОформлениеАналитик(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьНаСервере()
	
	ДокументОбъект = РеквизитФормыВЗначение("Объект");
	
	ДокументОбъект.Товары.Очистить();
	ДокументОбъект.ЗаполнитьДокументНаОснованииИнвентаризацииТоваровНаСкладе();
	
	ЗначениеВРеквизитФормы(ДокументОбъект, "Объект");
	
КонецПроцедуры

&НаСервере
Процедура УстановитьДоступностьЭлементовПоТипуСклада()
	
	УстановитьПараметрыФункциональныхОпцийФормы(Новый Структура("Склад", Объект.Склад));
	
	ЭлементыФормы = Новый Массив;
	ЭлементыФормы.Добавить("ИсточникФинансирования");
	
	ОбщегоНазначенияБольничнаяАптекаКлиентСервер.УстановитьСвойствоЭлементовФормы(
		Элементы,
		ЭлементыФормы,
		"ТолькоПросмотр",
		ТипСклада = Перечисления.ТипыСкладов.РозничныйМагазин);
	
КонецПроцедуры

// ИнтеграцияМДЛП

&НаСервере
Процедура СформироватьТекстУведомленияМДЛП()
	
	ИнтеграцияМДЛП.СформироватьТекстУведомленияМДЛП(ЭтотОбъект);
	
КонецПроцедуры

// Конец ИнтеграцияМДЛП

////////////////////////////////////////////////////////////////////////////////
// Обработка штрихкодов
#Область ОбработкаШтрихкодов

&НаКлиенте
Процедура ОбработатьШтрихкоды(ДанныеШтрихкодов)
	
	ДействияСДобавленнымиСтроками = Новый Структура;
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	ДействияСДобавленнымиСтроками = Новый Структура;
	ДействияСДобавленнымиСтроками.Вставить(Действия.Действие_ЗаполнитьЕдиницуИзмерения());
	ДействияСДобавленнымиСтроками.Вставить(Действия.Действие_ЗаполнитьПараметрыУчета(), ПараметрыУчетаНоменклатуры);
	ДействияСДобавленнымиСтроками.Вставить(Действия.Действие_ПересчитатьКоэффициент());
	ДействияСДобавленнымиСтроками.Вставить(Действия.Действие_ПересчитатьКоличествоЕдиниц());
	ДействияСДобавленнымиСтроками.Вставить(Действия.Действие_ПересчитатьСумму());
	
	ДействияСИзмененнымиСтроками = Новый Структура;
	ДействияСИзмененнымиСтроками.Вставить(Действия.Действие_ПересчитатьКоличествоЕдиниц());
	
	ИзменятьКоличество = Не ТолькоПросмотр;
	ПараметрыДействия = ОбработкаТабличнойЧастиКлиент.ПолучитьПараметрыОбработкиШтрихкодов(ДанныеШтрихкодов, ДействияСДобавленнымиСтроками, ДействияСИзмененнымиСтроками);
	ПараметрыДействия.ИзменятьКоличество = ИзменятьКоличество;
	ПараметрыДействия.ПараметрыУчетаНоменклатуры = ПараметрыУчетаНоменклатуры;
	
	ОбработатьШтрихкодыНаСервере(ПараметрыДействия, КэшированныеЗначения);
	
	ОбработкаТабличнойЧастиКлиент.СообщитьОНеизвестныхШтрихкодах(ПараметрыДействия);
	
	Если ПараметрыДействия.ТекущаяСтрока <> Неопределено Тогда
		Элементы.Товары.ТекущаяСтрока = ПараметрыДействия.ТекущаяСтрока;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьШтрихкодыНаСервере(ПараметрыДействия, КэшированныеЗначения)
	
	ОбработкаТабличнойЧастиСервер.ОбработатьШтрихкоды(ЭтотОбъект, Объект, ПараметрыДействия, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьСтрокиНенайденныхШтрихКодов(ЗарегистрированныеШтрихкоды = Неопределено)
	
	Если Не ОбработкаТабличнойЧастиКлиент.ЕстьНенайденныеШтрихкоды(Объект.Товары) Тогда
		Возврат;
	КонецЕсли;
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	ДействияСИзмененнымиСтроками = Новый Структура;
	ДействияСИзмененнымиСтроками.Вставить(Действия.Действие_ЗаполнитьЕдиницуИзмерения());
	ДействияСИзмененнымиСтроками.Вставить(Действия.Действие_ЗаполнитьПараметрыУчета(), ПараметрыУчетаНоменклатуры);
	ДействияСИзмененнымиСтроками.Вставить(Действия.Действие_ПересчитатьКоэффициент());
	ДействияСИзмененнымиСтроками.Вставить(Действия.Действие_ПересчитатьКоличествоЕдиниц());
	
	ПараметрыДействия = ОбработкаТабличнойЧастиКлиент.ПолучитьПараметрыОбработкиНенайденныхШтрихкодов();
	ПараметрыДействия.ДействияСИзмененнымиСтроками = ДействияСИзмененнымиСтроками;
	Если ЗарегистрированныеШтрихкоды <> Неопределено Тогда
		ПараметрыДействия.ЗарегистрированныеШтрихкоды = ЗарегистрированныеШтрихкоды;
	КонецЕсли;
	
	ОбновитьДанныеНенайденныхШтрихКодовНаСервере(ПараметрыДействия, КэшированныеЗначения);
	
	Если ПараметрыДействия.Модифицированность Тогда
		Модифицированность = Истина;
	КонецЕсли;
	
	ОбработкаТабличнойЧастиКлиент.СообщитьОНеизвестныхШтрихкодах(ПараметрыДействия);
	ОбработкаТабличнойЧастиКлиент.СообщитьОРезультатеОбновленияДанныхПоШтрихкодам(ПараметрыДействия);
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьДанныеНенайденныхШтрихКодовНаСервере(ПараметрыДействия, КэшированныеЗначения)
	
	ОбработкаТабличнойЧастиСервер.ОбновитьДанныеНенайденныхШтрихКодов(Объект, ПараметрыДействия, КэшированныеЗначения);
	
КонецПроцедуры

#КонецОбласти // ОбработкаШтрихкодов

////////////////////////////////////////////////////////////////////////////////
// Обработка подбора
#Область ОбработкаПодбора

&НаСервере
Процедура ОбработатьПодбор(АдресТоваровВХранилище, КэшированныеЗначения)
	
	СписокТоваров = ПолучитьИзВременногоХранилища(АдресТоваровВХранилище);
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ЗаполнитьПараметрыУчета(), ПараметрыУчетаНоменклатуры);
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьКоличествоЕдиниц());
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьСумму());
	
	Для Каждого СтрокаТовара Из СписокТоваров Цикл
		
		НоваяСтрока = Объект.Товары.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрокаТовара);
		
		ОбработкаТабличнойЧастиСервер.ОбработатьСтрокуТабличнойЧасти(НоваяСтрока, СтруктураДействий, КэшированныеЗначения);
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти // ОбработкаПодбора

////////////////////////////////////////////////////////////////////////////////
// Обработка изменения реквизитов
#Область ОбработкаИзмененияРеквизитов

&НаСервере
Процедура ОбработатьИзменениеОрганизации()
	
	Подразделение = ЗначениеНастроекБольничнаяАптекаПовтИсп.ПолучитьПодразделениеПоУмолчанию(Объект.ПодразделениеОрганизации, Объект.Организация);
	Если Объект.ПодразделениеОрганизации <> Подразделение Тогда
		Объект.ПодразделениеОрганизации = Подразделение;
		ОбработатьИзменениеПодразделения();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьИзменениеПодразделения()
	
	ОбщегоНазначенияБольничнаяАптека.ИзменитьСкладПриНеобходимости(Объект.ПодразделениеОрганизации, Объект.Склад, ОсновнойСклад);
	ОбработатьИзменениеСклада();
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьИзменениеСклада()
	
	Если Склад = Объект.Склад Тогда
		Возврат;
	КонецЕсли;
	
	Склад = Объект.Склад;
	
	ПараметрыУчетаНоменклатуры = Новый ФиксированнаяСтруктура(ЗапасыСервер.ПолучитьПараметрыУчетаНоменклатуры(Объект));
	ЗапасыСервер.ЗаполнитьСтатусыУчетаНоменклатуры(Объект, ПараметрыУчетаНоменклатуры);
	
	РеквизитыСклада = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Объект.Склад, "ТипСклада, ИсточникФинансирования");
	ТипСклада = РеквизитыСклада.ТипСклада;
	
	Если ТипСклада = Перечисления.ТипыСкладов.РозничныйМагазин Тогда
		Объект.ИсточникФинансирования = РеквизитыСклада.ИсточникФинансирования;
	КонецЕсли;
	
	УстановитьДоступностьЭлементовПоТипуСклада();
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьИзменениеСтатьиДоходов(КэшированныеЗначения)
	
	ПланыВидовХарактеристик.СтатьиДоходов.ОбработатьИзменениеСтатьиДоходов(Объект, Объект);
	
	ЗаполнитьСлужебныеРеквизиты(КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработатьЗагрузкуДанныхИзТСД(РезультатВыполнения, ДополнительныеПараметры) Экспорт
	
	Если РезультатВыполнения.Результат Тогда
		ОбработатьШтрихкоды(РезультатВыполнения.ТаблицаТоваров);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработатьПолучениеВеса(РезультатВыполнения, ТекущаяСтрока) Экспорт
	
	Если РезультатВыполнения.Результат Тогда
		ТекущаяСтрока.КоличествоВЕдиницахИзмерения = РезультатВыполнения.Вес;
		ПриИзмененииКоличестваВСтрокеСпискаТовары(ТекущаяСтрока);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриИзмененииКоличестваВСтрокеСпискаТовары(ТекущаяСтрока)
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьКоличествоЕдиниц());
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьСумму());
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТабличнойЧасти(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеРазбиенияСтроки(НоваяСтрока, ТекущаяСтрока) Экспорт
	
	ПриИзмененииКоличестваВСтрокеСпискаТовары(ТекущаяСтрока);
	ПриИзмененииКоличестваВСтрокеСпискаТовары(НоваяСтрока);
	
	Элементы.Товары.ТекущаяСтрока = НоваяСтрока.ПолучитьИдентификатор();
	
КонецПроцедуры

#КонецОбласти // ОбработкаИзмененияРеквизитов

////////////////////////////////////////////////////////////////////////////////
// Буфер обмена товарами
#Область БуферОбменаТоварами

&НаКлиенте
Процедура СкопироватьСтроки(Команда)
	
	ТаблицаТовары = Элементы.Товары;
	Если ОбработкаТабличнойЧастиКлиент.ВозможноКопированиеСтрок(ТаблицаТовары.ТекущаяСтрока) Тогда
		СкопироватьСтрокиВБуферОбмена(ТаблицаТовары.Имя);
		ОбработкаТабличнойЧастиКлиент.ОповеститьПользователяОКопированииСтрок(ТаблицаТовары.ВыделенныеСтроки.Количество());
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ВставитьСтроки(Команда)
	
	ВставитьТоварыИзБуфераОбмена();
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьБуфераОбмена(Команда)
	
	ОбработкаТабличнойЧастиКлиент.ОткрытьБуферОбмена(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура СкопироватьСтрокиВБуферОбмена(Знач ИмяТабличнойЧасти)
	
	ОбработкаТабличнойЧастиСервер.СкопироватьВыделенныеСтрокиВБуферОбмена(Объект, Объект[ИмяТабличнойЧасти], Элементы[ИмяТабличнойЧасти].ВыделенныеСтроки);
	
КонецПроцедуры

&НаКлиенте
Процедура ВставитьТоварыИзБуфераОбмена(ВыбранныеТовары = Неопределено)
	
	ТаблицаТовары = Объект.Товары;
	КоличествоТоваровДоВставки = ТаблицаТовары.Количество();
	
	ВставитьТоварыИзБуфераОбменаСервер(ВыбранныеТовары);
	
	КоличествоВставленных = ТаблицаТовары.Количество() - КоличествоТоваровДоВставки;
	ОбработкаТабличнойЧастиКлиент.ОповеститьПользователяОВставкеСтрок(КоличествоВставленных);
	
КонецПроцедуры

&НаСервере
Процедура ВставитьТоварыИзБуфераОбменаСервер(Знач ВыбранныеТовары = Неопределено)
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ЗаполнитьЕдиницуИзмерения());
	СтруктураДействий.Вставить(Действия.Действие_ЗаполнитьПараметрыУчета(), ПараметрыУчетаНоменклатуры);
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьКоэффициент());
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьКоличествоЕдиниц());
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьСумму());
	
	ДанныеВставлены = ОбработкаТабличнойЧастиСервер.ВставитьТоварыИзБуфераОбмена(ВыбранныеТовары, Объект.Товары, СтруктураДействий);
	Если ДанныеВставлены Тогда
		Модифицированность = Истина;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьДоступностьКомандБуфераОбмена(Форма, ЕстьДанныеВБуфереОбмена)
	
	Элементы = Форма.Элементы;
	Элементы.ТоварыБуферОбменаВставить.Доступность = ЕстьДанныеВБуфереОбмена;
	Элементы.ТоварыКонтекстноеМенюБуферОбменаВставить.Доступность = ЕстьДанныеВБуфереОбмена;
	Элементы.ТоварыБуферОбмена.Доступность = ЕстьДанныеВБуфереОбмена;
	
КонецПроцедуры

#КонецОбласти // БуферОбменаТоварами

#КонецОбласти // СлужебныеПроцедурыИФункции

////////////////////////////////////////////////////////////////////////////////
// СТАНДАРТНЫЕ ПОДСИСТЕМЫ
#Область СтандартныеПодсистемы

// СтандартныеПодсистемы.ПодключаемыеКоманды

&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	ПодключаемыеКомандыКлиент.ВыполнитьКоманду(ЭтотОбъект, Команда, Объект);
КонецПроцедуры

&НаСервере
Процедура Подключаемый_ВыполнитьКомандуНаСервере(Контекст, Результат)
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, Контекст, Объект, Результат);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
КонецПроцедуры

// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

// ИнтеграцияС1СДокументооборотом

&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуИнтеграции(Команда)
	
	МодульИнтеграцияС1СДокументооборотКлиент = ОбщегоНазначенияКлиент.ОбщийМодуль("ИнтеграцияС1СДокументооборотКлиент");
	МодульИнтеграцияС1СДокументооборотКлиент.ВыполнитьПодключаемуюКомандуИнтеграции(Команда, ЭтотОбъект, Объект);
	
КонецПроцедуры

// Конец ИнтеграцияС1СДокументооборотом

#КонецОбласти // СтандартныеПодсистемы
