
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформлениеФормы();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	УстановитьОтборыСписка();
	
	ЕстьПравоДобавления = ПравоДоступа("Добавление", Метаданные.Справочники.РегистрЛекарственныхСредств);
	Элементы.ФормаОткрытьПоискТоваровВБазеРЛС.Видимость = ЕстьПравоДобавления;
	Элементы.ФормаПринятьДанныеТоваровИзБазыРЛС.Видимость = ЕстьПравоДобавления;
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ОткрытьПоискТоваровВБазеРЛС(Команда)
	
	ИТСМедицинаКлиент.ОткрытьПоискТоваровВБазеРЛС();
	
КонецПроцедуры

&НаКлиенте
Процедура ПринятьДанныеТоваровИзБазыРЛС(Команда)
	
	ИТСМедицинаКлиент.ПринятьДанныеТоваровИзБазыРЛС();
	
КонецПроцедуры

#КонецОбласти // ОбработчикиКомандФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ
#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура ОтборНеПривязанныеКДискуИТСПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, Неопределено,,, "СозданныеВРучную", ОтборНеПривязанныеКДискуИТС);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборНеПривязанныеКНоменклатуреПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, Неопределено,,, "НеПривязанныеКНоменклатуре", ОтборНеПривязанныеКНоменклатуре);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийЭлементовФормы

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформлениеФормы()
	
	УсловноеОформлениеСписка = ОбщегоНазначенияБольничнаяАптекаКлиентСервер.ПолучитьУсловноеОформлениеДинамическогоСписка(
		Список, РежимОтображенияЭлементаНастройкиКомпоновкиДанных.Обычный);
	
	// Цвет фона в строках списка Список
	Элемент =УсловноеОформлениеСписка.Элементы.Добавить();
	Элемент.Представление = НСтр("ru = 'Элементы, не связанные с диском ИТС'");
	
	ОбщегоНазначенияКлиентСервер.ДобавитьЭлементКомпоновки(Элемент.Отбор,
		"НомерРЛС", ВидСравненияКомпоновкиДанных.Равно, 0);
	
	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветФона", ЦветаСтиля.ЦветНедоступногоТекста);
	
	// Цвет фона в строках списка Список
	Элемент = УсловноеОформлениеСписка.Элементы.Добавить();
	Элемент.Представление = НСтр("ru = 'Регистрация не действует'");
	
	Статусы = Новый СписокЗначений;
	Статусы.Добавить(2);
	Статусы.Добавить(3);
	Статусы.Добавить(4);
	Статусы.Добавить(10);
	Статусы.Добавить(99);
	
	ОбщегоНазначенияКлиентСервер.ДобавитьЭлементКомпоновки(Элемент.Отбор,
		"СтатусДействия", ВидСравненияКомпоновкиДанных.ВСписке, Статусы);
	
	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветФона", ЦветаСтиля.ПолеСОшибкойФон);
	
КонецПроцедуры

&НаСервере
Процедура УстановитьОтборыСписка()
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "НомерРЛС", 0,, "СозданныеВРучную", Ложь);
	Группа = ОбщегоНазначенияКлиентСервер.СоздатьГруппуЭлементовОтбора(
		Список.КомпоновщикНастроек.ФиксированныеНастройки.Отбор.Элементы, "ГруппаНеПривязанныеКНоменклатуре", ТипГруппыЭлементовОтбораКомпоновкиДанных.ГруппаИли);
	ОбщегоНазначенияКлиентСервер.ДобавитьЭлементКомпоновки(Группа, "Номенклатура",ВидСравненияКомпоновкиДанных.НеЗаполнено,, "НеПривязанныеКНоменклатуре", Ложь);
	ОбщегоНазначенияКлиентСервер.ДобавитьЭлементКомпоновки(Группа, "Номенклатура",ВидСравненияКомпоновкиДанных.Равно,, "НеПривязанныеКНоменклатуреОбъект", Ложь);
	
	Если Параметры.ОтборНеПривязанныеКДискуИТС Тогда
		ОтборНеПривязанныеКДискуИТС = Истина;
		Элементы.ОтборНеПривязанныеКДискуИТС.Доступность = Ложь;
	КонецЕсли;
	Если Параметры.ОтборНеПривязанныеКНоменклатуре Тогда
		ОтборНеПривязанныеКНоменклатуре = Истина;
		Элементы.ОтборНеПривязанныеКНоменклатуре.Доступность = Ложь;
	КонецЕсли;
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, Неопределено,,, "СозданныеВРучную", ОтборНеПривязанныеКДискуИТС);
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, Неопределено,,, "НеПривязанныеКНоменклатуре", ОтборНеПривязанныеКНоменклатуре);
	
	Если Параметры.Свойство("Номенклатура") Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, Неопределено, Параметры.Номенклатура,, "НеПривязанныеКНоменклатуреОбъект", Истина);
	КонецЕсли;
	
	Если Параметры.Отбор.Свойство("Штрихкод") Тогда
		КлючНазначенияИспользования = "ПоискПоШтрихкоду";
		Список.АвтоматическоеСохранениеПользовательскихНастроек = Ложь;
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбора(
			Список.КомпоновщикНастроек.Настройки.Отбор, "Штрихкод",
			Параметры.Отбор.Штрихкод,
			ВидСравненияКомпоновкиДанных.Содержит,
			,
			,
			РежимОтображенияЭлементаНастройкиКомпоновкиДанных.БыстрыйДоступ);
		Параметры.Отбор.Удалить("Штрихкод");
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти // СлужебныеПроцедурыИФункции
