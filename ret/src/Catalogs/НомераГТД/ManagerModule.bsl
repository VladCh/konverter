#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

// Процедура создает номер ГТД номенклатуры.
//
// Параметры:
//	Параметры - Коллекция - Коллекция параметров для создания номера ГТД
//
Процедура СоздатьНомерГТД(Параметры) Экспорт

	Если ЗначениеЗаполнено(Параметры.СтранаПроисхождения)
	 ИЛИ Не ПустаяСтрока(Параметры.Код) Тогда
	
		СправочникОбъект = Справочники.НомераГТД.СоздатьЭлемент();
		ЗаполнитьЗначенияСвойств(СправочникОбъект, Параметры, "
		|Код,
		|СтранаПроисхождения
		|");
		СправочникОбъект.Записать();
		
	КонецЕсли;

КонецПроцедуры

// Функция определяет наличие номера ГТД по коду.
//
// Параметры:
//	СтрокаНомерГТД - Строка - Код номера ГТД
//	Номенклатура - СправочникСсылка.Номенклатура - Выбранный товар
//
// Возвращаемое значение:
//	Булево - Истина - Есть номер ГТД с указанным кодом
//
Функция ЕстьНомерГТД(СтрокаНомерГТД) Экспорт
	
	ЕстьНомерГТД = Ложь;
	
	Если Не ПустаяСтрока(СтрокаНомерГТД) Тогда
	
		Запрос = Новый Запрос("
		|ВЫБРАТЬ ПЕРВЫЕ 1
		|	ИСТИНА КАК ЕстьНомерГТД
		|ИЗ
		|	Справочник.НомераГТД КАК НомераГТД
		|ГДЕ
		|	НомераГТД.Код ПОДОБНО &СтрокаНомерГТД
		|");
		Запрос.УстановитьПараметр("СтрокаНомерГТД", "%"+СокрЛП(СтрокаНомерГТД)+"%");
		
		Результат = Запрос.Выполнить();
		Если Не Результат.Пустой() Тогда
			ЕстьНомерГТД = Истина;
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат ЕстьНомерГТД;
	
КонецФункции

#КонецОбласти // ПрограммныйИнтерфейс

#КонецЕсли