
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	ТипЛинии = Параметры.ТипЛинии;
	Толщина = Параметры.Толщина;
	
	Если Толщина = 0 Тогда
		Толщина = 1;
	КонецЕсли;
	
	Если ТипЗнч(Параметры.ТипЛинии) = Тип("ТипЛинииЯчейкиТабличногоДокумента") Тогда
		Типы = ТипЛинииЯчейкиТабличногоДокумента;
		Элементы.ПримерЛинииЯчейки.Видимость = Истина;
		Элементы.ПримерЛинииРисунка.Видимость = Ложь;
	ИначеЕсли ТипЗнч(Параметры.ТипЛинии) = Тип("ТипЛинииРисункаТабличногоДокумента") Тогда
		Типы = ТипЛинииРисункаТабличногоДокумента;
		Элементы.ПримерЛинииЯчейки.Видимость = Ложь;
		Элементы.ПримерЛинииРисунка.Видимость = Истина;
	КонецЕсли;
	
	Для Каждого Тип Из Типы Цикл
		ТипыЛинии.Добавить(Тип);
	КонецЦикла;
	
	Элементы.ТипыЛинии.ТекущаяСтрока = ТипыЛинии.Индекс(ТипыЛинии.НайтиПоЗначению(ТипЛинии));
	
	Граница = Новый Линия(ТипЛинии, Толщина);
	Если ТипЗнч(ТипЛинии) = Тип("ТипЛинииЯчейкиТабличногоДокумента") Тогда
		ПримерЛинииЯчейки.Область("ПримерЛинии").Обвести(Граница, Граница, Граница, Граница);
	Иначе
		ПримерЛинииРисунка.Область("ПримерЛинии").Линия = Граница;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаОК(Команда)
	
	Закрыть(Новый Структура("ТипЛинии, Толщина", ТипЛинии, Толщина));
	
КонецПроцедуры

#КонецОбласти // ОбработчикиКомандФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ
#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура ТипЛинииПриАктивизацииСтроки(Элемент)
	
	ТипЛинии = Элементы.ТипыЛинии.ТекущиеДанные.Значение;
	Если ТипЛинии = ТипЛинииЯчейкиТабличногоДокумента.НетЛинии
	 Или ТипЛинии = ТипЛинииРисункаТабличногоДокумента.НетЛинии Тогда
		Толщина = 0;
	ИначеЕсли ТипЛинии = ТипЛинииЯчейкиТабличногоДокумента.Двойная Тогда
		Толщина = 1;
	ИначеЕсли Толщина = 0 Тогда
		Толщина = 1;
	КонецЕсли;
	
	ПодключитьОбработчикОжидания("ПоказатьЛинию", 0.1, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура ТолщинаПриИзменении(Элемент)
	
	ТипЛинии = Элементы.ТипыЛинии.ТекущиеДанные.Значение;
	Если ТипЛинии = ТипЛинииЯчейкиТабличногоДокумента.НетЛинии
	 Или ТипЛинии = ТипЛинииРисункаТабличногоДокумента.НетЛинии Тогда
		Толщина = 0;
	ИначеЕсли ТипЛинии = ТипЛинииЯчейкиТабличногоДокумента.Двойная Тогда
		Толщина = 1;
	КонецЕсли;
	
	ПодключитьОбработчикОжидания("ПоказатьЛинию", 0.1, Истина);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийЭлементовФормы

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ПоказатьЛинию()
	
	Граница = Новый Линия(ТипЛинии, Толщина);
	Если ТипЗнч(ТипЛинии) = Тип("ТипЛинииЯчейкиТабличногоДокумента") Тогда
		ПримерЛинииЯчейки.Область("ПримерЛинии").Обвести(Граница, Граница, Граница, Граница);
	Иначе
		ПримерЛинииРисунка.Область("ПримерЛинии").Линия = Граница;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти // СлужебныеПроцедурыИФункции
