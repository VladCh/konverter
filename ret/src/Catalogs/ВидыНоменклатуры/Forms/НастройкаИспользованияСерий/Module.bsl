
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	ИменаСохраняемыхРеквизитов = Справочники.ВидыНоменклатуры.ИменаРеквизитовДляФормыНастройкаИспользованияСерий();
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, Параметры, ИменаСохраняемыхРеквизитов);
	
	УстановитьДоступностьПолей();
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаОК(Команда)
	
	Если Не ИспользоватьНомерСерии
	   И Не ИспользоватьСрокГодностиСерии Тогда
		ТекстСообщения = НСтр("ru = 'Не выполнена настройка использования реквизитов серии.'");
		ПоказатьПредупреждение(Неопределено, ТекстСообщения);
		Возврат;
	КонецЕсли;
	
	Результат = Новый Структура(ИменаСохраняемыхРеквизитов);
	ЗаполнитьЗначенияСвойств(Результат, ЭтотОбъект);
	
	Закрыть(Результат);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиКомандФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ
#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура ИспользоватьСрокГодностиСерииПриИзменении(Элемент)
	
	Если ИспользоватьСрокГодностиСерии Тогда
		ТочностьУказанияСрокаГодностиСерии = ПредопределенноеЗначение("Перечисление.ТочностиУказанияСрокаГодности.СТочностьюДоДней");
	Иначе
		ТочностьУказанияСрокаГодностиСерии = ПредопределенноеЗначение("Перечисление.ТочностиУказанияСрокаГодности.ПустаяСсылка");
	КонецЕсли;
	
	УстановитьДоступностьПолей();
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийЭлементовФормы

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьДоступностьПолей()
	
	Элементы.ТочностьУказанияСрокаГодностиСерии.Доступность = ИспользоватьСрокГодностиСерии;
	
КонецПроцедуры

#КонецОбласти // СлужебныеПроцедурыИФункции
