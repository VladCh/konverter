
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	ПоказыватьНедействительныхПользователей = Ложь;
	
	Если Не Параметры.РежимВыбора Тогда
		
		// только если не режим выбора, делаем фильтрацию
		Если ПоказыватьНедействительныхПользователей Тогда
			ОбщегоНазначенияКлиентСервер.УдалитьЭлементыГруппыОтбораДинамическогоСписка(ПользователиСписок, "Недействителен");
		Иначе
			ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
				ПользователиСписок,
				"Недействителен",
				Ложь);
		КонецЕсли;
			
	КонецЕсли;
	
	ИспользоватьГруппы = ПолучитьФункциональнуюОпцию("ИспользоватьГруппыПользователей");
	
	Если ТипЗнч(Параметры.ТекущаяСтрока) = Тип("СправочникСсылка.ГруппыПользователей") Тогда
		Если ИспользоватьГруппы Тогда
			Элементы.ГруппыПользователей.ТекущаяСтрока = Параметры.ТекущаяСтрока;
		Иначе
			Параметры.ТекущаяСтрока = Неопределено;
		КонецЕсли;
	Иначе
		ТекущийЭлемент = Элементы.ПользователиСписок;
		Элементы.ГруппыПользователей.ТекущаяСтрока = Справочники.ГруппыПользователей.ВсеПользователи;
		ОбновитьЗначениеПараметраКомпоновкиДанных(ПользователиСписок, "ГруппаПользователей", Справочники.ГруппыПользователей.ВсеПользователи);
		ОбновитьЗначениеПараметраКомпоновкиДанных(ПользователиСписок, "ВыбиратьИерархически", Истина);
	КонецЕсли;
	
	// Настройка постоянных данных для списка пользователей
	ОбновитьЗначениеПараметраКомпоновкиДанных(ПользователиСписок, "ПустойУникальныйИдентификатор", Новый УникальныйИдентификатор("00000000-0000-0000-0000-000000000000"));
	ГруппаПользователейВсеПользователи = Справочники.ГруппыПользователей.ВсеПользователи;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	ОбновитьСодержимоеФормыПриИзмененииГруппы();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "Запись_ГруппыПользователей" Тогда
		Если Источник = Элементы.ГруппыПользователей.ТекущаяСтрока Тогда
			Элементы.ПользователиСписок.Обновить();
		КонецЕсли;
	КонецЕсли;
	
	Если ИмяСобытия = "Запись_НастройкиПродажДляПользователей" Тогда
		Элементы.ПользователиСписок.Обновить();
		Элементы.ГруппыПользователей.Обновить();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	
	Если Настройки[ВыбиратьИерархически] = Неопределено Тогда
		ВыбиратьИерархически = Истина;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПоказыватьНедействительныхПользователей(Команда)
	
	ПоказыватьНедействительныхПользователей = Не ПоказыватьНедействительныхПользователей;
	
	Элементы.ФормаПоказыватьНедействительныхПользователей.Пометка = ПоказыватьНедействительныхПользователей;
	
	Если ПоказыватьНедействительныхПользователей Тогда
		ОбщегоНазначенияКлиентСервер.УдалитьЭлементыГруппыОтбораДинамическогоСписка(ПользователиСписок, "Недействителен");
	Иначе
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
			ПользователиСписок,
			"Недействителен",
			Ложь);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ
#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура ГруппыПользователейПриАктивизацииСтроки(Элемент)
	
	ОбновитьСодержимоеФормыПриИзмененииГруппы();
	
КонецПроцедуры

&НаКлиенте
Процедура ВыбиратьИерархическиПриИзменении(Элемент)
	
	ОбновитьСодержимоеФормыПриИзмененииГруппы();
	
КонецПроцедуры

&НаКлиенте
Процедура ГруппыПользователейВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ТекущиеДанные = Элементы.ГруппыПользователей.ТекущиеДанные;
	
	Ссылка = РозничныеПродажиВызовСервера.НастройкаОграниченийПользователя(ТекущиеДанные.Ссылка);
	Если Ссылка = Неопределено Тогда
		ПараметрыФормы = Новый Структура("ЗначенияЗаполнения", Новый Структура("Владелец", ТекущиеДанные.Ссылка));
	Иначе
		ПараметрыФормы = Новый Структура("Ключ", Ссылка);
	КонецЕсли;
	ПараметрыФормы.Вставить("ИмяПоля", Поле.Имя);
	ОткрытьФорму("Справочник.НастройкиПродажДляПользователей.Форма.ФормаЭлемента", ПараметрыФормы);
	
КонецПроцедуры

&НаКлиенте
Процедура ПользователиСписокВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ТекущиеДанные = Элементы.ПользователиСписок.ТекущиеДанные;
	
	Ссылка = РозничныеПродажиВызовСервера.НастройкаОграниченийПользователя(ТекущиеДанные.Ссылка);
	Если Ссылка = Неопределено Тогда
		ПараметрыФормы = Новый Структура("ЗначенияЗаполнения", Новый Структура("Владелец", ТекущиеДанные.Ссылка));
	Иначе
		ПараметрыФормы = Новый Структура("Ключ", Ссылка);
	КонецЕсли;
	ПараметрыФормы.Вставить("ИмяПоля", Поле.Имя);
	ОткрытьФорму("Справочник.НастройкиПродажДляПользователей.Форма.ФормаЭлемента", ПараметрыФормы);
	
КонецПроцедуры

#КонецОбласти

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	//
	
	Элемент = ПользователиСписок.КомпоновщикНастроек.Настройки.УсловноеОформление.Элементы.Добавить();
	
	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Недействителен");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Истина;
	
	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветТекста", ЦветаСтиля.ТекстЗапрещеннойЯчейкиЦвет);
	
	//
	
	Элемент = ПользователиСписок.КомпоновщикНастроек.ФиксированныеНастройки.УсловноеОформление.Элементы.Добавить();
	
	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных("РМК_Использовать");
	
	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("РМК_Использовать");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Истина;
	
	Элемент.Оформление.УстановитьЗначениеПараметра("Текст", НСтр("ru = 'Есть ограничения'"));
	
	//
	
	Элемент = ПользователиСписок.КомпоновщикНастроек.ФиксированныеНастройки.УсловноеОформление.Элементы.Добавить();
	
	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных("РМК_Использовать");
	
	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("РМК_Использовать");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.НеРавно;
	ОтборЭлемента.ПравоеЗначение = Истина;
	
	Элемент.Оформление.УстановитьЗначениеПараметра("Текст", НСтр("ru = 'Нет ограничений'"));
	
	//
	
	Элемент = ГруппыПользователей.КомпоновщикНастроек.ФиксированныеНастройки.УсловноеОформление.Элементы.Добавить();
	
	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных("РМК_Использовать");
	
	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("РМК_Использовать");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Истина;
	
	Элемент.Оформление.УстановитьЗначениеПараметра("Текст", НСтр("ru = 'Есть ограничения'"));
	
	//
	
	Элемент = ГруппыПользователей.КомпоновщикНастроек.ФиксированныеНастройки.УсловноеОформление.Элементы.Добавить();
	
	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных("РМК_Использовать");
	
	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("РМК_Использовать");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.НеРавно;
	ОтборЭлемента.ПравоеЗначение = Истина;
	
	Элемент.Оформление.УстановитьЗначениеПараметра("Текст", НСтр("ru = 'Нет ограничений'"));
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьСодержимоеФормыПриИзмененииГруппы()
	
	Если Не ИспользоватьГруппы
	 Или Элементы.ГруппыПользователей.ТекущаяСтрока = ГруппаПользователейВсеПользователи Тогда
		//
		Элементы.ГруппаПоказыватьПользователейДочернихГрупп.ТекущаяСтраница = Элементы.ГруппаНельзяУстановитьСвойство;
		ОбновитьЗначениеПараметраКомпоновкиДанных(ПользователиСписок, "ВыбиратьИерархически", Истина);
		ОбновитьЗначениеПараметраКомпоновкиДанных(ПользователиСписок, "ГруппаПользователей", ГруппаПользователейВсеПользователи);
	Иначе
		Элементы.ГруппаПоказыватьПользователейДочернихГрупп.ТекущаяСтраница = Элементы.ГруппаУстановитьСвойство;
		ОбновитьЗначениеПараметраКомпоновкиДанных(ПользователиСписок, "ВыбиратьИерархически", ВыбиратьИерархически);
		ОбновитьЗначениеПараметраКомпоновкиДанных(ПользователиСписок, "ГруппаПользователей", Элементы.ГруппыПользователей.ТекущаяСтрока);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ОбновитьЗначениеПараметраКомпоновкиДанных(Знач ВладелецПараметров, Знач ИмяПараметра, Знач ЗначениеПараметра)
	
	Для Каждого Параметр Из ВладелецПараметров.Параметры.Элементы Цикл
		Если Строка(Параметр.Параметр) = ИмяПараметра Тогда
			Если Параметр.Использование И Параметр.Значение = ЗначениеПараметра Тогда
				Возврат;
			КонецЕсли;
			Прервать;
		КонецЕсли;
	КонецЦикла;
	
	ВладелецПараметров.Параметры.УстановитьЗначениеПараметра(ИмяПараметра, ЗначениеПараметра);
	
КонецПроцедуры

#КонецОбласти
