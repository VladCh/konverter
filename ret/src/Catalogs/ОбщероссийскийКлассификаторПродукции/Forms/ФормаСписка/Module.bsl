
///////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Элементы.ФормаЗагрузитьКлассификатор.Видимость = ПравоДоступа("Добавление", Метаданные.Справочники.ОбщероссийскийКлассификаторПродукции);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийФормы

///////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗагрузитьКлассификатор(Команда)
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("ПустойЭлементСправочника"        , ПредопределенноеЗначение("Справочник.ОбщероссийскийКлассификаторПродукции.ПустаяСсылка"));
	ПараметрыФормы.Вставить("ИмяФайлаПоУмолчанию"             , "okp.dbf");
	ПараметрыФормы.Вставить("ДискИТСДоступен"                 , Истина);
	ПараметрыФормы.Вставить("КаталогФайлаНаИТС"               , "OKP");
	ПараметрыФормы.Вставить("МаксимальныйУровеньПодчиненности", 4);
	
	ОткрытьФорму("Обработка.ЗагрузкаКлассификаторов.Форма.ЗагрузкаКлассификаторов", ПараметрыФормы, ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиКомандФормы
