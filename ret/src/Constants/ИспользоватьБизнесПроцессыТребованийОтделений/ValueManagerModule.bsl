#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

Процедура ПриОпределенииЗависимостиКонстант(ТаблицаКонстант) Экспорт
	
	ИмяКонстанты = Метаданные().Имя;
	
	ОбщегоНазначенияБольничнаяАптека.ДобавитьЗависимостьКонстант(ТаблицаКонстант,
		ИмяКонстанты                                              , Истина,
		Метаданные.Константы.ИспользоватьБизнесПроцессыИЗадачи.Имя, Истина);
	
	ОбщегоНазначенияБольничнаяАптека.ДобавитьЗависимостьКонстант(ТаблицаКонстант,
		ИмяКонстанты                                                  , Истина,
		Метаданные.Константы.ИспользоватьПодчиненныеБизнесПроцессы.Имя, Истина);
	
КонецПроцедуры

#КонецОбласти // ПрограммныйИнтерфейс

#КонецЕсли
