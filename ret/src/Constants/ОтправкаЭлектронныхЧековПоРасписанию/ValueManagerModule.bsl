#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

Процедура ПриОпределенииЗависимостиКонстант(ТаблицаКонстант) Экспорт
	
	ИмяКонстанты = Метаданные().Имя;
	
	ОбщегоНазначенияБольничнаяАптека.ДобавитьЗависимостьКонстант(ТаблицаКонстант,
		Метаданные.Константы.ОтправкаЭлектронныхЧековПослеПробитияЧека.Имя, Истина,
		ИмяКонстанты                                                      , Ложь);
	
	ОбщегоНазначенияБольничнаяАптека.ДобавитьЗависимостьКонстант(ТаблицаКонстант,
		Метаданные.Константы.ОтправкаЭлектронныхЧековПослеПробитияЧека.Имя, Ложь,
		ИмяКонстанты                                                      , Истина);
	
КонецПроцедуры

#КонецОбласти // ПрограммныйИнтерфейс

#КонецЕсли
