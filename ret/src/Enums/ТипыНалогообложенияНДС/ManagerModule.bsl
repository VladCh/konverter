
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ
#Область ОбработчикиСобытий

Процедура ОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ДанныеВыбора = Новый СписокЗначений;
	ДанныеВыбора.Добавить(Перечисления.ТипыНалогообложенияНДС.ПродажаОблагаетсяНДС);
	ДанныеВыбора.Добавить(Перечисления.ТипыНалогообложенияНДС.ПродажаОблагаетсяЕНВД);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытий
