
////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

// Вызывает диалог выбора файла
//
// Параметры:
//  Форма       - УправляемаяФорма - вызывающий объект
//  ПутьКДанным - Строка - полное имя реквизита формы, содержащего значение каталога. Например "ПутьКФайлу"
//                         или "Объект.ИмяФайла"
//  Фильтр      - Строка - Содержит строку с набором файловых фильтров.
//                         Пример строки: "Текстовый документ(*.txt)|*.txt|Табличный документ(*.mxl)|*.mxl"
//  Заголовок   - Строка - Заголовок диалога выбора
//
Процедура ВыбратьСуществующийФайл(Знач Форма, Знач ПутьКДанным, Знач Фильтр, Знач Заголовок = Неопределено) Экспорт
	
	Оповещение = Новый ОписаниеОповещения("ВыбратьФайлКаталогЗавершение", ЭтотОбъект, Новый Структура);
	Оповещение.ДополнительныеПараметры.Вставить("Форма", Форма);
	Оповещение.ДополнительныеПараметры.Вставить("ПутьКДанным", ПутьКДанным);
	
	ИмяФайла = ОбщегоНазначенияБольничнаяАптекаКлиентСервер.ЗначениеРеквизитаФормы(Форма, ПутьКДанным);
	
	ВыбратьФайлСКонтролемРасширенияРаботыСФайлами(Оповещение, ИмяФайла, Фильтр, Заголовок, Ложь, Истина);
	
КонецПроцедуры

// Вызывает диалог выбора файла
//
// Параметры:
//  Форма       - УправляемаяФорма - вызывающий объект
//  ПутьКДанным - Строка - полное имя реквизита формы, содержащего значение каталога. Например "РабочийКаталог" 
//                         или "Объект.КаталогИзображений"
//  Заголовок   - Строка - Заголовок диалога выбора
//
Процедура ВыбратьКаталог(Знач Форма, Знач ПутьКДанным, Знач Заголовок = Неопределено) Экспорт
	
	Оповещение = Новый ОписаниеОповещения("ВыбратьФайлКаталогЗавершение", ЭтотОбъект, Новый Структура);
	Оповещение.ДополнительныеПараметры.Вставить("Форма", Форма);
	Оповещение.ДополнительныеПараметры.Вставить("ПутьКДанным", ПутьКДанным);
	
	Каталог = ОбщегоНазначенияБольничнаяАптекаКлиентСервер.ЗначениеРеквизитаФормы(Форма, ПутьКДанным);
	
	ВыбратьКаталогСКонтролемРасширенияРаботыСФайлами(Оповещение, Каталог, Заголовок);
	
КонецПроцедуры

// Удаляет файл
//
Процедура УдалитьФайлНаКлиенте(ПутьКДаннымНаКлиенте, Оповещение = Неопределено) Экспорт
	
	НачатьУдалениеФайлов(Оповещение, ПутьКДаннымНаКлиенте);
	
КонецПроцедуры

// Получает асинхронно имя временного файла
//
Процедура ИмяВременногоФайла(Оповещение, Знач Расширение = "") Экспорт
	
	#Если ВебКлиент Тогда
		Параметры = Новый Структура;
		Параметры.Вставить("Расширение", Расширение);
		Параметры.Вставить("Оповещение", Оповещение);
		
		Оповестить = Новый ОписаниеОповещения("ИмяВременногоФайлаЗавершение", ЭтотОбъект, Параметры);
		НачатьПолучениеКаталогаВременныхФайлов(Оповестить);
		
	#Иначе
		ВыполнитьОбработкуОповещения(Оповещение, ПолучитьИмяВременногоФайла(Расширение));
	#КонецЕсли
	
КонецПроцедуры

// Завершение получения имени временного файла
//
Процедура ИмяВременногоФайлаЗавершение(Каталог, Параметры) Экспорт
	
	Расширение = Параметры.Расширение;
	Если ЗначениеЗаполнено(Расширение) Тогда
		Расширение = ?(Лев(Расширение, 1) = ".", "", ".") + Расширение;
	Иначе
		Расширение = "";
	КонецЕсли;
	
	ИмяВременногоФайла = ОбщегоНазначенияКлиентСервер.ДобавитьКонечныйРазделительПути(Каталог)
		+ ОбщегоНазначенияБольничнаяАптекаКлиентСервер.СформироватьУникальныйИдентификатор()
		+ Расширение;
	ВыполнитьОбработкуОповещения(Параметры.Оповещение, ИмяВременногоФайла);
	
КонецПроцедуры

Процедура ДиалогЗагрузкиФайла(ОповещениеЗавершения, ИмяФайла = "", Фильтр = Неопределено) Экспорт
	
	ДиалогВыборФайла = Новый Структура;
	ДиалогВыборФайла.Вставить("Режим" , РежимДиалогаВыбораФайла.Открытие);
	ДиалогВыборФайла.Вставить("Фильтр", Фильтр);
	
	Идентификатор = ОповещениеЗавершения.Модуль.УникальныйИдентификатор;
	ПоказатьПомещениеФайла(ОповещениеЗавершения, Идентификатор, ИмяФайла, ДиалогВыборФайла);
	
КонецПроцедуры

// Показывает диалог выбора файлов и помещает выбранные файлы во временное хранилище.
//  Совмещает работу методов глобального метода НачатьПомещениеФайла и ПоместитьФайлы,
//  возвращая идентичный результат вне зависимости от того, подключено расширение работы с файлами, или нет.
//
// Параметры:
//   ОбработчикЗавершения  - ОписаниеОповещения - Описание процедуры, принимающей результат выбора.
//   ИдентификаторФормы    - УникальныйИдентификатор - Уникальный идентификатор формы, из которой выполняется
//                                                     размещение файла.
//   НачальноеИмяФайла     - Строка - Полный путь и имя файла, которые будут предложены пользователю в начале выбора.
//   ПараметрыДиалога      - Структура, Неопределено - См. свойства ДиалогВыбораФайла в синтакс-помощнике.
//       Используется в случае, если удалось подключить расширение работы с файлами.
//
// Значение первого параметра, возвращаемого в ОбработчикРезультата:
//   ПомещенныеФайлы - Результат выбора.
//       * - Неопределено - Пользователь отказался от выбора.
//       * - Массив из ОписаниеПереданногоФайла, Структура - Пользователь выбрал файл.
//           ** Имя      - Строка - Полное имя выбранного файла.
//           ** Хранение - Строка - Адрес во временном хранилище, по которому размещен файл.
//
// Ограничения:
//   Используется только для интерактивного выбора в диалоге.
//   Не используется для выбора каталогов - эта опция не поддерживается веб-клиентом.
//   Не поддерживается множественный выбор в веб-клиенте, если не установлено расширение работы с файлами.
//   Не поддерживается передача адреса временного хранилища.
//
Процедура ПоказатьПомещениеФайла(ОбработчикЗавершения, ИдентификаторФормы, НачальноеИмяФайла, ПараметрыДиалога) Экспорт
	
	Параметры = Новый Структура;
	Параметры.Вставить("ОбработчикЗавершения", ОбработчикЗавершения);
	Параметры.Вставить("ИдентификаторФормы", ИдентификаторФормы);
	Параметры.Вставить("НачальноеИмяФайла", НачальноеИмяФайла);
	Параметры.Вставить("ПараметрыДиалога", ПараметрыДиалога);
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ПоказатьПомещениеФайлаПриПодключенииРасширенияРаботыСФайлами", ЭтотОбъект, Параметры);
	ОбщегоНазначенияКлиент.ПоказатьВопросОбУстановкеРасширенияРаботыСФайлами(ОписаниеОповещения);
	
КонецПроцедуры

#КонецОбласти // ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

// Вызывает диалог выбора файла
//
// Параметры:
//  Оповещение                  - ОписаниеОповещения - описание процедуры, которая будет вызвана после закрытия формы выбора файла
//  НачальноеПолноеИмяФайла     - Строка - начальный путь к файлу или пустая строка
//  Фильтр                      - Строка - Содержит строку с набором файловых фильтров.
//                                         Пример строки: "Текстовый документ(*.txt)|*.txt|Табличный документ(*.mxl)|*.mxl"
//  Заголовок                   - Строка - Заголовок диалога выбора файла
//  МножественныйВыбор          - Булево
//  ПроверятьСуществованиеФайла - Булево
//
Процедура ВыбратьФайлСКонтролемРасширенияРаботыСФайлами(Оповещение, ИмяФайла = "", Фильтр = "", Заголовок = Неопределено, МножественныйВыбор = Ложь, ТолькоСуществующие = Ложь) Экспорт
	
	ОповещениеПослеКонтроля = Новый ОписаниеОповещения("ВыбратьФайлЗавершениеКонтроляРасширенияРаботыСФайлами", ЭтотОбъект, Новый Структура);
	ОповещениеПослеКонтроля.ДополнительныеПараметры.Вставить("ИмяФайла", ИмяФайла);
	ОповещениеПослеКонтроля.ДополнительныеПараметры.Вставить("Фильтр", Фильтр);
	ОповещениеПослеКонтроля.ДополнительныеПараметры.Вставить("Заголовок", Заголовок);
	ОповещениеПослеКонтроля.ДополнительныеПараметры.Вставить("МножественныйВыбор", МножественныйВыбор);
	ОповещениеПослеКонтроля.ДополнительныеПараметры.Вставить("ПроверятьСуществованиеФайла", ТолькоСуществующие);
	ОповещениеПослеКонтроля.ДополнительныеПараметры.Вставить("Оповещение", Оповещение);
	
	ОбщегоНазначенияКлиент.ПоказатьВопросОбУстановкеРасширенияРаботыСФайлами(ОповещениеПослеКонтроля,, Ложь);
	
КонецПроцедуры

// Завершение немодального контроля расширения работы с файлами перед выбором файла
//
Процедура ВыбратьФайлЗавершениеКонтроляРасширенияРаботыСФайлами(Подключено, ДополнительныеПараметры) Экспорт
	
	Если Не Подключено Тогда
		ВыполнитьОбработкуОповещения(ДополнительныеПараметры.Оповещение, Неопределено);
		Возврат;
	КонецЕсли;
	
	ДиалогВыбораФайла = Новый ДиалогВыбораФайла(РежимДиалогаВыбораФайла.Открытие);
	ДиалогВыбораФайла.Заголовок = ДополнительныеПараметры.Заголовок;
	ДиалогВыбораФайла.Фильтр = ДополнительныеПараметры.Фильтр;
	ДиалогВыбораФайла.МножественныйВыбор = ДополнительныеПараметры.МножественныйВыбор;
	ДиалогВыбораФайла.ПроверятьСуществованиеФайла = ДополнительныеПараметры.ПроверятьСуществованиеФайла;
	
	НачальноеПолноеИмяФайла = ДополнительныеПараметры.ИмяФайла;
	Если Не ПустаяСтрока(НачальноеПолноеИмяФайла) Тогда
		Если Прав(НачальноеПолноеИмяФайла, 1) = "\" Или Прав(НачальноеПолноеИмяФайла, 1) = "/" Тогда
			ДиалогВыбораФайла.Каталог = НачальноеПолноеИмяФайла;
		Иначе
			ДиалогВыбораФайла.ПолноеИмяФайла = НачальноеПолноеИмяФайла;
		КонецЕсли;
	КонецЕсли;
	
	ДиалогВыбораФайла.Показать(ДополнительныеПараметры.Оповещение)
	
КонецПроцедуры

// Вызывает диалог выбора каталога
//
// Параметры:
//  Оповещение - ОписаниеОповещения - описание процедуры, которая будет вызвана после закрытия формы выбора каталога
//  Каталог    - начальный каталог
//  Заголовок  - заголовок окна выбора
//
Процедура ВыбратьКаталогСКонтролемРасширенияРаботыСФайлами(Оповещение, Каталог, Заголовок = Неопределено) Экспорт
	
	ОповещениеПослеКонтроля = Новый ОписаниеОповещения("ВыбратьКаталогЗавершениеКонтроляРасширенияРаботыСФайлами", ЭтотОбъект, Новый Структура);
	ОповещениеПослеКонтроля.ДополнительныеПараметры.Вставить("Каталог", Каталог);
	ОповещениеПослеКонтроля.ДополнительныеПараметры.Вставить("Заголовок", Заголовок);
	ОповещениеПослеКонтроля.ДополнительныеПараметры.Вставить("Оповещение", Оповещение);
	
	ОбщегоНазначенияКлиент.ПоказатьВопросОбУстановкеРасширенияРаботыСФайлами(ОповещениеПослеКонтроля,, Ложь);
	
КонецПроцедуры

// Завершение немодального контроля расширения работы с файлами перед выбором каталога
//
Процедура ВыбратьКаталогЗавершениеКонтроляРасширенияРаботыСФайлами(Подключено, ДополнительныеПараметры) Экспорт
	
	Если Не Подключено Тогда
		ВыполнитьОбработкуОповещения(ДополнительныеПараметры.Оповещение, Неопределено);
		Возврат;
	КонецЕсли;
	
	ДиалогВыбораКаталога = Новый ДиалогВыбораФайла(РежимДиалогаВыбораФайла.ВыборКаталога);
	ДиалогВыбораКаталога.Заголовок = ДополнительныеПараметры.Заголовок;
	ДиалогВыбораКаталога.Каталог = ДополнительныеПараметры.Каталог;
	
	ДиалогВыбораКаталога.Показать(ДополнительныеПараметры.Оповещение);
	
КонецПроцедуры

// Завершение немодального выбора файла
//
Процедура ВыбратьФайлКаталогЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ВыбранноеИмя = ?(Результат.Количество() = 1, Результат[0], Результат);
	
	ТекущийЭлемент = ДополнительныеПараметры.Форма;
	
	ЧастиПути = СтрЗаменить(ДополнительныеПараметры.ПутьКДанным, ".", Символы.ПС);
	КоличествоЧастей = СтрЧислоСтрок(ЧастиПути);
	Для Позиция = 1 По КоличествоЧастей Цикл
		ИмяРеквизита     = СтрПолучитьСтроку(ЧастиПути, Позиция);
		ВладелецЗначения = ТекущийЭлемент;
		Если Прав(ИмяРеквизита, 1) = "]" Тогда
			ПозицияСкобки = Найти(ИмяРеквизита, "[");
			Индекс = Число(Сред(Лев(ИмяРеквизита, СтрДлина(ИмяРеквизита) - 1), ПозицияСкобки + 1));
			ИмяРеквизита = Лев(ИмяРеквизита, ПозицияСкобки - 1);
			Если Позиция = КоличествоЧастей Тогда
				Элемент = ВладелецЗначения[ИмяРеквизита].НайтиПоИдентификатору(Индекс);
				Элемент = ВыбранноеИмя;
			Иначе
				ТекущийЭлемент = ВладелецЗначения[ИмяРеквизита].НайтиПоИдентификатору(Индекс);
			КонецЕсли;
		Иначе
			Если Позиция = КоличествоЧастей Тогда
				ВладелецЗначения[ИмяРеквизита] = ВыбранноеИмя;
			Иначе
				ТекущийЭлемент = ВладелецЗначения[ИмяРеквизита];
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	ДополнительныеПараметры.Форма.ОбновитьОтображениеДанных();
	
КонецПроцедуры


Процедура ПоказатьПомещениеФайлаПриПодключенииРасширенияРаботыСФайлами(РасширениеПодключено, ДополнительныеПараметры) Экспорт
	
	ОбработчикЗавершения = ДополнительныеПараметры.ОбработчикЗавершения;
	ИдентификаторФормы = ДополнительныеПараметры.ИдентификаторФормы;
	НачальноеИмяФайла = ДополнительныеПараметры.НачальноеИмяФайла;
	ПараметрыДиалога = ДополнительныеПараметры.ПараметрыДиалога;
	
	Если Не РасширениеПодключено Тогда
		Обработчик = Новый ОписаниеОповещения("ОбработатьРезультатПомещенияФайла", ЭтотОбъект, ОбработчикЗавершения);
		НачатьПомещениеФайла(Обработчик, , НачальноеИмяФайла, Истина, ИдентификаторФормы);
		Возврат;
	КонецЕсли;
	
	Если ПараметрыДиалога = Неопределено Тогда
		ПараметрыДиалога = Новый Структура;
	КонецЕсли;
	Если ПараметрыДиалога.Свойство("Режим") Тогда
		Режим = ПараметрыДиалога.Режим;
		Если Режим = РежимДиалогаВыбораФайла.ВыборКаталога Тогда
			ВызватьИсключение НСтр("ru = 'Выбор каталога не поддерживается'");
		КонецЕсли;
	Иначе
		Режим = РежимДиалогаВыбораФайла.Открытие;
	КонецЕсли;
	
	Диалог = Новый ДиалогВыбораФайла(Режим);
	Диалог.ПолноеИмяФайла = НачальноеИмяФайла;
	ЗаполнитьЗначенияСвойств(Диалог, ПараметрыДиалога);
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ОбработатьРезультатПомещенияФайлов", ЭтотОбъект, ОбработчикЗавершения, "ОбработатьОшибкуПомещенияФайлов", ЭтотОбъект);
	
	Если ИдентификаторФормы <> Неопределено Тогда
		НачатьПомещениеФайлов(ОписаниеОповещения, , Диалог, Истина, ИдентификаторФормы);
	Иначе
		НачатьПомещениеФайлов(ОписаниеОповещения, , Диалог, Истина);
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработатьРезультатПомещенияФайлов(ПомещенныеФайлы, ОбработчикЗавершения) Экспорт
	
	ВыборВыполнен = ПомещенныеФайлы <> Неопределено;
	ОбработатьРезультатПомещенияФайла(ВыборВыполнен, ПомещенныеФайлы, Неопределено, ОбработчикЗавершения);
	
КонецПроцедуры

Процедура ОбработатьОшибкуПомещенияФайлов(ИнформацияОбОшибке, СтандартнаяОбработка, ОбработчикЗавершения) Экспорт
	
	ВыборВыполнен = Ложь;
	ОбработатьРезультатПомещенияФайла(ВыборВыполнен, Неопределено, Неопределено, ОбработчикЗавершения);
	
КонецПроцедуры

Процедура ОбработатьРезультатПомещенияФайла(ВыборВыполнен, АдресИлиРезультатВыбора, ВыбранноеИмяФайла, ОбработчикЗавершения) Экспорт
	
	Если ВыборВыполнен = Истина Тогда
		Если ТипЗнч(АдресИлиРезультатВыбора) = Тип("Массив") Тогда
			ПомещенныеФайлы = АдресИлиРезультатВыбора;
		Иначе
			ОписаниеФайла = Новый Структура;
			ОписаниеФайла.Вставить("Хранение", АдресИлиРезультатВыбора);
			ОписаниеФайла.Вставить("Имя",      ВыбранноеИмяФайла);
			ПомещенныеФайлы = Новый Массив;
			ПомещенныеФайлы.Добавить(ОписаниеФайла);
		КонецЕсли;
	Иначе
		ПомещенныеФайлы = Неопределено;
	КонецЕсли;
	
	ВыполнитьОбработкуОповещения(ОбработчикЗавершения, ПомещенныеФайлы);
	
КонецПроцедуры

#КонецОбласти // СлужебныеПроцедурыИФункции
