
////////////////////////////////////////////////////////////////////////////////
// РАБОТА С СЕРВИСАМИ НА ДИСКЕ ИТС:МЕДИЦИНА

////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИТЕРФЕЙС
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// Формирование текста запроса к базе РЛС на ИТС:Медицина

#Если ВебКлиент Тогда

// Функция формирует поисковый запросов в формате версии 1.1
//
// Параметры
//	ПараметрыПоиска - массив структур следующего формата
//				Ключ			|			Значение
//			Имя справочника     |  Структура со свойства "Включить" и "Исключить",
//			или "Текст"			|	содержащими массивы названий элементов справочника
//	ЗаголовокОкна - строка, заголовок поиска
//	СразуПоиск - признак, открыть поиск с заполненными отборами или сразу результат поиска
//	ВыводитьСообщениеОбОтсутствииДокументов - признак вывода сообщения об отсутствии искомых данных
//	АктивизироватьОкноПоиска - признак активизации окно ИТС
//	ПутьДляОтвета - путь к каталогу, куда возвратить ответ
//	ДополнительныеРеквизиты - список запрашиваемых атрибутов
//	ЗапрашиваемыеСправочники
//
// Возвращаемое значение
//	Строка - текст сформированного запроса
//
Функция СформироватьТекстЗапросаДанныхКБазеРЛС(Знач ПараметрыПоиска = Неопределено,
                                               Знач ЗаголовокОкна = "",
                                               Знач СразуПоиск = Истина,
                                               Знач ВыводитьСообщениеОбОтсутствииДокументов = Истина,
                                               Знач АктивизироватьОкноПоиска = Истина,
                                               Знач ПутьДляОтвета = "",
                                               Знач ДополнительныеРеквизиты = Неопределено,
                                               Знач ЗапрашиваемыеСправочники = Неопределено) Экспорт
	
	Возврат ИТСМедицинаВызовСервера.СформироватьТекстЗапросаДанныхКБазеРЛС(
		ПараметрыПоиска,
		ЗаголовокОкна,
		СразуПоиск,
		ВыводитьСообщениеОбОтсутствииДокументов,
		АктивизироватьОкноПоиска,
		ПутьДляОтвета,
		ДополнительныеРеквизиты,
		ЗапрашиваемыеСправочники);
	
КонецФункции

#Иначе

// Функция формирует поисковый запросов в формате версии 1.1
//
// Параметры
//	ПараметрыПоиска - массив структур следующего формата
//				Ключ			|			Значение
//			Имя справочника     |  Структура со свойства "Включить" и "Исключить",
//			или "Текст"			|	содержащими массивы названий элементов справочника
//	ЗаголовокОкна - строка, заголовок поиска
//	СразуПоиск - признак, открыть поиск с заполненными отборами или сразу результат поиска
//	ВыводитьСообщениеОбОтсутствииДокументов - признак вывода сообщения об отсутствии искомых данных
//	АктивизироватьОкноПоиска - признак активизации окно ИТС
//	ПутьДляОтвета - путь к каталогу, куда возвратить ответ
//	ДополнительныеРеквизиты - список запрашиваемых атрибутов
//	ЗапрашиваемыеСправочники
//
// Возвращаемое значение
//	Строка - текст сформированного запроса
//
Функция СформироватьТекстЗапросаДанныхКБазеРЛС(Знач ПараметрыПоиска = Неопределено,
                                               Знач ЗаголовокОкна = "",
                                               Знач СразуПоиск = Истина,
                                               Знач ВыводитьСообщениеОбОтсутствииДокументов = Истина,
                                               Знач АктивизироватьОкноПоиска = Истина,
                                               Знач ПутьДляОтвета = "",
                                               Знач ДополнительныеРеквизиты = Неопределено,
                                               Знач ЗапрашиваемыеСправочники = Неопределено) Экспорт
	
	// Структура запроса на поиск программы Iss в формате xml
	// <?xml version="1.0" encoding="windows-1251"?>
	// <ISSFIND VER="1.1">
	//  <GETDICT>...</GETDICT>
	//  <GETDICT>...</GETDICT>
	//   ...
	//  <QUERY>
	//   ...
	//  </QUERY>
	// </ISSFIND>
	
	
	// ISSFIND
	//  VER - Версия (= 1.1) (Формат VER  1.0 будет поддерживаться)
	
	// подготовим строку заголовка
	ЗаголовокОкна = СтрЗаменить(ЗаголовокОкна, "&", "&amp;");
	ЗаголовокОкна = СтрЗаменить(ЗаголовокОкна, """", "&quot;");
	ЗаголовокОкна = СтрЗаменить(ЗаголовокОкна, "<", "&lt;");
	ЗаголовокОкна = СтрЗаменить(ЗаголовокОкна, ">", "&gt;");
	
	ЗапросXML = Новый ЗаписьXML;
	ЗапросXML.УстановитьСтроку("UTF-8");
	ЗапросXML.ЗаписатьОбъявлениеXML();
	ЗапросXML.ЗаписатьНачалоЭлемента("ISSFIND");
	
		ЗапросXML.ЗаписатьАтрибут("VER", "1.1");
		
		СформироватьПоисковойЗапрос(ЗапросXML,
									ПараметрыПоиска,
									ЗаголовокОкна,
									СразуПоиск,
									ВыводитьСообщениеОбОтсутствииДокументов,
									АктивизироватьОкноПоиска,
									ПутьДляОтвета,
									ДополнительныеРеквизиты);
												
		СформироватьЗапросСправочников(ЗапросXML, ЗапрашиваемыеСправочники);
		
	ЗапросXML.ЗаписатьКонецЭлемента(); // </ISSFIND>
	
	Возврат ЗапросXML.Закрыть();
	
КонецФункции

#КонецЕсли

// Получает описания товаров из корзины или по списку номеров РЛС
//
// Параметры
//  ДанныеАутентификации - Структура
//  КудаСохранить - Строка
//  ЗапрашиваемыеОписания - Массив
//
// Возвращаемое значение
//  Структура - результат загрузки
//
Функция ПолучитьОписанияТоваровССайтаИТС(Знач ДанныеАутентификации, КудаСохранить, ЗапрашиваемыеОписания = Неопределено) Экспорт
	
	ТекстЗапроса = "";
	Если ЗначениеЗаполнено(ЗапрашиваемыеОписания) Тогда
		Для Каждого НомерРЛС Из ЗапрашиваемыеОписания Цикл
			ТекстЗапроса = ТекстЗапроса + "&doc_id[]=" + Формат(НомерРЛС, "ЧН=; ЧГ=0");
		КонецЦикла;
		ТекстЗапроса = "?" + Сред(ТекстЗапроса, 2);
	КонецЕсли;
	
	URLСтрока = "http://its.1c.ru/db/basket/leksr.json" + ТекстЗапроса;
	Возврат СкачатьФайл(URLСтрока, ДанныеАутентификации, КудаСохранить);
	
КонецФункции

// Получает файл с сайта 1С:ИТС
//
Функция ЗагрузитьФайлСВебСервера(Знач ДанныеАутентификации, Знач ИмяФайла, Знач КудаСохранить) Экспорт
	
	URLСтрока = "http://its.1c.ru/download/med/get.php?file=" + ИмяФайла;
	Возврат СкачатьФайл(URLСтрока, ДанныеАутентификации, КудаСохранить);
	
КонецФункции

// Получает классификатор с сайта 1С:ИТС
//
Функция ЗагрузитьКлассификаторСВебИТС(Знач ДанныеАутентификации, Знач ИдентификаторКлассификатора, Знач КудаСохранить) Экспорт
	
	URLСтрока = "http://its.1c.ru/db/metadata/leksr/ftree_all/" + ИдентификаторКлассификатора + ".json";
	Возврат СкачатьФайл(URLСтрока, ДанныеАутентификации, КудаСохранить);
	
КонецФункции

// Получает результаты поиска описаний препаратов
//
Функция НайтиОписанияТоваровНаСайтеИТС(Знач ДанныеАутентификации, Знач ПараметрыПоиска, Знач КудаСохранить) Экспорт
	
	СправочникиИТС = ПолучитьСтруктуруСправочниковНаИТС();
	
	Фильтры = "";
	Для Каждого КлючИЗначение Из СправочникиИТС Цикл
		
		Если ПараметрыПоиска.Свойство(КлючИЗначение.Ключ) Тогда
			
			ПараметрыСправочника = КлючИЗначение.Значение;
			Идентификатор = ПараметрыСправочника.Номер;
			
			Если ПараметрыПоиска[КлючИЗначение.Ключ].Свойство("Включить") Тогда
				Для Каждого ЭлементМассива Из ПараметрыПоиска[КлючИЗначение.Ключ].Включить Цикл
					Фильтры = Фильтры + ?(ПустаяСтрока(Фильтры), "", "&") + "a_" + Идентификатор + "[]=" + ЭлементМассива;
				КонецЦикла;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	СтрокаПоиска = "";
	Если ПараметрыПоиска.Свойство("Текст") Тогда
		
		Если ПараметрыПоиска.Текст.Свойство("Включить") Тогда
			СтрокаПоиска = ПараметрыПоиска.Текст.Включить;
		КонецЕсли;
		
	КонецЕсли;
	
	Если ПустаяСтрока(СтрокаПоиска) Тогда
		СтрокаПоиска = " ";
	КонецЕсли;
	
	Если Не ПустаяСтрока(Фильтры) Тогда
		Фильтры = "?" + Фильтры;
	КонецЕсли;
	
	URLСтрока = СтрШаблон("http://its.1c.ru/db/search/leksr/%1.json%2", СтрокаПоиска, Фильтры);
	Возврат СкачатьФайл(URLСтрока, ДанныеАутентификации, КудаСохранить);
	
КонецФункции

// Возвращает структуру справочников на ИТС
//
Функция ПолучитьСтруктуруСправочниковНаИТС() Экспорт
	
	СправочникиРЛС = Новый Структура;
	СправочникиРЛС.Вставить("ТорговыеНаименования"                  , ОписаниеСправочника("1", НСтр("ru = '""Торговые наименования""'"),, Истина));
	СправочникиРЛС.Вставить("ДействующиеВеществаМНН"                , ОписаниеСправочника("2", НСтр("ru = '""Действующие вещества (МНН)""'")));
	СправочникиРЛС.Вставить("ЛекарственныеФормы"                    , ОписаниеСправочника("3", НСтр("ru = '""Лекарственные формы""'")));
	СправочникиРЛС.Вставить("ФармакологическиеГруппы"               , ОписаниеСправочника("4", НСтр("ru = '""Фармакологические группы""'"), "ФармакологическиеГруппы"));
	СправочникиРЛС.Вставить("Нозология"                             , ОписаниеСправочника("5", НСтр("ru = 'Справочник ""Нозология""'")));
	СправочникиРЛС.Вставить("АТХ"                                   , ОписаниеСправочника("6", НСтр("ru = '""АТХ""'"), "АТХКлассификация"));
	СправочникиРЛС.Вставить("ИндексВышковского"                     , ОписаниеСправочника("7", НСтр("ru = '""Индекс Вышковского""'")));
	СправочникиРЛС.Вставить("РецептурныйОтпуск"                     , ОписаниеСправочника("8", НСтр("ru = '""Признак рецептурного отпуска""'")));
	СправочникиРЛС.Вставить("ЖНВЛП"                                 , ОписаниеСправочника("9", НСтр("ru = '""Признак ЖНВЛП""'")));
	СправочникиРЛС.Вставить("ДЛО"                                   , ОписаниеСправочника("10", НСтр("ru = '""Признак ДЛО""'")));
	СправочникиРЛС.Вставить("НаркотическиеСредства"                 , ОписаниеСправочника("11", НСтр("ru = '""Группы наркотических средств""'")));
	СправочникиРЛС.Вставить("СильнодействующиеИЯды"                 , ОписаниеСправочника("12", НСтр("ru = '""Сильнодействующие и Яды""'")));
	СправочникиРЛС.Вставить("СпискиАиБ"                             , ОписаниеСправочника("13", НСтр("ru = '""Списки А и Б""'")));
	СправочникиРЛС.Вставить("ФармакологическиеДействия"             , ОписаниеСправочника("14", НСтр("ru = '""Фармакологические действия""'")));
	СправочникиРЛС.Вставить("СостоянияРегистрационногоУдостоверения", ОписаниеСправочника("15", НСтр("ru = '""Состояния регистрационного удостоверения""'")));
	СправочникиРЛС.Вставить("Регистраторы"                          , ОписаниеСправочника("16", НСтр("ru = '""Регистраторы""'")));
	СправочникиРЛС.Вставить("СтраныРегистраторов"                   , ОписаниеСправочника("17", НСтр("ru = '""Страны регистраторов""'")));
	СправочникиРЛС.Вставить("Производители"                         , ОписаниеСправочника("18", НСтр("ru = '""Производители""'")));
	СправочникиРЛС.Вставить("СтраныПроизводителей"                  , ОписаниеСправочника("19", НСтр("ru = '""Страны производителей""'")));
	СправочникиРЛС.Вставить("ТоварыФармацевтическогоРынка"          , ОписаниеСправочника("20", НСтр("ru = '""Товары фарм. рынка""'"), "ТоварыФармацевтическогоРынка"));
	СправочникиРЛС.Вставить("ФармакоТерапевтическиеГруппы"          , ОписаниеСправочника("23", НСтр("ru = '""Фармако-терапевтические группы""'"), "ФармакоТерапевтическиеГруппы"));
	
	Возврат СправочникиРЛС;
	
КонецФункции

#КонецОбласти // ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

Функция ОписаниеСправочника(Номер, Наименование, ИмяМетаданных = Неопределено, ПоискСначалаСтроки = Ложь)
	
	Описание = Новый Структура("Номер, Наименование", Номер, Наименование);
	Если ЗначениеЗаполнено(ИмяМетаданных) Тогда
		Описание.Вставить("ИмяМетаданных", ИмяМетаданных);
	КонецЕсли;
	Если ПоискСначалаСтроки Тогда
		Описание.Вставить("ПоискСначалаСтроки");
	КонецЕсли;
	
	Возврат Описание;
	
КонецФункции

#Если Не ВебКлиент Тогда

Процедура СформироватьПоисковойЗапрос(ЗапросXML,
                                      ПараметрыПоиска,
                                      ЗаголовокОкна = "",
                                      СразуПоиск = Истина,
                                      ВыводитьСообщениеОбОтсутствииДокументов = Истина,
                                      АктивизироватьОкноПоиска = Истина,
                                      ПутьДляОтвета = "",
                                      ДополнительныеРеквизиты = Неопределено)
	
	//	<QUERY [SEARCH="YES|NO"] [NOTFOUND="YES|NO"] [HIDDEN="YES|NO"]>
	//		<FINDNAME>название запроса</FINDNAME>
	//		<RESULTSETFILE FORMATTED="NO|YES">Полный путь до файла, в который будет сохранен результат выполнения запроса</RESULTSETFILE>
	//		<DICT ID="DICTID" [NAME="DICTNAME"]>
	//			...
	//		</DICT>
	//
	//		...
	//		<TEXT [CONTENT="YES|NO"] [TITLE="YES|NO"] [NEAR="DIST"]>
	//			...
	//		</TEXT>
	//
	//		<SELECT>
	//			...
	//		</SELECT>
	//
	//	</QUERY>
	
	
	// QUERY
	//	FINDNAME              - Название запроса, которое отображается в результатах поиска
	//	SEARCH	= <YES|NO>    - Выполнить поиск сразу - без показа параметров запроса
	//	NOTFOUND = <YES|NO>   - Выводить сообщение об отсутствии документов. Значение по умолчанию - "YES".
	//	HIDDEN = <YES|NO>     - Отображать окно с результатами поиска в "1С:Базы данных".
	//								Значение по умолчанию - "NO".
	//								Если значение равно "YES", то атрибуты SEARCH и NOTFOUND действия не имеют.
	//	RESULTSETFILE         - Полный путь до файла, в который будет сохранен результат выполнения запроса
	//	FORMATTED = <YES|NO>  - При отсутствии найденных документов - не выводит сообщение и создает «пустой» файл с результатами поиска.
	
	Если Не ТипЗнч(ПараметрыПоиска) = Тип("Структура") Тогда
		Возврат;
	КонецЕсли;
	
	ЗапросXML.ЗаписатьНачалоЭлемента("QUERY");
		ЗапросXML.ЗаписатьАтрибут("SEARCH", ?(СразуПоиск, "YES", "NO"));
		ЗапросXML.ЗаписатьАтрибут("NOTFOUND", ?(ВыводитьСообщениеОбОтсутствииДокументов, "YES", "NO"));
		ЗапросXML.ЗаписатьАтрибут("HIDDEN", ?(АктивизироватьОкноПоиска, "NO", "YES"));
		
		ЗапросXML.ЗаписатьНачалоЭлемента("FINDNAME");
			ЗапросXML.ЗаписатьСекциюCDATA(ЗаголовокОкна);
		ЗапросXML.ЗаписатьКонецЭлемента(); // </FINDNAME>
		
		Если Не ПустаяСтрока(ПутьДляОтвета) Тогда
			ЗапросXML.ЗаписатьНачалоЭлемента("RESULTSETFILE");
				ЗапросXML.ЗаписатьАтрибут("FORMATTED", ?(Ложь, "YES", "NO"));
				
				ЗапросXML.ЗаписатьСекциюCDATA(ПутьДляОтвета);
			ЗапросXML.ЗаписатьКонецЭлемента(); // </RESULTSETFILE>
		КонецЕсли;
		
		ДобавитьФильтрыВЗапрос(ЗапросXML, ПараметрыПоиска);
		
		ДобавитьЗапрашиваемыеАтрибутыВЗапрос(ЗапросXML, ДополнительныеРеквизиты);
		
	ЗапросXML.ЗаписатьКонецЭлемента(); // </QUERY>
	
КонецПроцедуры

// Процедура добавляет список запрашиваемых файлов с диска ИТС
//
// Параметры
//  ЗапросXML - ЗаписьXML - содержащая заголовок запроса
//  ЗапрашиваемыеФайлы - Массив - содержит структуры с полями "НомерСправочника", "ИмяФайла"
//
Процедура СформироватьЗапросСправочников(ЗапросXML, ЗапрашиваемыеФайлы)
	
	// <GETDICT ID="<DICTID>" [NAME="<DICTNAME>"] FORMATTED="NO|YES">  Полный путь до файла, в который будет сохранен справочник</GETDICT>
	
	// DICTID    DICTNAME
	//  1   Торговое название
	//  2   Действующее вещество
	//  3   Лекарственная форма
	//  4   Фармакологическая группа
	//  5   Нозология
	//  6   АТХ
	//  7   Индекс Вышковского
	//          Значения справочника: 1 - "Высокий уровень индекса Вышковского"
	//  8   Рецептурный отпуск
	//          Значения справочника: 1 - "Препараты безрецептурного отпуска"
	//  9   ЖНиВЛС
	//          Значения справочника: 1 - "Лекарственные средства, включенные в Перечень ЖНиВЛС"
	//                                2 - "Лекарственные средства, включенные в Перечень ЖНиВЛС и по которым ведется контроль цен"
	//  10  ДЛО
	//          Значения справочника: 1 - "Лекарственные средства, включенные в список ДЛО"
	//  11  Наркотические средства, психотропные вещества и их прекурсоры
	//          Значения справочника: 1 - "Список I", 2 - "Список II", 3 - "Список III", 4 - "Список IV"
	//  12  Сильнодействующие и ядовитые вещества.
	//          Значения справочника: 1 - "Сильнодействующие", 2 - "Ядовитые"
	//  13  Списки А и Б
	//          Значения справочника: 1 - "Список А", 2 - "Список Б"
	//  15  Состояние регистрационного удостоверения
	//          Значения справочника: 1 - "Действует", 2 - "Регистрация не действует"
	//  16  Регистратор
	//  17  Страна регистратор
	//  18  Производитель
	//  19  Страна производитель
	//  20  Товары фармакологического рынка
	
	Если Не ТипЗнч(ЗапрашиваемыеФайлы) = Тип("Массив") Тогда
		Возврат;
	КонецЕсли;
	
	Для каждого ЗапрашиваемыйФайл Из ЗапрашиваемыеФайлы Цикл
		
		ЗапросXML.ЗаписатьНачалоЭлемента("GETDICT");
		
			ЗапросXML.ЗаписатьАтрибут("ID", ЗапрашиваемыйФайл.НомерСправочника);
			ЗапросXML.ЗаписатьАтрибут("FORMATTED", ?(ЗапрашиваемыйФайл.Свойство("Отформатированный"), "YES", "NO"));
			
			ЗапросXML.ЗаписатьСекциюCDATA(ЗапрашиваемыйФайл.ИмяФайла);
			
		ЗапросXML.ЗаписатьКонецЭлемента(); //</GETDICT>
		
	КонецЦикла;
	
КонецПроцедуры

// Процедура добавляет список фильтров в запрос к ИТС
//
// Параметры
//  ЗапросXML - ЗаписьXML - содержащая заголовок запроса
//  СписокФильтров - структура следующего формата
//				Ключ			|			Значение
//			Имя справочника     |  Структура со свойства "Включить" и "Исключить",
//								|	содержащими массивы названий элементов справочника
//
Процедура ДобавитьФильтрыВЗапрос(ЗапросXML, СписокФильтров)
	
	//		<DICT ID="<DICTID>" [NAME="<DICTNAME>"]>
	//				<INCLUDE>
	//						<ITEM  [ID="<ITEMID>"]> <ITEMNAME>здесь текст</ITEMNAME>
	//								<ITEM  [ID="<ITEMID>"]> <ITEMNAME>здесь текст</ITEMNAME>
	//										...
	//								</ITEM>
	//								...
	//						</ITEM>
	//						...
	//				</INCLUDE>
	//				<EXCLUDE>
	//						<ITEM  [ID="<ITEMID>"]> <ITEMNAME>здесь текст</ITEMNAME>
	//								<ITEM  [ID="<ITEMID>"]> <ITEMNAME>здесь текст</ITEMNAME>
	//										...
	//								</ITEM>
	//								...
	//						</ITEM>
	//						...
	//				</EXCLUDE>
	//		</DICT>
	//		<TEXT [CONTENT="<YES|NO>"] [TITLE="<YES|NO>"] [NEAR="<DIST>"]>
	//				<INCLUDE>здесь текст</INCLUDE>
	//				<EXCLUDE>здесь текст</EXCLUDE>
	//		</TEXT>
	
	//DICTID    DICTNAME
	//	1  	Торговое название
	//	2  	Действующее вещество
	//	3	Лекарственная форма
	//	4  	Фармакологическая группа
	//	5	Нозология
	//	6	АТХ
	//	7	Индекс Вышковского
	//			Значения справочника: 1 - "Высокий уровень индекса Вышковского"
	//	8	Рецептурный отпуск
	//			Значения справочника: 1 - "Препараты безрецептурного отпуска"
	//	9	ЖНиВЛС
	//			Значения справочника: 1 - "Лекарственные средства, включенные в Перечень ЖНиВЛС"
	//								  2 - "Лекарственные средства, включенные в Перечень ЖНиВЛС и по которым ведется контроль цен"
	//	10	ДЛО
	//			Значения справочника: 1 - "Лекарственные средства, включенные в список ДЛО"
	//	11	Наркотические средства, психотропные вещества и их прекурсоры
	//			Значения справочника: 1 - "Список I", 2 - "Список II", 3 - "Список III", 4 - "Список IV"
	//	12	Сильнодействующие и ядовитые вещества.
	//			Значения справочника: 1 - "Сильнодействующие", 2 - "Ядовитые"
	//	13	Списки А и Б
	//			Значения справочника: 1 - "Список А", 2 - "Список Б"
	//	15	Состояние регистрационного удостоверения
	//			Значения справочника: 1 - "Действует", 2 - "Регистрация не действует"
	//	16	Регистратор
	//	17	Страна регистратор
	//	18	Производитель
	//	19	Страна производитель
	//	20	Товары фармакологического рынка
	
	//ITEM
	//	ID = <ITEMID>        - Идентификатор пункта справочника
	//	ITEMNAME             - Название пункта справочника
	//	FIND="StartLine"     - совпадение с начала строки, иначе будет искаться подстрока.
	//	MATCHCASE="No|Yes"   - Поиск по пунктам справочника с учетом регистра.
	//
	//	В теге ITEM должно быть задано или параметр ID или тег ITEMNAME
	//	
	//TEXT 
	//	CONTENT = <YES|NO>   - Искать в тексте
	//	TITLE = <YES|NO>     - Искать в заголовках
	//	NEAR = <DIST>        - "Близость" по словам, целое число [0,12]. Default = 6
	//	INCLUDE              - текст запроса
	//	EXCLUDE              - кроме
	
	Если Не ТипЗнч(СписокФильтров) = Тип("Структура") Тогда
		Возврат;
	КонецЕсли;
	
	СправочникиИТС = ПолучитьСтруктуруСправочниковНаИТС();
	
	Для Каждого КлючИЗначение Из  СправочникиИТС Цикл
		
		Если СписокФильтров.Свойство(КлючИЗначение.Ключ) Тогда
			
			ПараметрыСправочника = КлючИЗначение.Значение;
			ПоискСначалаСтроки = ПараметрыСправочника.Свойство("ПоискСначалаСтроки");
			
			Идентификатор = ПараметрыСправочника.Номер;
			
			ЗапросXML.ЗаписатьНачалоЭлемента("DICT");
			ЗапросXML.ЗаписатьАтрибут("ID", Идентификатор);
			//ЗапросXML.ЗаписатьАтрибут("NAME", ИмяСправочника);
			Если СписокФильтров[КлючИЗначение.Ключ].Свойство("Включить") Тогда
				ЗапросXML.ЗаписатьНачалоЭлемента("INCLUDE");
				Для каждого ЭлементМассива Из СписокФильтров[КлючИЗначение.Ключ].Включить Цикл
					ЗапросXML.ЗаписатьНачалоЭлемента("ITEM");
					ЗапросXML.ЗаписатьНачалоЭлемента("ITEMNAME");
					Если ПоискСначалаСтроки Тогда
						ЗапросXML.ЗаписатьАтрибут("FIND", "StartLine");
					КонецЕсли;
					ЗапросXML.ЗаписатьСекциюCDATA(ЭлементМассива);
					ЗапросXML.ЗаписатьКонецЭлемента(); // </ITEM>
					ЗапросXML.ЗаписатьКонецЭлемента(); // </ITEMNAME>
				КонецЦикла;
				ЗапросXML.ЗаписатьКонецЭлемента(); // </INCLUDE>
			ИначеЕсли СписокФильтров[КлючИЗначение.Ключ].Свойство("Исключить") Тогда
				ЗапросXML.ЗаписатьНачалоЭлемента("EXCLUDE");
				Для каждого ЭлементМассива Из СписокФильтров[КлючИЗначение.Ключ].Исключить Цикл
					ЗапросXML.ЗаписатьНачалоЭлемента("ITEM");
					ЗапросXML.ЗаписатьНачалоЭлемента("ITEMNAME");
					Если ПоискСначалаСтроки Тогда
						ЗапросXML.ЗаписатьАтрибут("FIND", "StartLine");
					КонецЕсли;
					ЗапросXML.ЗаписатьСекциюCDATA(ЭлементМассива);
					ЗапросXML.ЗаписатьКонецЭлемента(); // </ITEM>
					ЗапросXML.ЗаписатьКонецЭлемента(); // </ITEMNAME>
				КонецЦикла;
				ЗапросXML.ЗаписатьКонецЭлемента(); // </EXCLUDE>
			КонецЕсли;
			ЗапросXML.ЗаписатьКонецЭлемента(); // </DICT>
			
		КонецЕсли;
	КонецЦикла;
	
	Если СписокФильтров.Свойство("Текст") Тогда
		
		ЗапросXML.ЗаписатьНачалоЭлемента("TEXT");
		ЗапросXML.ЗаписатьАтрибут("CONTENT", "YES");
		ЗапросXML.ЗаписатьАтрибут("TITLE"	, "YES");
		ЗапросXML.ЗаписатьАтрибут("NEAR"	, "12");
		Если СписокФильтров.Текст.Свойство("Включить") Тогда
			ЗапросXML.ЗаписатьНачалоЭлемента("INCLUDE");
			ЗапросXML.ЗаписатьСекциюCDATA(СписокФильтров.Текст.Включить);
			ЗапросXML.ЗаписатьКонецЭлемента(); // </INCLUDE>
		ИначеЕсли СписокФильтров.Текст.Свойство("Исключить") Тогда
			ЗапросXML.ЗаписатьНачалоЭлемента("EXCLUDE");
			ЗапросXML.ЗаписатьСекциюCDATA(СписокФильтров.Текст.Исключить);
			ЗапросXML.ЗаписатьКонецЭлемента(); // </EXCLUDE>
		КонецЕсли;
		ЗапросXML.ЗаписатьКонецЭлемента(); // </TEXT>
		
	КонецЕсли;
	
КонецПроцедуры

// Процедура добавляет список запрашиваемых атрибутов в запрос
//
// Параметры
//  ЗапросXML - ЗаписьXML - содержащая заголовок запроса
//  СписокАтрибутов - структура
//
Процедура ДобавитьЗапрашиваемыеАтрибутыВЗапрос(ЗапросXML, СписокАтрибутов)
	
	//		<SELECT>
	//			<ATTRIBUTE ID="<ATTRIBID>"></ATTRIBUTE>
	//		</SELECT>
	
	// ATTRIBID Название атрибута
	//  10020    Торговое название
	//  10021    Статус (почти Состояние рег. удостоверения, но подробнее)
	//  10028    Страна регистратор
	//  10034    Фирма регистратор
	//  10035    Фирма производитель
	//  10036    Страна производитель
	//  10038    Фарм. группа
	//  10039    Нозология
	//  10040    АТХ
	//  10041    Без рецепта (Рецептурный отпуск)
	//  10042    Лекарственная форма
	//  10043    ЖНиВЛС
	//  10044    ДЛО
	//  1003     Состояние рег. удостоверения
	//  10050    Свойства
	//  10062    Наркотические средства, психотропные вещества и их прекурсоры
	//  10063    Сильнодействующие
	//  10066    Действующее вещество
	//  10067    Производитель
	//  10068    Товары фармрынка
	//  10069    Рубрикатор
	//  10070    Регистратор
	//  10071    Индекс Вышковского
	
	Если Не ТипЗнч(СписокАтрибутов) = Тип("Структура") Тогда
		Возврат;
	КонецЕсли;
	
	Атрибуты = Новый  Соответствие;
	Атрибуты.Вставить("ТорговоеНаименование"                   , "10020");
	Атрибуты.Вставить("Статус"                                 , "10021");
	Атрибуты.Вставить("СтранаРегистратор"                      , "10028");
	Атрибуты.Вставить("ФирмаРегистратор"                       , "10034");
	Атрибуты.Вставить("ФирмаПроизводитель"                     , "10035");
	Атрибуты.Вставить("СтранаПроизводитель"                    , "10036");
	Атрибуты.Вставить("ФармакологическаяГруппа"                , "10038");
	Атрибуты.Вставить("Нозология"                              , "10039");
	Атрибуты.Вставить("АТХ"                                    , "10040");
	Атрибуты.Вставить("РецептурныйОтпуск"                      , "10041");
	Атрибуты.Вставить("ЛекарственнаяФорма"                     , "10042");
	Атрибуты.Вставить("ЖНиВЛС"                                 , "10043");
	Атрибуты.Вставить("ДЛО"                                    , "10044");
	Атрибуты.Вставить("СостояниеРегистрационногоУдостоверения" , "1003");
	Атрибуты.Вставить("НаркотическиеСредства"                  , "10062");
	Атрибуты.Вставить("СильнодействующиеИЯды"                  , "10063");
	Атрибуты.Вставить("ДействующиеВеществаМНН"                 , "10066");
	Атрибуты.Вставить("ТоварыФармРынка"                        , "10068");
	Атрибуты.Вставить("Рубрикатор"                             , "10069");
	Атрибуты.Вставить("ИндексВышковского"                      , "10071");
	
	// Данные атрибуты возвращаются всегда:
	//  10050 - Свойства
	//  10067 - Производитель
	//  10070 - Регистратор
	
	ЗапросXML.ЗаписатьНачалоЭлемента("SELECT");
	
	Для Каждого КлючИЗначение Из  Атрибуты Цикл
		
		Если СписокАтрибутов.Свойство(КлючИЗначение.Ключ) Тогда
			
			ЗапросXML.ЗаписатьНачалоЭлемента("ATTRIBUTE");
				ЗапросXML.ЗаписатьАтрибут("ID", КлючИЗначение.Значение);
			ЗапросXML.ЗаписатьКонецЭлемента();
			
		КонецЕсли;
		
	КонецЦикла;
	
	ЗапросXML.ЗаписатьКонецЭлемента(); // </SELECT>
	
КонецПроцедуры

#КонецЕсли

Функция СкачатьФайл(Знач URLСтрока, Знач ДанныеАутентификации, Знач КудаСохранить, Знач Таймаут = Неопределено)
	
	ПараметрыЗагрузкиФайла = Новый Структура;
	ПараметрыЗагрузкиФайла.Вставить("Пользователь", ДанныеАутентификации.Логин);
	ПараметрыЗагрузкиФайла.Вставить("Пароль",  ДанныеАутентификации.Пароль);
	ПараметрыЗагрузкиФайла.Вставить("ПутьДляСохранения", КудаСохранить);
	Если Таймаут = Неопределено Тогда
		Таймаут = 43200; // ждем 12 часов
	КонецЕсли;
	ПараметрыЗагрузкиФайла.Вставить("Таймаут", Таймаут);
	
	#Если Клиент Тогда
		Результат = ПолучениеФайловИзИнтернетаКлиент.СкачатьФайлНаКлиенте(URLСтрока, ПараметрыЗагрузкиФайла);
	#Иначе
		Результат = ПолучениеФайловИзИнтернета.СкачатьФайлНаСервере(URLСтрока, ПараметрыЗагрузкиФайла);
	#КонецЕсли
	
	Если Результат.Статус Тогда
		Если Результат.Свойство("Заголовки") Тогда
			Статус = Результат.Заголовки.Получить("Status");
			Если Статус = "401 Unauthorized" Тогда
				Результат.Вставить("КодСостояния", 401);
				Результат.Статус = Ложь;
				Результат.Вставить("СообщениеОбОшибке", НСтр("ru = 'Доступ к данному материалу ограничен'"));
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции

#КонецОбласти // СлужебныеПроцедурыИФункции
