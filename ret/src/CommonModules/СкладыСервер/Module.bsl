
////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

// Функция возвращает признак ведения складского учета в отделении
//
// Параметры:
//  Отделение - СправочникСсылка.ОтделенияОрганизаций -
//  Дата      - Дата - дата на которую проверяется ведение складского учета
//
// Возвращаемое значение:
//  Булево - Истина, если ведется складской учет
//
Функция ВестиСкладскойУчетВОтделении(Знач Отделение, Знач Дата = Неопределено) Экспорт
	
	Если Не ЗначениеЗаполнено(Отделение) Тогда
		Возврат Ложь;
	КонецЕсли;
	
	Если Не ПолучитьФункциональнуюОпцию("ВестиСкладскойУчетВОтделениях") Тогда
		Возврат Ложь;
	КонецЕсли;
	
	ДатаПроверки = ?(ЗначениеЗаполнено(Дата), Дата, ТекущаяДатаСеанса());
	
	ПараметрыВеденияСкладскогоУчета = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Отделение, "ВестиСкладскойУчет, ДатаНачалаВеденияСкладскогоУчета");
	Возврат ПараметрыВеденияСкладскогоУчета.ВестиСкладскойУчет
	      И ДатаПроверки >= ПараметрыВеденияСкладскогоУчета.ДатаНачалаВеденияСкладскогоУчета;
	
КонецФункции

// Проверяет ведение складского учета в отделениях для объекта.
//
// Параметры:
//  Объект - ДокументОбъект
//  Отказ - Булево
//
Процедура ПроверитьВедениеСкладскогоУчетаВОтделении(Объект, Отказ, РеквизитОтделение = "Отделение") Экспорт
	
	Отделение = Объект[РеквизитОтделение];
	Если ЗначениеЗаполнено(Отделение) И Не ВестиСкладскойУчетВОтделении(Отделение, Объект.Дата) Тогда
		
		ТекстСообщения = НСтр("ru='В отделении %1 на %2 не велся складской учет.'");
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			ТекстСообщения, Отделение, Формат(Объект.Дата, "ДЛФ=D"));
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			ТекстСообщения,
			Объект,
			РеквизитОтделение,
			,
			Отказ);
		
	КонецЕсли;
	
КонецПроцедуры

// Возвращает параметры учета номенклатуры на складе
//
Функция ПараметрыУчетаНоменклатуры(Склад) Экспорт
	
	ПараметрыФО = Новый Структура("Склад", Склад);
	
	ПараметрыУчета = Новый Структура;
	ПараметрыУчета.Вставить("ИспользоватьСерииНоменклатуры", ПолучитьФункциональнуюОпцию("ИспользоватьСерииНоменклатурыСклад", ПараметрыФО));
	ПараметрыУчета.Вставить("ИспользоватьПартии", ПолучитьФункциональнуюОпцию("ИспользоватьПартииСклад", ПараметрыФО));
	
	Возврат ПараметрыУчета;
	
КонецФункции

// Определяет, является ли склад розничным магазином
//
// Параметры:
//   Склад - СправочникСсылка.Склады
//
// Возвращаемое значение:
//   Булево - Истина, если тип склад - розничный магазин
//
Функция ЭтоРозничныйСклад(Склад) Экспорт
	
	Возврат (ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Склад, "ТипСклада") = Перечисления.ТипыСкладов.РозничныйМагазин);
	
КонецФункции

// Определяет, является ли склад отделением
//
// Параметры:
//   Склад - СправочникСсылка.Склады
//
// Возвращаемое значение:
//   Булево - Истина, если тип склад - отделение
//
Функция ЭтоСкладОтделения(Склад) Экспорт
	
	Возврат (ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Склад, "ТипСклада") = Перечисления.ТипыСкладов.Отделение);
	
КонецФункции

// Возвращает имя реквизита склада, с которого выбывает товар.
//
// Параметры:
//  Объект - ДокументОбъект
//
// Возвращаемое значение:
//  Строка - имя реквизита склада отправителя, если он есть в объекте, иначе Неопределено.
//
Функция ИмяРеквизитаСкладОтправитель(Объект) Экспорт
	
	МетаданныеОбъекта = Объект.Метаданные();
	Если ОбщегоНазначения.ЕстьРеквизитОбъекта("СкладОтправитель", МетаданныеОбъекта) Тогда
		Возврат "СкладОтправитель";
	КонецЕсли;
	
	Если ОбщегоНазначения.ЕстьРеквизитОбъекта("Склад", МетаданныеОбъекта) Тогда
		Возврат "Склад";
	КонецЕсли;
	
	Возврат Неопределено;
	
КонецФункции

#КонецОбласти // ПрограммныйИнтерфейс
