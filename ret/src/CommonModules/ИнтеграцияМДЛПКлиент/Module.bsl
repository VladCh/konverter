
#Область ПрограммныйИнтерфейс

// Запускает процедуру обмена сообщениями.
//
// Параметры:
//  Организация              - ОпределяемыйТип.ОрганизацииМДЛП - организация, для которой запускается обмена.
//  МестоДеятельности        - ОпределяемыйТип.МестаДеятельностиМДЛП - место деятельности, для которого запускается обмен.
//  ОповещениеПриЗавершении  - ОписаниеОповещения - куда вернуть результат обмена.
//
Процедура ВыполнитьОбмен(Организация = Неопределено, МестоДеятельности = Неопределено, ОповещениеПриЗавершении = Неопределено) Экспорт
	
	ОчиститьСообщения();
	
	ДанныеДляВыполненияОбменаНаКлиенте = ИнтеграцияМДЛПВызовСервера.ВыполнитьОбмен(Организация, МестоДеятельности);
	
	ИнтеграцияМДЛПСлужебныйКлиент.ОбработатьОчередьПередачиДанных(
		ДанныеДляВыполненияОбменаНаКлиенте,
		ОповещениеПриЗавершении);
	
КонецПроцедуры

// Запускает выполнение обмена по расписанию.
//
Процедура ВыполнитьОбменНаКлиентеПоРасписанию() Экспорт
	
	ДанныеДляВыполненияОбменаНаКлиенте = ИнтеграцияМДЛПВызовСервера.ВыполнитьОбмен(,, ДатыПоследнегоЗапускаОбменаПоРасписанию());
	
	Если Не ДанныеДляВыполненияОбменаНаКлиенте.ВыполнитьОбменПоРасписанию Тогда
		Возврат;
	КонецЕсли;
	
	ИнтеграцияМДЛПСлужебныйКлиент.ОбработатьОчередьПередачиДанных(ДанныеДляВыполненияОбменаНаКлиенте);
	
КонецПроцедуры

// Обработчик команд по выполнению требуемого дальнейшего действия в динамических списках.
//
// Параметры:
//  Список - ЭлементФормы - список в котором выполняется команда.
//  ДальнейшееДействие - ПеречислениеСсылка.ДальнейшиеДействияПоВзаимодействиюМДЛП - действие, которое будет выполнено.
//
Процедура ПодготовитьСообщенияКПередаче(Список, ДальнейшееДействие) Экспорт
	
	ОчиститьСообщения();
	
	СписокДокументов = Новый Массив;
	НепроведенныеДокументы = Новый Массив;
	
	ВыделенныеСтроки = Список.ВыделенныеСтроки;
	Для Каждого ВыделеннаяСтрока Из ВыделенныеСтроки Цикл
		Если ТипЗнч(ВыделеннаяСтрока) = Тип("СтрокаГруппировкиДинамическогоСписка") Тогда
			Продолжить;
		КонецЕсли;
		
		ДанныеСтроки = Список.ДанныеСтроки(ВыделеннаяСтрока);
		Для Индекс = 1 По 3 Цикл
			Если ДанныеСтроки["ДальнейшееДействие" + Индекс] = ДальнейшееДействие Тогда
				СписокДокументов.Добавить(ДанныеСтроки.Ссылка);
				Если Не ДанныеСтроки.Проведен Тогда
					НепроведенныеДокументы.Добавить(ДанныеСтроки.Ссылка);
				КонецЕсли;
				Прервать;
			КонецЕсли;
		КонецЦикла;
		
	КонецЦикла;
	
	Если СписокДокументов.Количество() = 0 Тогда
		ПоказатьПредупреждение(, НСтр("ru = 'Команда не может быть выполнена для указанного объекта.'"));
		Возврат;
	КонецЕсли;
	
	Если НепроведенныеДокументы.Количество() > 0 Тогда
		
		Если НепроведенныеДокументы.Количество() = 1 Тогда
			ТекстВопроса = НСтр("ru = 'Для выполнения команды необходимо предварительно провести документ. Выполнить проведение документа и продолжить?'");
		Иначе
			ТекстВопроса = НСтр("ru = 'Для выполнения команды необходимо предварительно провести документы. Выполнить проведение документов и продолжить?'");
		КонецЕсли;
		
		Контекст = Новый Структура;
		Контекст.Вставить("СписокДокументов"      , СписокДокументов);
		Контекст.Вставить("НепроведенныеДокументы", НепроведенныеДокументы);
		Контекст.Вставить("ДальнейшееДействие"    , ДальнейшееДействие);
		Контекст.Вставить("Список"                , Список);
		
		Обработчик = Новый ОписаниеОповещения("ПодготовитьСообщенияКПередачеПодтверждениеПроведения", ИнтеграцияМДЛПСлужебныйКлиент, Контекст);
		
		Кнопки = Новый СписокЗначений;
		Кнопки.Добавить(КодВозвратаДиалога.Да, НСтр("ru = 'Продолжить'"));
		Кнопки.Добавить(КодВозвратаДиалога.Отмена);
		ПоказатьВопрос(Обработчик, ТекстВопроса, Кнопки);
		
	Иначе
		
		ИнтеграцияМДЛПСлужебныйКлиент.ПодготовитьСообщенияКПередаче(СписокДокументов, ДальнейшееДействие, Список);
		
	КонецЕсли;
	
КонецПроцедуры

// Формирует сообщение для передачи, согласно указанной операции.
//
// Параметры:
//  ДокументСсылка - ДокументСсылка - документ, по которому формируется сообщение.
//  ДальнейшееДействие - ПеречислениеСсылка.ДальнейшиеДействияПоВзаимодействиюМДЛП - выполняемая операция обмена.
//  ДополнительныеПараметры - Произвольный - дополнительные параметры формирования сообщения.
//
Процедура ПодготовитьСообщениеКПередаче(ДокументСсылка, ДальнейшееДействие, ДополнительныеПараметры = Неопределено) Экспорт
	
	ОчиститьСообщения();
	
	ИнтеграцияМДЛПСлужебныйКлиент.ПодготовитьСообщенияКПередаче(
		ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(ДокументСсылка),
		ДальнейшееДействие,
		ДокументСсылка,
		ДополнительныеПараметры);
	
КонецПроцедуры

// Обрабатывает навигационную ссылку статуса информирования уведомления.
//
// Параметры:
//  Форма                - Форма  - форма уведомления.
//  НавигационнаяСсылка  - Строка - навигационная ссылка.
//  СтандартнаяОбработка - Булево - признак стандартной обработки навигационной ссылки.
//
Процедура ОбработатьНавигационнуюСсылкуСтатуса(Форма, НавигационнаяСсылка, СтандартнаяОбработка, ТребуетсяПроведение = Истина) Экспорт
	
	СтандартнаяОбработка = Ложь;
	ОчиститьСообщения();
	
	Контекст = Новый Структура;
	Контекст.Вставить("Форма", Форма);
	Контекст.Вставить("НавигационнаяСсылка", НавигационнаяСсылка);
	Обработчик = Новый ОписаниеОповещения("ОбработатьНавигационнуюСсылкуСтатусаЗавершение", ИнтеграцияМДЛПСлужебныйКлиент, Контекст);
	ИнтеграцияМДЛПСлужебныйКлиент.ЗаписатьДокументВФормеПриНеобходимости(Обработчик, Форма, ТребуетсяПроведение);
	
КонецПроцедуры

// Открывает форму протокола обмена
//
// Параметры:
//  Документ        - ДокументСсылка - документ, для которого открывается протокол обмена.
//  ФормаВладелец   - УправляемаяФорма, Неопределено - форма, которая будет являться владельцем формы протокола обмена.
//
Процедура ОткрытьПротоколОбмена(Документ, ФормаВладелец = Неопределено) Экспорт
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("Документ", Документ);
	
	ОткрытьФорму(
		"Справочник.МДЛППрисоединенныеФайлы.Форма.ПротоколОбмена",
		ПараметрыФормы,
		ФормаВладелец,
		Истина,
		,
		,
		,
		РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

// Открывает форму выбора субъектов обращения собственных организаций.
//
// Параметры:
//  Форма                - Форма - форма объекта, из которой вызывается открытие формы выбора.
//  Элемент              - см. обработчик события НачалоВыбора.
//  ДанныеВыбора         - см. обработчик события НачалоВыбора.
//  СтандартнаяОбработка - см. обработчик события НачалоВыбора.
//
Процедура ВыбратьИдентификаторСобственнойОрганизации(Форма, Элемент, ДанныеВыбора, СтандартнаяОбработка) Экспорт
	
	СтандартнаяОбработка = Ложь;
	Отбор = Новый Структура;
	Отбор.Вставить("СобственнаяОрганизация", Истина);
	
	ПараметрыВыбора = Новый Структура;
	ПараметрыВыбора.Вставить("Отбор", Отбор);
	
	ОткрытьФорму("РегистрСведений.СубъектыОбращенияМДЛП.Форма.ФормаВыбора", ПараметрыВыбора, Элемент);
	
КонецПроцедуры

// Открывает форму выбора субъектов обращения сторонних организаций.
//
// Параметры:
//  Форма                - Форма - форма объекта, из которой вызывается открытие формы выбора.
//  Элемент              - см. обработчик события НачалоВыбора.
//  ДанныеВыбора         - см. обработчик события НачалоВыбора.
//  СтандартнаяОбработка - см. обработчик события НачалоВыбора.
//
Процедура ВыбратьИдентификаторСтороннейОрганизации(Форма, Элемент, ДанныеВыбора, СтандартнаяОбработка, ДополнительныйОтбор = Неопределено) Экспорт
	
	СтандартнаяОбработка = Ложь;
	Отбор = Новый Структура;
	Отбор.Вставить("СобственнаяОрганизация", Ложь);
	
	Если ДополнительныйОтбор <> Неопределено Тогда
		ОбщегоНазначенияКлиентСервер.ДополнитьСтруктуру(Отбор, ДополнительныйОтбор, Ложь);
	КонецЕсли;
	
	ПараметрыВыбора = Новый Структура;
	ПараметрыВыбора.Вставить("Отбор", Отбор);
	
	ОткрытьФорму("РегистрСведений.СубъектыОбращенияМДЛП.Форма.ФормаВыбора", ПараметрыВыбора, Элемент);
	
КонецПроцедуры

// Устанавливает отбор строк в табличной части формы.
//
// Параметры:
//  Форма          - УправляемаяФорма - форма, для одной из ТЧ, которой устанавливается отбор.
//  ИмяТЧ          - Строка - имя табличной части, для которой устанавливается отбор строк.
//  ЗначениеОтбора - Строка - значение отбор.
//  Команда        - КомандаФормы - команда, которую выполнил пользователь для установки отбора.
//
Процедура УстановитьОтборПоСостояниюПодтверждения(Форма, ИмяТабличнойЧасти, СостояниеПодтверждения, Команда) Экспорт
	
	ЗначениеОтбора = ПредопределенноеЗначение("Перечисление.СостоянияПодтвержденияМДЛП." + СостояниеПодтверждения);
	Если ЗначениеОтбора = Форма.ОтборСостояниеПодтверждения Тогда
		Отбор = Неопределено;
	Иначе
		Отбор = Новый Структура("СостояниеПодтверждения", ЗначениеОтбора);
	КонецЕсли;
	
	ИменаТабличныхЧастей = СтрРазделить(ИмяТабличнойЧасти, ", ", Ложь);
	Если Отбор = Неопределено Тогда
		Для Каждого Имя Из ИменаТабличныхЧастей Цикл
			СнятьОтборСтрок(Форма.Элементы[Имя].ОтборСтрок, "СостояниеПодтверждения");
		КонецЦикла;
	Иначе
		Для Каждого Имя Из ИменаТабличныхЧастей Цикл
			УстановитьОтборСтрок(Форма.Элементы[Имя].ОтборСтрок, Отбор);
		КонецЦикла;
	КонецЕсли;
	
	Форма.ОтборСостояниеПодтверждения = ?(Отбор = Неопределено, Неопределено, ЗначениеОтбора);
	
	Для Каждого ЭлементКоманда Из Форма.Элементы.ОтборПоСостояниюПодтверждения.ПодчиненныеЭлементы Цикл
		
		Если Отбор = Неопределено Тогда
			ЭлементКоманда.Пометка = Ложь;
		Иначе
			ЭлементКоманда.Пометка = (ЭлементКоманда.ИмяКоманды = Команда.Имя);
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

// Устанавливает отбор строк в таблице формы.
//
// Параметры:
//  ОтборСтрок  - Структура, Неопределено - свойство ОтборСтрок таблицы формы.
//  Отбор       - Структура - устанавливаемый отбор
//
Процедура УстановитьОтборСтрок(ОтборСтрок, Отбор) Экспорт
	
	Если ОтборСтрок = Неопределено Тогда
		ЗначениеОтбора = Новый Структура;
	Иначе
		ЗначениеОтбора = Новый Структура(ОтборСтрок);
	КонецЕсли;
	
	Для Каждого КлючЗначение Из Отбор Цикл
		ЗначениеОтбора.Вставить(КлючЗначение.Ключ, КлючЗначение.Значение);
	КонецЦикла;
	
	ОтборСтрок = Новый ФиксированнаяСтруктура(ЗначениеОтбора);
	
КонецПроцедуры

// Снимает указанный отбор в таблице формы.
//
// Параметры:
//  ОтборСтрок  - Структура, Неопределено - свойство ОтборСтрок таблицы формы.
//  КлючОтбора  - Строка - имя снимаемого отбора.
//
Процедура СнятьОтборСтрок(ОтборСтрок, КлючОтбора) Экспорт
	
	Если ОтборСтрок = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если ОтборСтрок.Свойство(КлючОтбора) Тогда
		ЗначениеОтбора = Новый Структура(ОтборСтрок);
		ЗначениеОтбора.Удалить(КлючОтбора);
		Если ЗначениеОтбора.Количество() = 0 Тогда
			ОтборСтрок = Неопределено;
		Иначе
			ОтборСтрок = Новый ФиксированнаяСтруктура(ЗначениеОтбора);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

// Открывает форму редактирования адреса.
//
// Параметры:
//  Форма                -Форма- форма объекта, из которой вызывается открытие формы редактирования.
//  Объект               - объект с адресной информацией.
//  ИмяРеквизитаАдреса   - Строка - имя реквизита, в котором хранится адрес.
//  СтандартнаяОбработка - Булево - признак стандартной обработки события НачалоВыбора.
//
Процедура РедактироватьАдресВДиалоге(Форма, Объект, ИмяРеквизитаАдреса, СтандартнаяОбработка = Ложь) Экспорт
	
	Если Не ОбщегоНазначенияКлиент.ПодсистемаСуществует("СтандартныеПодсистемы.КонтактнаяИнформация") Тогда
		Возврат;
	КонецЕсли;
	
	СтандартнаяОбработка = Ложь;
	
	МодульУправлениеКонтактнойИнформациейКлиент = ОбщегоНазначенияКлиент.ОбщийМодуль("УправлениеКонтактнойИнформациейКлиент");
	
	ВидКонтактнойИнформации = Новый Структура;
	ВидКонтактнойИнформации.Вставить("Тип", ПредопределенноеЗначение("Перечисление.ТипыКонтактнойИнформации." + "Адрес"));
	ВидКонтактнойИнформации.Вставить("ТолькоНациональныйАдрес", Истина);
	ВидКонтактнойИнформации.Вставить("ПроверятьПоФИАС"        , Истина);
	ВидКонтактнойИнформации.Вставить("ПроверятьКорректность"  , Истина);
	
	ПараметрыОткрытия = МодульУправлениеКонтактнойИнформациейКлиент.ПараметрыФормыКонтактнойИнформации(
		ВидКонтактнойИнформации,
		Объект[ИмяРеквизитаАдреса + "ЗначенияПолей"],
		Объект[ИмяРеквизитаАдреса]);
	
	ДополнительныеПараметры = Новый Структура;
	ДополнительныеПараметры.Вставить("Форма", Форма);
	ДополнительныеПараметры.Вставить("Объект", Объект);
	ДополнительныеПараметры.Вставить("ИмяРеквизитаАдреса", ИмяРеквизитаАдреса);
	
	Оповестить = Новый ОписаниеОповещения("ОбработатьРезультатРедактированияАдреса", ИнтеграцияМДЛПСлужебныйКлиент, ДополнительныеПараметры);
	
	МодульУправлениеКонтактнойИнформациейКлиент.ОткрытьФормуКонтактнойИнформации(ПараметрыОткрытия, Форма, Оповестить);
	
КонецПроцедуры

// Подключает обработчик ожидания для выполнения обмена с МДЛП на клиенте по расписанию.
//
Процедура ПодключитьОбработчикВыполненияОбменаНаКлиентеПоРасписанию() Экспорт
	
	ОтключитьОбработчикОжидания("ОбработчикОжиданияВыполненияОбменаНаКлиентеПоРасписаниюМДЛП");
	
	Если Не ИнтеграцияМДЛПВызовСервера.ДоступноВыполнениеОбменаНаКлиентеПоРасписанию() Тогда
		Возврат;
	КонецЕсли;
	
	Если Не МенеджерОборудованияКлиент.ОбновитьРабочееМестоКлиента() Тогда
		Возврат;
	КонецЕсли;
	
	ПодключитьОбработчикОжидания("ОбработчикОжиданияВыполненияОбменаНаКлиентеПоРасписаниюМДЛП", 60, Ложь);
	
КонецПроцедуры

// Обрабатывает нажатие на гиперссылку со статусом обработки документа в МДЛП.
//
// Параметры:
//  Форма - УправляемаяФорма - форма документа, в которой произошло нажатие на гиперссылку,
//  НавигационнаяСсылкаФорматированнойСтроки - Строка - значение гиперссылки форматированной строки,
//  СтандартнаяОбработка - Булево - признак стандартной (системной) обработки события.
//
Процедура ТекстУведомленияМДЛПОбработкаНавигационнойСсылки(Форма, НавигационнаяСсылкаФорматированнойСтроки, СтандартнаяОбработка) Экспорт
	
	ДокументОснование = Форма.Объект.Ссылка;
	
	СтандартнаяОбработка = Ложь;
	
	Контекст = Новый Структура;
	Контекст.Вставить("ДокументОснование", ДокументОснование);
	Контекст.Вставить("НавигационнаяСсылкаФорматированнойСтроки", НавигационнаяСсылкаФорматированнойСтроки);
	Контекст.Вставить("Форма", Форма);
	Обработчик = Новый ОписаниеОповещения("ТекстУведомленияМДЛПОбработкаНавигационнойСсылкиЗавершение", ЭтотОбъект, Контекст);
	
	ИнтеграцияМДЛПСлужебныйКлиент.ЗаписатьДокументВФормеПриНеобходимости(Обработчик, Форма, Ложь);
	
КонецПроцедуры

// Обрабатывает нажатие на гиперссылку со статусом обработки документа в МДЛП.
//
// Параметры:
//  РезультатВопроса - КодВозвратаДиалога - выбранный пользователем ответ,
//  Контекст - Структура - контекст формы документа:
//   * Форма - УправляемаяФорма - форма документа, в которой произошло нажатие на гиперссылку,
//   * ДокументОснование - ДокументСсылка - ссылка на документ, в котором произошло нажатие на гиперссылку,
//   * НавигационнаяСсылкаФорматированнойСтроки - Строка - значение гиперссылки форматированной строки.
//
Процедура ТекстУведомленияМДЛПОбработкаНавигационнойСсылкиЗавершение(ОбъектЗаписан, Контекст) Экспорт
	
	Если Не ОбъектЗаписан Тогда
		Возврат;
	КонецЕсли;
	
	ВыполнитьКомандуГиперссылки(
		Контекст.Форма.Объект.Ссылка,
		Контекст.НавигационнаяСсылкаФорматированнойСтроки,
		Контекст.Форма);
	
КонецПроцедуры

// Выполняет обработку команды по открытию или созданию документов МДЛП по документу-основанию.
//
// Параметры:
//  ДокументОснование - ДокументСсылка - Документ, на основании которого необходимо создать документ МДЛП или открыть существующий.
//  Команда - Строка - Выполняемая команда. Например: "СоздатьУведомлениеОВыбытииМДЛП".
//  ФормаВладелец - УправляемаяФорма, Неопределено - Форма-владелец.
//
Процедура ВыполнитьКомандуГиперссылки(ДокументОснование, Команда, Форма) Экспорт
	
	Если Команда = "ОткрытьПротоколОбмена" Тогда
		
		ОткрытьПротоколОбмена(ДокументОснование, Форма);
		
	ИначеЕсли СтрНачинаетсяС(Команда, "Открыть") Тогда
		
		Результат = ИнтеграцияМДЛПВызовСервера.ДокументыПоОснованию(ДокументОснование);
		
		МассивДокументов = Результат[Сред(Команда, СтрДлина("Открыть") + 1)];
		Если МассивДокументов.Количество() = 1 Тогда
			ПоказатьЗначение(, МассивДокументов[0].Ссылка);
		КонецЕсли;
		
	ИначеЕсли СтрНачинаетсяС(Команда, "Создать") Тогда
		
		ПараметрыФормы = Новый Структура;
		ПараметрыФормы.Вставить("Основание", ДокументОснование);
		ИмяУведомления = Сред(Команда, СтрДлина("Создать") + 1);
		ОткрытьФорму("Документ." + ИмяУведомления + ".ФормаОбъекта", ПараметрыФормы, Форма);
		
	ИначеЕсли СтрНачинаетсяС(Команда, "Связать") Тогда
		
		ИмяУведомления = Сред(Команда, СтрДлина("Связать") + 1);
		ПараметрыФормы = Новый Структура;
		ПараметрыФормы.Вставить("Отбор", Новый Структура("Ссылка", ИнтеграцияМДЛПВызовСервера.ВозможныеУведомленияПоОснованию(ИмяУведомления, ДокументОснование)));
		Контекст = Новый Структура;
		Контекст.Вставить("Форма", Форма);
		Контекст.Вставить("Основание", ДокументОснование);
		Обработчик = Новый ОписаниеОповещения("СвязатьУведомлениеСОснованиемПослеВыбора", ИнтеграцияМДЛПСлужебныйКлиент, Контекст);
		ОткрытьФорму("Документ." + ИмяУведомления + ".ФормаВыбора", ПараметрыФормы, Форма,,,, Обработчик, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
		
	Иначе
		
		ПерейтиПоНавигационнойСсылке(Команда);
		
	КонецЕсли;
	
КонецПроцедуры

// Подтверждает передачу сообщения, находящегося в очереди передачи данных, и удаляет его из очереди.
//
// Параметры:
//  Сообщение               - СправочникСсылка.МДЛППрисоединенныеФайлы - сообщение, отправку которого нужно подтвердить.
//  ОповещениеПриЗавершении - ОписаниеОповещения - обработчик результата выполнения операции.
//
Процедура ПодтвердитьПередачу(Сообщение, ОповещениеПриЗавершении = Неопределено) Экспорт
	
	ИнтеграцияМДЛПСлужебныйКлиент.ПодтвердитьПередачу(Сообщение, ОповещениеПриЗавершении);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Установить дату последнего запуска обмена на клиенте по расписанию.
//
// Параметры:
//  ИдентификаторСубъектаОбращения - Строка - идентификатор.
//
Процедура ОбновитьДатуПоследнегоЗапускаОбменаНаКлиентеПоРасписанию(ИдентификаторСубъектаОбращения) Экспорт
	
	Если ТипЗнч(ИдентификаторСубъектаОбращения) = Тип("Массив") Тогда
		Идентификаторы = ИдентификаторСубъектаОбращения;
	Иначе
		Идентификаторы = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(ИдентификаторСубъектаОбращения);
	КонецЕсли;
	
	ДатыПоследнегоЗапуска = ДатыПоследнегоЗапускаОбменаПоРасписанию();
	ТекущаяДата = ОбщегоНазначенияКлиент.ДатаСеанса();
	
	Для Каждого Идентификатор Из Идентификаторы Цикл
		ДатыПоследнегоЗапуска.Вставить(Идентификатор, ТекущаяДата);
	КонецЦикла;
	
	ИмяПараметра = "ИнтеграцияМДЛП.ДатыПоследнегоЗапускаОбменаПоРасписанию";
	ПараметрыПриложения.Вставить(ИмяПараметра, ДатыПоследнегоЗапуска);
	
КонецПроцедуры

Функция ДатыПоследнегоЗапускаОбменаПоРасписанию()
	
	ИмяПараметра = "ИнтеграцияМДЛП.ДатыПоследнегоЗапускаОбменаПоРасписанию";
	Если ПараметрыПриложения[ИмяПараметра] = Неопределено Тогда
		ПараметрыПриложения.Вставить(ИмяПараметра, Новый Соответствие);
	КонецЕсли;
	
	Возврат ПараметрыПриложения[ИмяПараметра];
	
КонецФункции

#Область Прочее

///////////////////////////////////////////////////
// МЕТОДЫ РАБОТЫ СО СКАНЕРОМ ШТРИХКОДОВ

Функция ПреобразоватьДанныеСоСканераВМассив(Параметр) Экспорт
	
	Данные = Новый Массив;
	Данные.Добавить(ПреобразоватьДанныеСоСканераВСтруктуру(Параметр));
	
	Возврат Данные;
	
КонецФункции

Функция ПреобразоватьДанныеСоСканераВСтруктуру(Параметр) Экспорт
	
	Если Параметр[1] = Неопределено Тогда
		Данные = Новый Структура("Штрихкод, Количество", Параметр[0], 1);    // Достаем штрихкод из основных данных
	Иначе
		Данные = Новый Структура("Штрихкод, Количество", Параметр[1][1], 1); // Достаем штрихкод из дополнительных данных
	КонецЕсли;
	
	Возврат Данные;
	
КонецФункции

#КонецОбласти

#КонецОбласти
