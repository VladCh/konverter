
#Область ПрограммныйИнтерфейс

#Область МенюОтчеты

// Добавляет команду отчета в список команд.
// 
// Параметры:
//   КомандыОтчетов - ТаблицаЗначений - состав полей см. в функции МенюОтчеты.СоздатьКоллекциюКомандОтчетов.
//
Процедура ДобавитьКомандуСтруктураПодчиненности(КомандыОтчетов) Экспорт
	
	
	Возврат;
	
КонецПроцедуры

// Добавляет команду отчета в список команд.
// 
// Параметры:
//   КомандыОтчетов - ТаблицаЗначений - состав полей см. в функции МенюОтчеты.СоздатьКоллекциюКомандОтчетов.
//
Процедура ДобавитьКомандуДвиженияДокумента(КомандыОтчетов) Экспорт
	
	Возврат;
	
КонецПроцедуры

#КонецОбласти

// Устанавливает параметры выбора номенклатуры.
//
// Параметры:
//  Форма - УправляемаяФорма - форма, в которой нужно установить параметры выбора,
//  ИмяПоляВвода - Строка - имя поля ввода номенклатуры.
//
Процедура УстановитьПараметрыВыбораНоменклатуры(Форма, ИмяПоляВвода = "ТоварыНоменклатура") Экспорт
	
	// БольничнаяАптека
	ПараметрыВыбора = ОбщегоНазначенияКлиентСервер.СкопироватьМассив(Форма.Элементы[ИмяПоляВвода].ПараметрыВыбора);
	ПараметрыВыбора.Добавить(Новый ПараметрВыбора("Отбор.ТипНоменклатуры"           , Перечисления.ТипыНоменклатуры.Товар));
	ПараметрыВыбора.Добавить(Новый ПараметрВыбора("Отбор.ТипНоменклатурыРасширенный", Справочники.ТипыНоменклатурыРасширенные.ЛекарственноеСредство));
	
	Форма.Элементы[ИмяПоляВвода].ПараметрыВыбора = Новый ФиксированныйМассив(ПараметрыВыбора);
	
	СвязиПараметровВыбора = ОбщегоНазначенияКлиентСервер.СкопироватьМассив(Форма.Элементы[ИмяПоляВвода].СвязиПараметровВыбора);
	СвязиПараметровВыбора.Добавить(Новый СвязьПараметраВыбора("Отбор.GTIN", "Элементы." + СтрЗаменить(ИмяПоляВвода, "Номенклатура", ".ТекущиеДанные.GTIN")));
	
	Форма.Элементы[ИмяПоляВвода].СвязиПараметровВыбора = Новый ФиксированныйМассив(СвязиПараметровВыбора);
	// Конец БольничнаяАптека
	
	Возврат;
	
КонецПроцедуры

// Устанавливает связь элемента формы с полем ввода номенклатуры.
//
// Параметры:
//   Форма - УправляемаяФорма - форма, в которой нужно установить связь,
//   ИмяПоляВвода - Строка - имя поля, связываемого с номенклатурой,
//   ПутьКДаннымНоменклатуры - Строка - путь к текущей номенклатуре в форме.
//
Процедура УстановитьСвязиПараметровВыбораСНоменклатурой(Форма, ИмяПоляВвода, ПутьКДаннымНоменклатуры = "Элементы.Товары.ТекущиеДанные.Номенклатура") Экспорт
	
	// БольничнаяАптека
	Если СтрЗаканчиваетсяНа(ИмяПоляВвода, "Серия") Тогда
		СвязиПараметровВыбора = ОбщегоНазначенияКлиентСервер.СкопироватьМассив(Форма.Элементы[ИмяПоляВвода].СвязиПараметровВыбора);
		СвязиПараметровВыбора.Добавить(Новый СвязьПараметраВыбора("Отбор.Владелец", ПутьКДаннымНоменклатуры, РежимИзмененияСвязанногоЗначения.НеИзменять));
		
		Форма.Элементы[ИмяПоляВвода].СвязиПараметровВыбора = Новый ФиксированныйМассив(СвязиПараметровВыбора);
	КонецЕсли;
	// Конец БольничнаяАптека
	Возврат;
	
КонецПроцедуры

// Устанавливает условное оформление для поля "Единица измерения".
//
// Параметры:
//  Форма - УправляемаяФорма - форма, в которой нужно установить условное оформление,
//  ИмяПоляВводаЕдиницИзмерения - Строка - имя элемента формы "Единица измерения",
//  ПутьКПолюОтбора - Строка - полный путь к реквизиту "Упаковка".
//
Процедура УстановитьУсловноеОформлениеЕдиницИзмерения(Форма,
	                                                  ИмяПоляВводаЕдиницИзмерения = "ТоварыНоменклатураЕдиницаИзмерения",
	                                                  ПутьКПолюОтбора = "Объект.Товары.Упаковка") Экспорт
	
	
	Возврат;
	
КонецПроцедуры

// Устанавливает условное оформление для поля "Характеристика".
//
// Параметры:
//  Форма - УправляемаяФорма - форма, в которой нужно установить условное оформление,
//  ИмяПоляВводаХарактеристики - Строка - имя элемента формы "Характеристика",
//  ПутьКПолюОтбора - Строка - полный путь к реквизиту "Характеристики используются".
//
Процедура УстановитьУсловноеОформлениеХарактеристикНоменклатуры(Форма,
	                                                            ИмяПоляВводаХарактеристики = "ТоварыХарактеристика",
	                                                            ПутьКПолюОтбора = "Объект.Товары.ХарактеристикиИспользуются") Экспорт
	
	
	Возврат;
	
КонецПроцедуры

// Устанавливает условное оформление для поля "Серия".
//
// Параметры:
//  Форма - УправляемаяФорма - форма, в которой нужно установить условное оформление,
//
Процедура УстановитьУсловноеОформлениеСерийНоменклатуры(Форма,
	                                                    ИмяПоляВводаСерии = "ТоварыСерия",
	                                                    ПутьКПолюОтбораСтатусУказанияСерий = "Объект.Товары.СтатусУказанияСерий",
	                                                    ПутьКПолюОтбораТипНоменклатуры = "Объект.Товары.ТипНоменклатуры") Экспорт
	
	
	Возврат;
	
КонецПроцедуры

Процедура ОбработатьУказаниеСерий(Объект, ПараметрыУказанияСерий, ПараметрыФормыУказанияСерий, КэшированныеЗначения) Экспорт
	
	
	Возврат;
	
КонецПроцедуры

Функция ПараметрыУказанияСерийФормыОбъекта(Объект, МенеджерОбъекта) Экспорт
	
	
	Возврат Неопределено;
	
КонецФункции

Процедура ЗаполнитьСтатусыУказанияСерийПриОкончанииРедактированияСтрокиТЧ(Объект, ПараметрыУказанияСерий, ТекущаяСтрокаИдентификатор, КэшированныеЗначения) Экспорт
	
	
	Возврат;
	
КонецПроцедуры

Процедура ЗаполнитьСтатусыУказанияСерий(Объект, ПараметрыУказанияСерий) Экспорт
	
	
	Возврат;
	
КонецПроцедуры

Функция  ПараметрыФормыУказанияСерий(Объект, ПараметрыУказанияСерий, ТекущиеДанныеИдентификатор, ЭтаФорма) Экспорт
	
	
	Возврат Неопределено;
	
КонецФункции

// Имена реквизитов, от значений которых зависят параметры указания серий
//
// Возвращаемое значение:
//  Строка - имена реквизитов, перечисленные через запятую
//
Функция ИменаРеквизитовДляЗаполненияПараметровУказанияСерий(ТипДокумента) Экспорт
	
	ИменаРеквизитов = "";
	
	Возврат ИменаРеквизитов;
	
КонецФункции

// Возвращает параметры указания серий для товаров, указанных в документе.
//
// Параметры:
//  Объект - Структура - структура значений реквизитов объекта, необходимых для заполнения параметров указания серий.
// 
// Возвращаемое значение:
//  Структура - состав полей задается в функции ОбработкаТабличнойЧастиКлиентСервер.ПараметрыУказанияСерий.
//
Функция ПараметрыУказанияСерий(ТипДокумента, Объект) Экспорт
	
	Возврат Неопределено;
	
КонецФункции

Функция ТекстЗапросаЗаполненияСтатусовУказанияСерий(ТипОбъекта, ПараметрыУказанияСерий) Экспорт
	
	ТекстЗапроса = "";
	
	Возврат ТекстЗапроса;
	
КонецФункции

// Заполняет в табличной части служебные реквизиты, например: признак использования характеристик номенклатуры, артикул.
//
// Параметры:
//  Форма - УправляемаяФорма - Форма.
//  ТабличнаяЧасть - ДанныеФормыКоллекция, ТаблицаЗначений - таблица для заполнения.
//
Процедура ЗаполнитьСлужебныеРеквизитыВКоллекции(Форма, ТабличнаяЧасть) Экспорт
	
	// БольничнаяАптека
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ЗаполнитьЕдиницуИзмерения(), НоменклатураКлиентСервер.ВидЕдиницы_ПотребительскаяУпаковка());
	ОбработкаТабличнойЧастиСервер.ОбработатьТабличнуюЧасть(ТабличнаяЧасть, СтруктураДействий, Неопределено);
	// Конец БольничнаяАптека
	Возврат;
	
КонецПроцедуры

// Проверяет заполнение характеристик в таблице значений.
//
// Параметры:
//  ТаблицаТоваров - ТаблицаЗначений - таблица для проверки с колонками: Номенклатура, Характеристика.
//  Отказ - Булево - Истина - характеристики заполнены в требуемых строках, в противном случае - Ложь.
//
Процедура ПроверитьЗаполнениеХарактеристикВТаблицеЗначений(ТаблицаТоваров, Отказ) Экспорт
	
	
КонецПроцедуры

// В функции требуется определить признак использования характеристик для номенклатуры.
//
// Параметры:
//  Номенклатура - ОпределяемыйТип.Номенклатура - ссылка на элемент номенклатуры.
//
// Возвращаемое значение:
//   Булево - признак использования характеристик.
//
Функция ПризнакИспользованияХарактеристик(Номенклатура) Экспорт
	
	
	Возврат Ложь;
	
КонецФункции

// В функции требуется определить признак использования серий для номенклатуры.
//
// Параметры:
//  Номенклатура - ОпределяемыйТип.Номенклатура - ссылка на элемент номенклатуры.
//
// Возвращаемое значение:
//   Булево - признак использования серий.
//
Функция ПризнакИспользованияСерий(Номенклатура) Экспорт
	
	
	Возврат Ложь;
	
КонецФункции

// В функции требуется определить признак использования упаковок для номенклатуры.
//
// Параметры:
//  Номенклатура - ОпределяемыйТип.Номенклатура - ссылка на элемент номенклатуры.
//
// Возвращаемое значение:
//   Булево - признак использования упаковок.
//
Функция ПризнакИспользованияУпаковок(Номенклатура) Экспорт
	
	// БольничнаяАптека
	Возврат Истина;
	// Конец БольничнаяАптека
	Возврат Ложь;
	
КонецФункции

// В функции нужно реализовать получение и возврат базовой единицы измерения номенклатуры.
//
// Параметры:
//  Номенклатура - ОпределяемыйТип.Номенклатура - ссылка на элемент номенклатуры.
//
// Возвращаемое значение:
//   ОпределяемыйТип.Упаковка - базовая единица измерения.
//
Функция БазоваяЕдиницаИзмеренияНоменклатуры(Номенклатура) Экспорт
	
	
	Возврат Неопределено;
	
КонецФункции

// В процедуре нужно реализовать заполнение структуры данными из информационной базы.
//
// Параметры:
//  Организация - ОпределяемыйТип.ОрганизацияКонтрагентЕГАИС - ссылка на собственную организацию или контрагента,
//  Сведения - Структура - структура с реквизитами организации, которые требуется заполнить,
//  ДатаСведений - Дата - дата, на которую требуется получить информацию.
//
Процедура ЗаполнитьСведенияОбОрганизации(Организация, Сведения, ДатаСведений = Неопределено) Экспорт
	
	
КонецПроцедуры

// Возвращает строковое представление руководителя организации.
//
// Параметры:
//  Организация - ОпределяемыйТип.ОрганизацияКонтрагентЕГАИС - ссылка на собственную организацию или контрагента,
//  ДатаСведений - Дата - дата, на которую требуется получить информацию.
//
// Возвращаемое значение:
//  Структура - данные руководителя:
//   * Руководитель - Строка - ФИО руководителя,
//   * Должность - Строка - должность руководителя.
//
Функция ДанныеРуководителяОрганизации(Организация, ДатаСведений = Неопределено) Экспорт
	
	Результат = Новый Структура("Руководитель, Должность", "", "");
	
	
	Возврат Результат;
	
КонецФункции

#КонецОбласти
