
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформлениеФормы();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	Список.Параметры.УстановитьЗначениеПараметра("ТекущаяДата", ТекущаяДатаСеанса());
	
	БизнесПроцессыИЗадачиСервер.УстановитьФорматДаты(Элементы.Дата);
	БизнесПроцессыИЗадачиСервер.УстановитьФорматДаты(Элементы.СрокИсполнения);
	
	Если ПоИсполнителю = Неопределено Тогда
		ПоИсполнителю = Справочники.Пользователи.ПустаяСсылка();
	КонецЕсли;
	
	УстановитьОтбор();
	
КонецПроцедуры

&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	
	УстановитьОтбор();
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Остановить(Команда)
	
	БизнесПроцессыИЗадачиКлиент.Остановить(Элементы.Список.ВыделенныеСтроки);
	
КонецПроцедуры

&НаКлиенте
Процедура ПродолжитьБизнесПроцесс(Команда)
	
	БизнесПроцессыИЗадачиКлиент.СделатьАктивным(Элементы.Список.ВыделенныеСтроки);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиКомандФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ
#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура ПоАвторуПриИзменении(Элемент)
	
	УстановитьОтбор();
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказыватьЗавершенныеПриИзменении(Элемент)
	
	УстановитьОтбор();
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказыватьОстановленныеПриИзменении(Элемент)
	
	УстановитьОтбор();
	
КонецПроцедуры

&НаКлиенте
Процедура ПоИсполнителюНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	БизнесПроцессыИЗадачиКлиент.ВыбратьИсполнителя(
		Элемент,
		ПоИсполнителю,
		Ложь, // ТолькоПростыеРоли
		Истина); // БезВнешнихРолей
	
КонецПроцедуры

&НаКлиенте
Процедура ПоИсполнителюОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	Если ТипЗнч(ВыбранноеЗначение) = Тип("Структура") Тогда
		СтандартнаяОбработка = Ложь;
		
		ПоИсполнителю = ВыбранноеЗначение.РольИсполнителя;
		ОсновнойОбъектАдресации = ВыбранноеЗначение.ОсновнойОбъектАдресации;
		ДополнительныйОбъектАдресации = ВыбранноеЗначение.ДополнительныйОбъектАдресации;
		
		УстановитьОтбор();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПоИсполнителюАвтоПодбор(Элемент, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка)
	
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		ДанныеВыбора = БизнесПроцессыИЗадачиВызовСервера.СформироватьДанныеВыбораИсполнителя(Текст);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПоИсполнителюОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, СтандартнаяОбработка)
	
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		ДанныеВыбора = БизнесПроцессыИЗадачиВызовСервера.СформироватьДанныеВыбораИсполнителя(Текст);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПоИсполнителюПриИзменении(Элемент)
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ПоИсполнителюПриИзмененииЗавершение", ЭтотОбъект);
	
	БизнесПроцессыИЗадачиБольничнаяАптекаКлиент.ПриИзмененииУчастника(
		ЭтотОбъект,
		"ПоИсполнителю",
		"ОсновнойОбъектАдресации",
		"ДополнительныйОбъектАдресации",
		ЭтотОбъект,
		ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура ПоИсполнителюПриИзмененииЗавершение(Результат, Параметры) Экспорт
	
	УстановитьОтбор();
	
КонецПроцедуры

&НаКлиенте
Процедура ПоИсполнителюОчистка(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ПоИсполнителю = ПредопределенноеЗначение("Справочник.Пользователи.ПустаяСсылка");
	ОсновнойОбъектАдресации = Неопределено;
	ДополнительныйОбъектАдресации = Неопределено;
	
	УстановитьОтбор();
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийЭлементовФормы

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформлениеФормы()
	
	БизнесПроцессыИЗадачиСервер.УстановитьОформлениеБизнесПроцессов(Список.УсловноеОформление);
	
	БизнесПроцессыИЗадачиБольничнаяАптека.УстановитьОформлениеДлительностиПроцесса(Список.УсловноеОформление);
	
КонецПроцедуры

&НаСервере
Процедура УстановитьОтбор()
	
	ПараметрыОтбора = Новый Структура;
	ПараметрыОтбора.Вставить("ПоАвтору", ПоАвтору);
	ПараметрыОтбора.Вставить("ПоИсполнителю", ПоИсполнителю);
	ПараметрыОтбора.Вставить("ОсновнойОбъектАдресации", ОсновнойОбъектАдресации);
	ПараметрыОтбора.Вставить("ДополнительныйОбъектАдресации", ДополнительныйОбъектАдресации);
	ПараметрыОтбора.Вставить("ПоказыватьЗавершенные"  , ПоказыватьЗавершенные);
	ПараметрыОтбора.Вставить("ПоказыватьОстановленные", ПоказыватьОстановленные);
	УстановитьОтборСписка(Список, ПараметрыОтбора);
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура УстановитьОтборСписка(Список, ПараметрыОтбора)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Автор", ПараметрыОтбора.ПоАвтору,,, Не ПараметрыОтбора.ПоАвтору.Пустая());
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Завершен", Ложь,,, Не ПараметрыОтбора.ПоказыватьЗавершенные);
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Остановлен", Ложь,,, Не ПараметрыОтбора.ПоказыватьОстановленные);
	
	ПоИсполнителю = ПараметрыОтбора.ПоИсполнителю;
	ОсновнойОбъектАдресации = ПараметрыОтбора.ОсновнойОбъектАдресации;
	ДополнительныйОбъектАдресации = ПараметрыОтбора.ДополнительныйОбъектАдресации;
	
	ОтборПоИсполнителю = ЗначениеЗаполнено(ПоИсполнителю);
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Исполнитель", ПоИсполнителю,,, ОтборПоИсполнителю);
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
		Список,
		"ОсновнойОбъектАдресации",
		ОсновнойОбъектАдресации,
		,
		,
		ОтборПоИсполнителю И ЗначениеЗаполнено(ОсновнойОбъектАдресации));
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
		Список,
		"ДополнительныйОбъектАдресации",
		ДополнительныйОбъектАдресации,
		,
		,
		ОтборПоИсполнителю И ЗначениеЗаполнено(ДополнительныйОбъектАдресации));
	
КонецПроцедуры

#КонецОбласти // СлужебныеПроцедурыИФункции
