#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Возвращает идентификатор организации субъекта обращения.
//
// Параметры:
//  ИдентификаторСубъектаОбращения - Строка(14) - идентификатор субъекта обращения;
//  СобственнаяОрганизация - Булево - признак собственной организации.
//
// Возвращаемое значение:
//  Строка, Неопределено - идентификатор организации заданный для субъекта обращения.
//
Функция ИдентификаторОрганизацииСубъектаОбращения(ИдентификаторСубъектаОбращения, СобственнаяОрганизация = Истина) Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	СубъектОбращения.ИдентификаторОрганизации  КАК ИдентификаторОрганизации
	|ИЗ
	|	РегистрСведений.СубъектыОбращенияМДЛП КАК СубъектОбращения
	|ГДЕ
	|	СубъектОбращения.СобственнаяОрганизация = &СобственнаяОрганизация
	|	И СубъектОбращения.ИдентификаторСубъектаОбращения = &ИдентификаторСубъектаОбращения
	|");
	
	Запрос.УстановитьПараметр("ИдентификаторСубъектаОбращения", ИдентификаторСубъектаОбращения);
	Запрос.УстановитьПараметр("СобственнаяОрганизация", СобственнаяОрганизация);
	
	Результат = Запрос.Выполнить();
	Если Результат.Пустой() Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	Выборка = Результат.Выбрать();
	Выборка.Следующий();
	
	Возврат Выборка.ИдентификаторОрганизации;
	
КонецФункции

#КонецОбласти

#КонецЕсли