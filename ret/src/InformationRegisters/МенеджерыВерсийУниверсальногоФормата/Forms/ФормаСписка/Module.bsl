
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	Элементы.ПланОбмена.Видимость = Не Параметры.Отбор.Свойство("ПланОбмена");
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ВыгрузитьВФайлОбработку(Команда)
	
	ТекущиеДанные = Элементы.Список.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ПараметрыВыгрузки = СформироватьПараметрыВыгрузки(ТекущиеДанные.ПланОбмена, ТекущиеДанные.ВерсияФорматаОбмена, УникальныйИдентификатор);
	Если ПараметрыВыгрузки <> Неопределено И Не ПустаяСтрока(ПараметрыВыгрузки.АдресДанныхОбработки) Тогда
		ТекстСообщения = НСтр("ru = 'Для выгрузки внешнего менеджера обмена через универсальный формат рекомендуется установить расширение для веб-клиента 1С:Предприятие.'");
		Оповестить = Новый ОписаниеОповещения("ВыгрузитьВФайлОбработкуПродолжение", ЭтотОбъект, ПараметрыВыгрузки);
		ОбщегоНазначенияКлиент.ПоказатьВопросОбУстановкеРасширенияРаботыСФайлами(Оповестить, ТекстСообщения);
	Иначе
		ТекстОповещения = НСтр("ru = 'Файл обработки не выгружен'");
		Пояснение = НСтр("ru = 'Файл менеджера версии формата не обнаружен в программе.'");
		ПоказатьОповещениеПользователя(ТекстОповещения,, Пояснение, БиблиотекаКартинок.Предупреждение_32);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьФайлОбработки(Команда)
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("Ключ", Элементы.Список.ТекущаяСтрока);
	ПараметрыФормы.Вставить("ПоказатьДиалогЗагрузкиИзФайлаПриОткрытии", Истина);
	
	ОткрытьФорму("РегистрСведений.МенеджерыВерсийУниверсальногоФормата.ФормаЗаписи", ПараметрыФормы);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиКомандФормы

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

// Продолжение процедуры ВыгрузитьВФайлОбработку.
//
&НаКлиенте
Процедура ВыгрузитьВФайлОбработкуПродолжение(Подключено, ПараметрыВыгрузки) Экспорт
	
	Если Не Подключено Тогда
		ПолучитьФайл(ПараметрыВыгрузки.АдресДанныхОбработки, ПараметрыВыгрузки.ИмяФайла, Истина);
		Возврат;
	КонецЕсли;
	
	ДиалогСохраненияФайла = Новый ДиалогВыбораФайла(РежимДиалогаВыбораФайла.Сохранение);
	ДиалогСохраненияФайла.ПолноеИмяФайла     = ПараметрыВыгрузки.ИмяФайла;
	ДиалогСохраненияФайла.Фильтр             = НСтр("ru = 'Внешние обработки (*.epf)|*.epf'");
	ДиалогСохраненияФайла.Заголовок          = НСтр("ru = 'Укажите файл'");
	
	Оповестить = Новый ОписаниеОповещения("ВыгрузитьВФайлОбработкуПослеВыбораФайла", ЭтотОбъект, ПараметрыВыгрузки);
	ДиалогСохраненияФайла.Показать(Оповестить);
	
КонецПроцедуры

// Продолжение процедуры ВыгрузитьВФайлОбработкуПродолжение.
//
&НаКлиенте
Процедура ВыгрузитьВФайлОбработкуПослеВыбораФайла(ВыбранныеФайлы, ПараметрыВыгрузки) Экспорт
	
	Если ВыбранныеФайлы <> Неопределено Тогда
		
		ПолноеИмяФайла = ВыбранныеФайлы[0];
		ПолучаемыеФайлы = Новый Массив;
		ПолучаемыеФайлы.Добавить(Новый ОписаниеПередаваемогоФайла(ПолноеИмяФайла, ПараметрыВыгрузки.АдресДанныхОбработки));
		
		Оповестить = Новый ОписаниеОповещения("ВыгрузитьВФайлОбработкуЗавершение", ЭтотОбъект, ПараметрыВыгрузки);
		НачатьПолучениеФайлов(Оповестить, ПолучаемыеФайлы, ПолноеИмяФайла, Ложь);
		
	КонецЕсли;
	
КонецПроцедуры

// Продолжение процедуры ВыгрузитьВФайлОбработкуПослеВыбораФайла.
//
&НаКлиенте
Процедура ВыгрузитьВФайлОбработкуЗавершение(ПолученныеФайлы, ПараметрыВыгрузки) Экспорт
	
	Если ПолученныеФайлы <> Неопределено Тогда
		ТекстОповещения = НСтр("ru = 'Файл внешней обработки выгружен'");
		ПоказатьОповещениеПользователя(ТекстОповещения,, ПараметрыВыгрузки.ИмяФайла, БиблиотекаКартинок.ЗеленаяГалка);
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция СформироватьПараметрыВыгрузки(ПланОбмена, ВерсияФорматаОбмена, Адрес)
	
	Возврат РегистрыСведений.МенеджерыВерсийУниверсальногоФормата.ДанныеВерсииФормата(ПланОбмена, ВерсияФорматаОбмена, Адрес);
	
КонецФункции

#КонецОбласти // СлужебныеПроцедурыИФункции
