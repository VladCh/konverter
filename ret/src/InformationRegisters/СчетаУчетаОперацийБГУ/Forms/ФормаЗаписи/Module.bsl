
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформлениеФормы();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Запись, ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	Если Не ПустаяСтрока(Запись.СчетУчетаВыдачиМЗНаНуждыУчреждения) И Элементы.СчетУчетаВыдачиМЗНаНуждыУчреждения.СписокВыбора.НайтиПоЗначению(Запись.СчетУчетаВыдачиМЗНаНуждыУчреждения) = Неопределено Тогда
		Элементы.СчетУчетаВыдачиМЗНаНуждыУчреждения.СписокВыбора.Вставить(0, Запись.СчетУчетаВыдачиМЗНаНуждыУчреждения);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	РесурсыРегистра = Метаданные.РегистрыСведений.СчетаУчетаОперацийБГУ.Ресурсы;
	ИменаПолейКПС = ИменаПолейКПС();
	Для Каждого ИмяПоляКПС Из ИменаПолейКПС Цикл
		
		КПС = Запись[ИмяПоляКПС];
		
		Если Не ПустаяСтрока(КПС) И СтрДлина(КПС) < 17 Тогда
			ПредставлениеПоляКПС = РесурсыРегистра[ИмяПоляКПС].Представление();
			ТекстСообщения = НСтр("ru = 'Количество символов не может быть отлично от 17.'");
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Поле", "Корректность", ПредставлениеПоляКПС,,, ТекстСообщения);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,, "Запись." + ИмяПоляКПС,, Отказ);
		КонецЕсли;
		
		Если Не ПустаяСтрока(КПС) И Не ЗначениеЗаполнено(Запись["Вид" + ИмяПоляКПС]) Тогда
			ПредставлениеПоляВидКПС = РесурсыРегистра["Вид" + ИмяПоляКПС].Представление();
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Поле", "Заполнение", ПредставлениеПоляВидКПС);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,, "Запись.Вид" + ИмяПоляКПС,, Отказ);
		КонецЕсли;
		
	КонецЦикла;
	
	Если Не СтрНачинаетсяС(Запись.СчетУчетаВыдачиМЗНаНуждыУчреждения, "401.2")
	   И Не СтрНачинаетсяС(Запись.СчетУчетаВыдачиМЗНаНуждыУчреждения, "109.") Тогда
		ПредставлениеПоля = РесурсыРегистра.СчетУчетаВыдачиМЗНаНуждыУчреждения.Представление();
		ТекстСообщения = НСтр("ru = 'Счет должен входить в группу 109 или 401.2.'");
		ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Поле", "Корректность", ПредставлениеПоля,,, ТекстСообщения);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,, "Запись.СчетУчетаВыдачиМЗНаНуждыУчреждения",, Отказ);
	КонецЕсли;
	
	Если Не СтрНачинаетсяС(Запись.СчетУчетаСписанияМЗНаПациентов, "401.2")
	   И Не СтрНачинаетсяС(Запись.СчетУчетаСписанияМЗНаПациентов, "109.6") Тогда
		ПредставлениеПоля = РесурсыРегистра.СчетУчетаСписанияМЗНаПациентов.Представление();
		ТекстСообщения = НСтр("ru = 'Счет должен входить в группу 109.6 или 401.2.'");
		ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Поле", "Корректность", ПредставлениеПоля,,, ТекстСообщения);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,, "Запись.СчетУчетаСписанияМЗНаПациентов",, Отказ);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийФормы

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформлениеФормы()
	
	ИменаПолейКПС = ИменаПолейКПС();
	Для Каждого ИмяПоляКПС Из ИменаПолейКПС Цикл
		
		Элемент = УсловноеОформление.Элементы.Добавить();
		
		ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
		ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы["Вид" + ИмяПоляКПС].Имя);
		
		ОбщегоНазначенияКлиентСервер.ДобавитьЭлементКомпоновки(Элемент.Отбор,
			"Запись." + ИмяПоляКПС, ВидСравненияКомпоновкиДанных.Заполнено);
		ОбщегоНазначенияКлиентСервер.ДобавитьЭлементКомпоновки(Элемент.Отбор,
			"Запись.Вид" + ИмяПоляКПС, ВидСравненияКомпоновкиДанных.НеЗаполнено);
		
		Элемент.Оформление.УстановитьЗначениеПараметра("ОтметкаНезаполненного", Истина);
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Функция ИменаПолейКПС()
	
	ИменаПолейКПС = Новый Массив;
	ИменаПолейКПС.Добавить("КПСПоСчету105");
	ИменаПолейКПС.Добавить("КПСРасчетыСПоставщиками");
	ИменаПолейКПС.Добавить("КПССписаниеНедостач");
	ИменаПолейКПС.Добавить("КПСОприходованиеИзлишков");
	ИменаПолейКПС.Добавить("КПСБезвозмездноеПолучениеМежбюджетное");
	ИменаПолейКПС.Добавить("КПСБезвозмездноеПолучениеПрочее");
	ИменаПолейКПС.Добавить("КПСПередачиТоваровНаСторону");
	ИменаПолейКПС.Добавить("КПСРозницы");
	ИменаПолейКПС.Добавить("КПСДохода");
	
	Возврат ИменаПолейКПС;
	
КонецФункции

#КонецОбласти // СлужебныеПроцедурыИФункции