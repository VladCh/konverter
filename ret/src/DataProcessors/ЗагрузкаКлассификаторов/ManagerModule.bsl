#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

Процедура ЗаполнитьКлассификатор(Знач Параметры, Знач АдресРезультатаЗаполнения) Экспорт
	
	ОписаниеФайлов    = Параметры.ОписаниеФайлов;
	КаталогНазначения = ПолучитьИмяВременногоФайла();
	
	МассивИменФайлов  = ПолучитьПомещенныеФайлы(КаталогНазначения, ОписаниеФайлов);
	
	ПолноеИмяФайла = ?(МассивИменФайлов.Количество() = 1, МассивИменФайлов[0], "");
	Если СтрЗаканчиваетсяНа(ВРег(ПолноеИмяФайла), ".ZIP") Тогда
		ПолноеИмяФайла = ПолучитьФайлИзАрхива(КаталогНазначения, ПолноеИмяФайла, Параметры.ИмяФайлаПоУмолчанию);
	КонецЕсли;
	
	МетаданныеСправочника = Метаданные.Справочники[Параметры.ИмяСправочникаИБ];
	ПредставлениеСправочника = МетаданныеСправочника.Представление();
	СправочникМенеджер = Справочники[МетаданныеСправочника.Имя];
	
	Попытка
		ДеревоРезультат = СправочникМенеджер.ПрочитатьФайлКлассификатора(ПолноеИмяФайла);
	Исключение
		ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru = 'При чтении классификатора ""%1"" произошла ошибка: %2'"),
			ПредставлениеСправочника,
			КраткоеПредставлениеОшибки(ИнформацияОбОшибке()));
		ВызватьИсключение ТекстОшибки;
	КонецПопытки;
	
	Результат = Новый Структура;
	Результат.Вставить("ДеревоКлассификатора", ДеревоРезультат);
	
	ПоместитьВоВременноеХранилище(Результат, АдресРезультатаЗаполнения);
	
КонецПроцедуры

Процедура ЗагрузитьКлассификатор(ПараметрыЗагружаемыхКлассификаторов, АдресРезультатаЗагрузки) Экспорт
	
	ДеревоСПометкамиНеполное = ПараметрыЗагружаемыхКлассификаторов.ДеревоСПометкамиНеполное;
	ДеревоПолное = ПараметрыЗагружаемыхКлассификаторов.ДеревоБезПометок;
	
	СтрокиДереваСПометками = ДеревоСПометкамиНеполное.Строки;
	СтрокиПолногоДерева = ДеревоПолное.Строки;
	
	Отбор = Новый Структура("Пометка", 1);
	МассивСтрокДерева = СтрокиДереваСПометками.НайтиСтроки(Отбор, Истина);
	
	Если МассивСтрокДерева.Количество() <= 0 Тогда
		ВызватьИсключение НСтр("ru = 'Не выбрано ни одного элемента классификатора.'");
	КонецЕсли;
	
	УстановитьПометкиВДереве(СтрокиПолногоДерева, МассивСтрокДерева);
	
	КоличествоЗагружаемыхЭлементов = СтрокиПолногоДерева.Итог("Пометка", Истина);
	
	Колонки = ДеревоПолное.Колонки;
	СтруктураОписаниеСправочника = ПолучитьОписаниеСправочника(ПараметрыЗагружаемыхКлассификаторов.ПустаяСсылка.Метаданные());
	
	ЗагрузитьСтроки(СтрокиПолногоДерева, Колонки, СтруктураОписаниеСправочника, КоличествоЗагружаемыхЭлементов);
	
КонецПроцедуры

#КонецОбласти // ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

Функция ПолучитьПомещенныеФайлы(КаталогНазначения, ОписаниеФайлов)
	
	МассивИменФайлов = Новый Массив;
	
	Для Каждого Описание Из ОписаниеФайлов Цикл
		Файл = Новый Файл(Описание.Имя);
		ИмяФайла = ФайловыеФункцииБольничнаяАптекаКлиентСервер.ПолучитьУникальныйПутьКФайлу(КаталогНазначения, ВРег(Файл.Имя));
		
		Если ЭтоАдресВременногоХранилища(Описание.Хранение) Тогда
			Данные = ПолучитьИзВременногоХранилища(Описание.Хранение);
		Иначе
			Данные = Описание.Хранение;
		КонецЕсли;
		
		Попытка
			Данные.Записать(ИмяФайла);
		Исключение
			ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				НСтр("ru = 'Не удалось получить данные по описанию файлов. %1'"),
				КраткоеПредставлениеОшибки(ИнформацияОбОшибке()));
			ВызватьИсключение ТекстОшибки;
		КонецПопытки;
		
		МассивИменФайлов.Добавить(ИмяФайла);
		
	КонецЦикла;
	
	Возврат МассивИменФайлов;
	
КонецФункции

Функция ПолучитьФайлИзАрхива(КаталогНазначения, ПутьКФайлуАрхива, ИмяЭлементаАрхива)
	
	Если Не ФайловыеФункцииБольничнаяАптекаКлиентСервер.ФайлСуществует(ПутьКФайлуАрхива) Тогда
		ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru = 'Извлечение файлов из архива. Файл архива ""%1"" не найден.'"), ПутьКФайлуАрхива);
		ВызватьИсключение ТекстОшибки;
	КонецЕсли;
	
	ZIPФайл       = Новый ЧтениеZipФайла(ПутьКФайлуАрхива);
	ЭлементАрхива = ZIPФайл.Элементы.Найти(ВРег(ИмяЭлементаАрхива));
	
	Если ЭлементАрхива = Неопределено Тогда
		ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru = 'Извлечение файлов из архива. Файл ""%1"" в архиве ""%2"" не найден.'"),
			ИмяЭлементаАрхива,
			ПутьКФайлуАрхива);
		ВызватьИсключение ТекстОшибки;
	КонецЕсли;
	
	ZIPФайл.Извлечь(ЭлементАрхива, КаталогНазначения, РежимВосстановленияПутейФайловZIP.НеВосстанавливать);
	ZIPФайл.Закрыть();
	
	Возврат ФайловыеФункцииБольничнаяАптекаКлиентСервер.ПолучитьПолныйПутьКФайлу(КаталогНазначения, ИмяЭлементаАрхива);
	
КонецФункции

Функция ПолучитьОписаниеСправочника(МетаданныеСправочника)
	
	СтруктураОписаниеСправочника = Новый Структура;
	СтруктураОписаниеСправочника.Вставить("МетаданныеСправочника"  , МетаданныеСправочника);
	СтруктураОписаниеСправочника.Вставить("ЕстьКод"                , МетаданныеСправочника.ДлинаКода > 0);
	СтруктураОписаниеСправочника.Вставить("ЕстьНаименование"       , МетаданныеСправочника.ДлинаНаименования > 0);
	СтруктураОписаниеСправочника.Вставить("ИмяСправочникаИБ"       , МетаданныеСправочника.Имя);
	СтруктураОписаниеСправочника.Вставить("СправочникДляОбновления", Справочники[МетаданныеСправочника.Имя]);
	СтруктураОписаниеСправочника.Вставить("Иерархия"               , МетаданныеСправочника.Иерархический);
	СтруктураОписаниеСправочника.Вставить("ИерархияЭлементов"      , (МетаданныеСправочника.ВидИерархии = Метаданные.СвойстваОбъектов.ВидИерархии.ИерархияЭлементов));
	
	СтруктураРеквизитов = Новый Структура;
	Для каждого Реквизит Из МетаданныеСправочника.Реквизиты Цикл
		СтруктураРеквизитов.Вставить(Реквизит.Имя);
	КонецЦикла;
	Для каждого Реквизит Из МетаданныеСправочника.СтандартныеРеквизиты Цикл
		СтруктураРеквизитов.Вставить(Реквизит.Имя);
	КонецЦикла;
	СтруктураОписаниеСправочника.Вставить("Реквизиты", СтруктураРеквизитов);
	
	СтруктураТабличныхЧастей = Новый Структура;
	Для каждого Реквизит Из МетаданныеСправочника.ТабличныеЧасти Цикл
		СтруктураТабличныхЧастей.Вставить(Реквизит.Имя);
	КонецЦикла;
	СтруктураОписаниеСправочника.Вставить("ТабличныеЧасти", СтруктураТабличныхЧастей);
	
	Возврат СтруктураОписаниеСправочника;
	
КонецФункции

Процедура ЗагрузитьСтроки(Строки, Колонки, СтруктураОписаниеСправочника, КоличествоЗагружаемыхЭлементов, КоличествоПройденныхСтрок = 0)
	
	КоличествоПройденныхСтрок = КоличествоПройденныхСтрок + Строки.Итог("Пометка");
	ТекстСтатуса = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru = 'Загрузка данных... Загружено %1 из %2 элементов...'"),
		Строка(КоличествоПройденныхСтрок),
		Строка(КоличествоЗагружаемыхЭлементов));
	ДлительныеОперации.СообщитьПрогресс(100 * КоличествоПройденныхСтрок / КоличествоЗагружаемыхЭлементов, ТекстСтатуса);
	
	Родитель = Неопределено;
	
	РодительСтрокДерева = Строки.Родитель;
	Если СтруктураОписаниеСправочника.Иерархия И РодительСтрокДерева <> Неопределено Тогда
		Родитель = СтруктураОписаниеСправочника.СправочникДляОбновления.НайтиПоКоду(РодительСтрокДерева.Код);
		Если НЕ Родитель.Пустая() Тогда
			Если НЕ СтруктураОписаниеСправочника.ИерархияЭлементов Тогда
				Если НЕ Родитель.ЭтоГруппа Тогда
					Возврат;
				КонецЕсли;
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	
	Для Индекс = 0 По Строки.Количество() - 1 Цикл
		
		СправочникОбъект = Неопределено;
		
		ТекущаяСтрокаДерева = Строки.Получить(Индекс);
		
		Если ТекущаяСтрокаДерева.Пометка = Неопределено Или (ТекущаяСтрокаДерева.Пометка = 0) Тогда
			Продолжить;
		КонецЕсли;
		
		НачатаТранзакция = (ТекущаяСтрокаДерева.Уровень() = 0);
		Если НачатаТранзакция Тогда
			НачатьТранзакцию();
		КонецЕсли;
		
		Попытка
			Если СтруктураОписаниеСправочника.ЕстьКод Тогда
				Ссылка = СтруктураОписаниеСправочника.СправочникДляОбновления.НайтиПоКоду(ТекущаяСтрокаДерева.Код);
			ИначеЕсли СтруктураОписаниеСправочника.ЕстьНаименование Тогда
				Ссылка = СтруктураОписаниеСправочника.СправочникДляОбновления.НайтиПоНаименованию(ТекущаяСтрокаДерева.Наименование);
			Иначе
				ВызватьИсключение НСтр("ru = 'Поиск по коду и наименованию не дал результатов.'");
			КонецЕсли;
			
			Если Ссылка.Пустая() Или Ссылка.ЭтоГруппа <> ТекущаяСтрокаДерева.ЭтоГруппа Тогда
				
				Если Не Ссылка.Пустая() Тогда
					Данные = Ссылка.ПолучитьОбъект();
					Данные.Код = "000000";
					Данные.ПометкаУдаления = Истина;
					Если СтруктураОписаниеСправочника.Иерархия Тогда
						Данные.Родитель = Неопределено;
					КонецЕсли;
					ОбновлениеИнформационнойБазы.ЗаписатьДанные(Данные, Истина);
				КонецЕсли;
				
				Если СтруктураОписаниеСправочника.Иерархия И Не СтруктураОписаниеСправочника.ИерархияЭлементов И ТекущаяСтрокаДерева.ЭтоГруппа Тогда
					СправочникОбъект = СтруктураОписаниеСправочника.СправочникДляОбновления.СоздатьГруппу();
				Иначе
					СправочникОбъект = СтруктураОписаниеСправочника.СправочникДляОбновления.СоздатьЭлемент();
				КонецЕсли;
			КонецЕсли;
			
			Если СправочникОбъект <> Неопределено Тогда
				
				ЗаполнитьЗначенияСвойств(СправочникОбъект, ТекущаяСтрокаДерева,, "Родитель");
				
				Для Сч = 1 По Колонки.Количество() - 1 Цикл
					Колонка = Колонки[Сч]; 
					ИмяКолонки = Колонка.Имя;
					Если СтруктураОписаниеСправочника.ТабличныеЧасти.Свойство(ИмяКолонки) Тогда
						Если ТипЗнч(ТекущаяСтрокаДерева[ИмяКолонки]) = Тип("ТаблицаЗначений") Тогда
							СправочникОбъект[ИмяКолонки].Загрузить(ТекущаяСтрокаДерева[ИмяКолонки]);
						КонецЕсли;
					КонецЕсли;
				КонецЦикла;
				
				Если СтруктураОписаниеСправочника.Иерархия Тогда
					СправочникОбъект.Родитель = Родитель;
				КонецЕсли;
				
				Попытка
					СправочникОбъект.Записать();
				Исключение
					ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
						НСтр("ru = 'Ошибка при записи элемента с кодом %1: %2'"),
						ТекущаяСтрокаДерева.Код,
						КраткоеПредставлениеОшибки(ИнформацияОбОшибке()));
					ВызватьИсключение ТекстОшибки;
				КонецПопытки;
				
			КонецЕсли;
			
			ЗагрузитьСтроки(ТекущаяСтрокаДерева.Строки, Колонки, СтруктураОписаниеСправочника, КоличествоЗагружаемыхЭлементов, КоличествоПройденныхСтрок);
			
			Если НачатаТранзакция Тогда
				ЗафиксироватьТранзакцию();
			КонецЕсли;
			
		Исключение
			Если НачатаТранзакция Тогда
				ОтменитьТранзакцию();
			КонецЕсли;
			ВызватьИсключение;
		КонецПопытки;
		
	КонецЦикла;
	
КонецПроцедуры

Процедура УстановитьПометкиВДереве(СтрокиДереваБезПометок, МассивСтрокДерева)
	
	ТекстСтатуса = НСтр("ru = 'Получение помеченных элементов...'");
	ДлительныеОперации.СообщитьПрогресс(0, ТекстСтатуса);
	ТипЧисло = ОбщегоНазначения.ОписаниеТипаЧисло(15);
	
	Для Каждого СтрокаДерева Из МассивСтрокДерева Цикл
		
		СтрокаДереваБезПометок = СтрокиДереваБезПометок.Найти(СтрокаДерева.Код, "Код", Истина);
		
		Если СтрокаДереваБезПометок = Неопределено Тогда
			КодЧислом = ТипЧисло.ПривестиЗначение(СтрокаДерева.Код);
			Если ЗначениеЗаполнено(КодЧислом) Тогда
				СтрокаДереваБезПометок = СтрокиДереваБезПометок.Найти(КодЧислом, "Код", Истина);
			КонецЕсли;
		КонецЕсли;
		
		Если СтрокаДереваБезПометок <> Неопределено Тогда
			УстановитьПометкиРодителям(СтрокаДереваБезПометок);
			УстановитьПометкиПодчиненнымЭлементам(СтрокаДереваБезПометок.Строки);
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Процедура УстановитьПометкиРодителям(СтрокаДерева)
	
	СтрокаДерева.Пометка = Истина;
	РодительСтроки = СтрокаДерева.Родитель;
	
	Если РодительСтроки <> Неопределено Тогда
		УстановитьПометкиРодителям(РодительСтроки);
	КонецЕсли;
	
КонецПроцедуры

Процедура УстановитьПометкиПодчиненнымЭлементам(ПодчиненныеСтроки)
	
	Для Каждого СтрокаДерева Из ПодчиненныеСтроки Цикл
		
		СтрокаДерева.Пометка = Истина;
		СтрокиДерева = СтрокаДерева.Строки;
		
		Если СтрокиДерева.Количество() > 0 Тогда
			УстановитьПометкиПодчиненнымЭлементам(СтрокиДерева);
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти // СлужебныеПроцедурыИФункции

#КонецЕсли