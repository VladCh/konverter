#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// Формирование этикеток и ценников
#Область ФормированиеЭтикетокИЦенников

// Функция подготавливает структуру данных, необходимую для печати ценников и этикеток.
//
// Возвращаемое значение:
//  Структура - данные, необходимые для печати этикеток и ценников.
//
Функция ПодготовитьСтруктуруДанных(СтруктураНастроек) Экспорт
	
	Если СтруктураНастроек.ИсходныеДанные <> Неопределено Тогда
		ИсходныеДанные = СтруктураНастроек.ИсходныеДанные.Скопировать();
	Иначе
		ИсходныеДанные = Неопределено;
	КонецЕсли;
	
	СтруктураРезультата = ПолучитьПустуюСтруктуруРезультата();
	
	////////////////////////////////////////////////////////////////////////////////
	// ПОДГОТОВКА СХЕМЫ КОМПОНОВКИ ДАННЫХ И КОМПОНОВЩИКА НАСТРОЕК СКД
	
	// Схема компоновки.
	СхемаКомпоновкиДанных = Обработки.ПечатьЭтикетокИЦенников.ПолучитьМакет(СтруктураНастроек.ИмяМакетаСхемыКомпоновкиДанных);
	
	// Подготовка компоновщика макета компоновки данных.
	Компоновщик = Новый КомпоновщикНастроекКомпоновкиДанных;
	Компоновщик.Инициализировать(Новый ИсточникДоступныхНастроекКомпоновкиДанных(СхемаКомпоновкиДанных));
	Компоновщик.ЗагрузитьНастройки(СхемаКомпоновкиДанных.НастройкиПоУмолчанию);
	Компоновщик.Настройки.Отбор.Элементы.Очистить();
	
	// Отбор компоновщика настроек.
	Если СтруктураНастроек.КомпоновщикНастроек <> Неопределено Тогда
		СтруктураНастроек.КомпоновщикНастроек.Восстановить();
		ОбщегоНазначенияБольничнаяАптекаКлиентСервер.СкопироватьЭлементы(Компоновщик.Настройки.Отбор, СтруктураНастроек.КомпоновщикНастроек.Настройки.Отбор);
	КонецЕсли;
	
	// Выбранные поля компоновщика настроек.
	Для Каждого ОбязательноеПоле Из СтруктураНастроек.ОбязательныеПоля Цикл
		ПолеСКД = ОбщегоНазначенияБольничнаяАптека.НайтиПолеСКДПоПолномуИмени(Компоновщик.Настройки.Выбор.ДоступныеПоляВыбора.Элементы, ОбязательноеПоле);
		Если ПолеСКД <> Неопределено Тогда
			ВыбранноеПоле = Компоновщик.Настройки.Выбор.Элементы.Добавить(Тип("ВыбранноеПолеКомпоновкиДанных"));
			ВыбранноеПоле.Поле = ПолеСКД.Поле;
		КонецЕсли;
	КонецЦикла;
	
	// Заполнение параметров.
	Для Каждого ПараметрДанных Из СтруктураНастроек.ПараметрыДанных Цикл
		Если ПараметрДанных.Ключ = "Склад" Тогда
			УстановитьЗначениеПараметраСКД(Компоновщик, ПараметрДанных.Ключ, ПараметрДанных.Значение, Ложь);
		ИначеЕсли ПараметрДанных.Ключ = "Организация" Тогда
			УстановитьЗначениеПараметраСКД(Компоновщик, ПараметрДанных.Ключ, ПараметрДанных.Значение, Ложь);
		Иначе
			УстановитьЗначениеПараметраСКД(Компоновщик, ПараметрДанных.Ключ, ПараметрДанных.Значение);
		КонецЕсли;
	КонецЦикла;
	УстановитьЗначениеПараметраСКД(Компоновщик, "ТекущееВремя",        ТекущаяДатаСеанса());
	УстановитьЗначениеПараметраСКД(Компоновщик, "ТекущийПользователь", Пользователи.ТекущийПользователь());
	
	// Компоновка макета компоновки данных.
	КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
	МакетКомпоновкиДанных = КомпоновщикМакета.Выполнить(СхемаКомпоновкиДанных, Компоновщик.Настройки,,,Тип("ГенераторМакетаКомпоновкиДанныхДляКоллекцииЗначений"));
	
	////////////////////////////////////////////////////////////////////////////////
	// ПОДГОТОВКА ВСПОМОГАТЕЛЬНЫХ ДАННЫХ ДЛЯ СОПОСТАВЛЕНИЯ ПОЛЕЙ ШАБЛОНА И СКД
	
	Для каждого Поле Из МакетКомпоновкиДанных.НаборыДанных.НаборДанных.Поля Цикл
		СтруктураРезультата.СоответствиеПолейСКДКолонкамТаблицыТоваров.Вставить(Справочники.ШаблоныЭтикетокИЦенников.ИмяПоляВШаблоне(Поле.ПутьКДанным), Поле.Имя);
	КонецЦикла;
	
	////////////////////////////////////////////////////////////////////////////////
	// ВЫПОЛНЕНИЕ ЗАПРОСА
	
	Запрос = Новый Запрос(МакетКомпоновкиДанных.НаборыДанных.НаборДанных.Запрос);
	
	// Заполнение параметров с полей отбора компоновщика настроек формы обработки.
	Для каждого Параметр Из МакетКомпоновкиДанных.ЗначенияПараметров Цикл
		Запрос.Параметры.Вставить(Параметр.Имя, Параметр.Значение);
	КонецЦикла;
		
	// Подмена запроса при печати этикеток...
	Если ИсходныеДанные <> Неопределено Тогда
		
		ТекстВременнойТаблицы =
		"	(ВЫБРАТЬ
		|		ЗНАЧЕНИЕ(Справочник.Номенклатура.ПустаяСсылка) КАК Номенклатура,
		|		ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка) КАК СерияНоменклатуры,
		|		ЗНАЧЕНИЕ(Справочник.ПартииНоменклатуры.ПустаяСсылка) КАК Партия,
		|		ЗНАЧЕНИЕ(Справочник.ЕдиницыИзмерения.ПустаяСсылка) КАК Упаковка,
		|		0 КАК Порядок,
		|		0 КАК Количество,
		|		0 КАК НомерСтроки)";
		
		Запрос.Текст = СтрЗаменить(Запрос.Текст, ТекстВременнойТаблицы, "&Таблица");
		
		ПронумероватьТаблицуЗначений(ИсходныеДанные, "Порядок");
		Запрос.Параметры.Вставить("Таблица", ИсходныеДанные);
		
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)", "ИсходныеДанные.Организация");
		Если ИсходныеДанные.Колонки.Найти("Склад") <> Неопределено Тогда
			Запрос.Текст = СтрЗаменить(Запрос.Текст, "ЗНАЧЕНИЕ(Справочник.Склады.ПустаяСсылка)", "ИсходныеДанные.Склад");
		КонецЕсли;
		Если ИсходныеДанные.Колонки.Найти("МестоХранения") <> Неопределено Тогда
			Запрос.Текст = СтрЗаменить(Запрос.Текст, "ЗНАЧЕНИЕ(Справочник.МестаХранения.ПустаяСсылка)", "ИсходныеДанные.МестоХранения");
		КонецЕсли;
		Если ИсходныеДанные.Колонки.Найти("ИсточникФинансирования") <> Неопределено Тогда
			Запрос.Текст = СтрЗаменить(Запрос.Текст, "ЗНАЧЕНИЕ(Справочник.ИсточникиФинансирования.ПустаяСсылка)", "ИсходныеДанные.ИсточникФинансирования");
		КонецЕсли;
		Если ИсходныеДанные.Колонки.Найти("Поставщик") <> Неопределено Тогда
			Запрос.Текст = СтрЗаменить(Запрос.Текст, "ЗНАЧЕНИЕ(Справочник.Контрагенты.ПустаяСсылка)", "ИсходныеДанные.Поставщик");
		КонецЕсли;
		
		Если ИсходныеДанные.Колонки.Найти("ДатаДокумента") <> Неопределено Тогда
			Запрос.Текст = СтрЗаменить(Запрос.Текст, """ДатаДокумента""", "ИсходныеДанные.ДатаДокумента");
		КонецЕсли;
		
		Запрос.Текст = СтрЗаменить(Запрос.Текст, """КоличествоВДокументе""",
			?(ИсходныеДанные.Колонки.Найти("КоличествоВДокументе") = Неопределено,
			"ИсходныеДанные.Количество", "ИсходныеДанные.КоличествоВДокументе"));
		Запрос.Текст = СтрЗаменить(Запрос.Текст, """КоличествоЦенников""", "ИсходныеДанные.КоличествоЦенников");
		Запрос.Текст = СтрЗаменить(Запрос.Текст, """КоличествоЭтикеток""", "ИсходныеДанные.КоличествоЭтикеток");
		Запрос.Текст = СтрЗаменить(Запрос.Текст, """КоличествоСтеллажныхКарточек""", "ИсходныеДанные.КоличествоСтеллажныхКарточек");
		Запрос.Текст = СтрЗаменить(Запрос.Текст, """ШаблонЦенника""", "ИсходныеДанные.ШаблонЦенника");
		Запрос.Текст = СтрЗаменить(Запрос.Текст, """ШаблонЭтикетки""", "ИсходныеДанные.ШаблонЭтикетки");
		Запрос.Текст = СтрЗаменить(Запрос.Текст, """ШаблонСтеллажнойКарточки""", "ИсходныеДанные.ШаблонСтеллажнойКарточки");
		
		Запрос.Текст = СтрЗаменить(Запрос.Текст, """Штрихкод""", "ИсходныеДанные.Штрихкод");
		Запрос.Текст = СтрЗаменить(Запрос.Текст, """ЦенаВДокументе""", "ИсходныеДанные.Цена");
		
	КонецЕсли;
	
	СтруктураРезультата.ТаблицаТоваров = Запрос.Выполнить().Выгрузить();
	
	Возврат СтруктураРезультата;
	
КонецФункции

Процедура ПронумероватьТаблицуЗначений(Таблица, ИмяКолонкиНомераСтроки) Экспорт
	
	Таблица.Колонки.Добавить(ИмяКолонкиНомераСтроки, Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(15, 0)));
	
	КоличествоСтрок = Таблица.Количество() - 1;
	Для НомерСтроки = 0 По КоличествоСтрок Цикл
		Таблица[НомерСтроки][ИмяКолонкиНомераСтроки] = НомерСтроки;
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти // ФормированиеЭтикетокИЦенников

////////////////////////////////////////////////////////////////////////////////
// Печать этикеток и ценников
#Область ПечатьЭтикетокИЦенников

// Возвращает коллекцию описаний печатных форм обработки
//
// Параметры:
//  ИдентификаторыПечатныхФорм - Строка - идентификаторы печатных форм обработки, которые нужно включить в коллекцию,
//                                        если пустая строка, то включаются все.
//
// Возвращаемое значение:
//  ТаблицаЗначений - см. УправлениеПечатьюБольничнаяАптека.СоздатьКоллекциюДоступныхПечатныхФорм.
//
Функция ДоступныеПечатныеФормы(ИдентификаторыПечатныхФорм = "") Экспорт
	
	Если ПустаяСтрока(ИдентификаторыПечатныхФорм) Тогда
		ИдентификаторыПечатныхФорм = "Ценники,Этикетки,СтеллажныеКарточки";
	КонецЕсли;
	
	ПечатныеФормы = УправлениеПечатьюБольничнаяАптека.СоздатьКоллекциюДоступныхПечатныхФорм();
	
	Обработчик = "УправлениеПечатьюБольничнаяАптекаКлиент.ПечатьЭтикетокИЦенников";
	СписокИдентификаторов = СтроковыеФункцииКлиентСервер.РазложитьСтрокуВМассивПодстрок(ИдентификаторыПечатныхФорм);
	
	Если СписокИдентификаторов.Найти("Ценники") <> Неопределено Тогда
		ПечатнаяФорма = УправлениеПечатьюБольничнаяАптека.ДобавитьПечатнуюФорму(ПечатныеФормы, "Ценники",, Обработчик);
		ПечатнаяФорма.Представление = НСтр("ru = 'Ценники'");
		ПечатнаяФорма.Порядок = 55;
		ПечатнаяФорма.Роли = "ДобавлениеИзменениеНоменклатуры,ОтчетыИОбработкиПоСкладу,ОтчетыИОбработкиМенеджераПоРозничнойТорговле";
		УправлениеПечатьюБольничнаяАптека.ДобавитьКомандуПечати(ПечатнаяФорма);
	КонецЕсли;
	
	Если СписокИдентификаторов.Найти("Этикетки") <> Неопределено Тогда
		ПечатнаяФорма = УправлениеПечатьюБольничнаяАптека.ДобавитьПечатнуюФорму(ПечатныеФормы, "Этикетки",, Обработчик);
		ПечатнаяФорма.Представление = НСтр("ru = 'Этикетки'");
		ПечатнаяФорма.Порядок = 60;
		ПечатнаяФорма.Роли = "ДобавлениеИзменениеНоменклатуры,ОтчетыИОбработкиПоСкладу,ОтчетыИОбработкиМенеджераПоРозничнойТорговле";
		УправлениеПечатьюБольничнаяАптека.ДобавитьКомандуПечати(ПечатнаяФорма);
	КонецЕсли;
	
	Если СписокИдентификаторов.Найти("СтеллажныеКарточки") <> Неопределено Тогда
		ПечатнаяФорма = УправлениеПечатьюБольничнаяАптека.ДобавитьПечатнуюФорму(ПечатныеФормы, "СтеллажныеКарточки",, Обработчик);
		ПечатнаяФорма.Представление = НСтр("ru = 'Стеллажные карточки'");
		ПечатнаяФорма.Порядок = 65;
		ПечатнаяФорма.Роли = "ДобавлениеИзменениеНоменклатуры,ОтчетыИОбработкиПоСкладу,ОтчетыИОбработкиМенеджераПоРозничнойТорговле,ОтчетыИОбработкиПоДвижениюТоваровВОтделениях";
		УправлениеПечатьюБольничнаяАптека.ДобавитьКомандуПечати(ПечатнаяФорма);
	КонецЕсли;
	
	Возврат ПечатныеФормы;
	
КонецФункции

// Определяет обработку получения настроек печати ценников и этикеток
// и возвращает адрес во временном хранилище, где находятся эти настройки.
//
// Параметры:
//  Идентификатор - Строка - идентификатор печатной формы.
//  ОбъектыПечати - Массив ссылок объектов печати.
//
// Возвращаемое значение:
//  Строка - адрес настроек во временном хранилище для печати ценников и этикеток.
//
Функция ПолучитьДанныеДляПечати(Идентификатор, ОбъектыПечати) Экспорт
	
	Если Идентификатор = "Ценники" Тогда
		Данные = Документы[ОбъектыПечати[0].Метаданные().Имя].ПолучитьДанныеДляПечатиЦенников(ОбъектыПечати);
		Данные.СтруктураДействий.Вставить("УстановитьРежим", "ПечатьЦенников");
		Данные.СтруктураДействий.Вставить("ЗаполнитьКоличествоЦенниковПоДокументу");
	ИначеЕсли Идентификатор = "Этикетки" Тогда
		Данные = Документы[ОбъектыПечати[0].Метаданные().Имя].ПолучитьДанныеДляПечатиЭтикеток(ОбъектыПечати);
		Данные.СтруктураДействий.Вставить("УстановитьРежим", "ПечатьЭтикеток");
		Данные.СтруктураДействий.Вставить("ЗаполнитьКоличествоЭтикетокПоДокументу");
	ИначеЕсли Идентификатор = "СтеллажныеКарточки" Тогда
		Данные = Документы[ОбъектыПечати[0].Метаданные().Имя].ПолучитьДанныеДляПечатиСтеллажныхКарточек(ОбъектыПечати);
		Данные.СтруктураДействий.Вставить("УстановитьРежим", "ПечатьСтеллажныхКарточек");
		Данные.СтруктураДействий.Вставить("ЗаполнитьКоличествоСтеллажныхКарточекПоДокументу");
	Иначе
		ВызватьИсключение СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru='Неизвестный идентификатор печатной формы: %1'"), Идентификатор);
	КонецЕсли;
	
	Если Не Данные.СтруктураДействий.Свойство("ПоказыватьКолонкуКоличествоВДокументе") Тогда
		Данные.СтруктураДействий.Вставить("ПоказыватьКолонкуКоличествоВДокументе", Истина);
	КонецЕсли;
	Данные.СтруктураДействий.Вставить("УстановитьРежимПечатиИзДокумента");
	Данные.СтруктураДействий.Вставить("ЗаполнитьТаблицуТоваров");
	
	Возврат ПоместитьВоВременноеХранилище(Данные);
	
КонецФункции

// Функция формирует табличный документ с ценниками и этикетками.
//
// Возвращаемое значение:
//  Табличный документ - печатная форма с ценниками и этикетками.
//
Функция СформироватьПечатныеФормыЦенниковИЭтикеток(СтруктураНастроек) Экспорт
	
	////////////////////////////////////////////////////////////////////////////////
	// ПОДГОТОВКА СТРУКТУРЫ ДАННЫХ ШАБЛОНА
	
	СтруктураРезультата = ПодготовитьСтруктуруДанных(СтруктураНастроек);
	
	////////////////////////////////////////////////////////////////////////////////
	// ФОРМИРОВАНИЕ ТАБЛИЧНОГО ДОКУМЕНТА
	
	Эталон = Обработки.ПечатьЭтикетокИЦенников.ПолучитьМакет("Эталон");
	КоличествоМиллиметровВПикселе = Эталон.Рисунки.Квадрат100Пикселей.Высота / 100;
	
	// Подготовка коллекции печатных форм.
	КоллекцияПечатныхФорм = Новый ТаблицаЗначений;
	КоллекцияПечатныхФорм.Колонки.Добавить("ИмяМакета");
	КоллекцияПечатныхФорм.Колонки.Добавить("ТабличныйДокумент");
	КоллекцияПечатныхФорм.Колонки.Добавить("ИмяКолонкиКоличество");
	КоллекцияПечатныхФорм.Колонки.Добавить("ИмяКолонкиШаблон");
	КоллекцияПечатныхФорм.Колонки.Добавить("Шаблон");
	
	Для Каждого КлючИЗначение Из СтруктураНастроек.СоответствиеШаблоновИСтруктурыШаблонов Цикл
	
		ПечатнаяФорма = КоллекцияПечатныхФорм.Добавить();
		ПечатнаяФорма.ИмяМакета            = НСтр("ru = 'Этикетка:'") + " " + КлючИЗначение.Ключ;
		ПечатнаяФорма.ИмяКолонкиКоличество = "КоличествоЭтикетокДляПечати";
		ПечатнаяФорма.ИмяКолонкиШаблон     = "ШаблонЭтикеткиДляПечати";
		ПечатнаяФорма.Шаблон = КлючИЗначение.Ключ;
		
		ПечатнаяФорма = КоллекцияПечатныхФорм.Добавить();
		ПечатнаяФорма.ИмяМакета            = НСтр("ru = 'Ценник:'") + " " + КлючИЗначение.Ключ;
		ПечатнаяФорма.ИмяКолонкиКоличество = "КоличествоЦенниковДляПечати";
		ПечатнаяФорма.ИмяКолонкиШаблон     = "ШаблонЦенникаДляПечати";
		ПечатнаяФорма.Шаблон = КлючИЗначение.Ключ;
		
		ПечатнаяФорма = КоллекцияПечатныхФорм.Добавить();
		ПечатнаяФорма.ИмяМакета            = НСтр("ru = 'Стеллажная карточка:'") + " " + КлючИЗначение.Ключ;
		ПечатнаяФорма.ИмяКолонкиКоличество = "КоличествоСтеллажныхКарточекДляПечати";
		ПечатнаяФорма.ИмяКолонкиШаблон     = "ШаблонСтеллажнойКарточкиДляПечати";
		ПечатнаяФорма.Шаблон = КлючИЗначение.Ключ;
		
	КонецЦикла;
	
	Для Каждого ПечатнаяФорма Из КоллекцияПечатныхФорм Цикл
		
		НомерКолонки = 0;
		НомерРяда = 0;
		
		КолонкиТаблицыТоваров = СтруктураРезультата.ТаблицаТоваров.Колонки;
		
		Для Каждого СтрокаТовары Из СтруктураРезультата.ТаблицаТоваров Цикл
			
			Если СтрокаТовары[ПечатнаяФорма.ИмяКолонкиКоличество] > 0 И СтрокаТовары[ПечатнаяФорма.ИмяКолонкиШаблон] = ПечатнаяФорма.Шаблон Тогда
				
				СтруктураШаблона = СтруктураНастроек.СоответствиеШаблоновИСтруктурыШаблонов.Получить(СтрокаТовары[ПечатнаяФорма.ИмяКолонкиШаблон]);
				
				Если ПечатнаяФорма.ТабличныйДокумент = Неопределено Тогда
					ПечатнаяФорма.ТабличныйДокумент = Новый ТабличныйДокумент;
				КонецЕсли;
				
				Область = СтруктураШаблона.МакетЭтикетки.ПолучитьОбласть(СтруктураШаблона.ИмяОбластиПечати);
				
				// Применение настроек табличного документа.
				ЗаполнитьЗначенияСвойств(ПечатнаяФорма.ТабличныйДокумент, СтруктураШаблона.МакетЭтикетки, , "ОбластьПечати");
				
				Для Каждого ПараметрШаблона Из СтруктураШаблона.ПараметрыШаблона Цикл
					Если ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(Область.Параметры, ПараметрШаблона.Значение) Тогда
						НаименованиеКолонки = СтруктураРезультата.СоответствиеПолейСКДКолонкамТаблицыТоваров.Получить(Справочники.ШаблоныЭтикетокИЦенников.ИмяПоляВШаблоне(ПараметрШаблона.Ключ));
						Если НаименованиеКолонки <> Неопределено Тогда
							
							ЗначениеПоля = СтрокаТовары[НаименованиеКолонки];
							Если ТипЗнч(ЗначениеПоля) = Тип("Дата") Тогда
								Колонка = КолонкиТаблицыТоваров.Найти(НаименованиеКолонки);
								КолонкаЧастиДаты = Колонка.ТипЗначения.КвалификаторыДаты.ЧастиДаты;
								Если КолонкаЧастиДаты = ЧастиДаты.Дата Тогда
									ЗначениеПоля = Формат(ЗначениеПоля, "ДЛФ=D");
								ИначеЕсли КолонкаЧастиДаты = ЧастиДаты.Время Тогда
									ЗначениеПоля = Формат(ЗначениеПоля, "ДФ=T");
								КонецЕсли;
							КонецЕсли;
							
							Область.Параметры[ПараметрШаблона.Значение] = ЗначениеПоля;
							
						КонецЕсли;
					КонецЕсли;
				КонецЦикла;
				
				Для Каждого Рисунок Из Область.Рисунки Цикл
					Если СтрНайти(Рисунок.Имя, Справочники.ШаблоныЭтикетокИЦенников.ИмяПараметраШтрихкод()) = 1 Тогда
						
						ЗначениеШтрихкода = СтрокаТовары[СтруктураРезультата.СоответствиеПолейСКДКолонкамТаблицыТоваров.Получить(Справочники.ШаблоныЭтикетокИЦенников.ИмяПараметраШтрихкод())];
						Если ЗначениеЗаполнено(ЗначениеШтрихкода) Тогда
							
							ПараметрыШтрихкода = Новый Структура;
							ПараметрыШтрихкода.Вставить("Ширина",          Рисунок.Ширина / КоличествоМиллиметровВПикселе);
							ПараметрыШтрихкода.Вставить("Высота",          Рисунок.Высота / КоличествоМиллиметровВПикселе);
							ПараметрыШтрихкода.Вставить("Штрихкод",        ЗначениеШтрихкода);
							ПараметрыШтрихкода.Вставить("ТипКода",         СтруктураШаблона.ТипКода);
							ПараметрыШтрихкода.Вставить("ОтображатьТекст", СтруктураШаблона.ОтображатьТекст);
							ПараметрыШтрихкода.Вставить("РазмерШрифта",    СтруктураШаблона.РазмерШрифта);
							
							Рисунок.Картинка = МенеджерОборудованияВызовСервера.ПолучитьКартинкуШтрихкода(ПараметрыШтрихкода);
							
						КонецЕсли;
						
					ИначеЕсли СтрНайти(Рисунок.Имя, "ЗнакВалюты") = 1 Тогда
						ЗначениеКодаВалюты = СтрокаТовары[СтруктураРезультата.СоответствиеПолейСКДКолонкамТаблицыТоваров.Получить(Справочники.ШаблоныЭтикетокИЦенников.ИмяПараметраКодВалюты())];
						Если Метаданные.Справочники.ШаблоныЭтикетокИЦенников.Макеты.Найти("ЗнакВалюты" + ЗначениеКодаВалюты) <> Неопределено Тогда
							Рисунок.Картинка = Новый Картинка(Справочники.ШаблоныЭтикетокИЦенников.ПолучитьМакет("ЗнакВалюты" + ЗначениеКодаВалюты), Истина);
						Иначе
							Рисунок.Картинка = Новый Картинка;
						КонецЕсли;
					КонецЕсли;
				КонецЦикла;
				
				Для Инд = 1 По СтрокаТовары[ПечатнаяФорма.ИмяКолонкиКоличество] Цикл // Цикл по количеству экземпляров
					
					НомерКолонки = НомерКолонки + 1;
					
					Если НомерКолонки = 1 Тогда
						
						НомерРяда = НомерРяда + 1;
						
						ПечатнаяФорма.ТабличныйДокумент.Вывести(Область);
						
					Иначе
						
						ПечатнаяФорма.ТабличныйДокумент.Присоединить(Область);
						
					КонецЕсли;
					
					Если НомерКолонки = СтруктураШаблона.КоличествоПоГоризонтали И НомерРяда = СтруктураШаблона.КоличествоПоВертикали Тогда
						
						НомерРяда    = 0;
						НомерКолонки = 0;
						
						ПечатнаяФорма.ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
						
					ИначеЕсли НомерКолонки = СтруктураШаблона.КоличествоПоГоризонтали Тогда
						
						НомерКолонки = 0;
						
					КонецЕсли;
					
				КонецЦикла; // Цикл по количеству экземпляров
				
			КонецЕсли;
			
		КонецЦикла; // Цикл по строкам таблицы товаров
		
	КонецЦикла;
	
	МассивСтрокДляУдаления = Новый Массив;
	Для Каждого ПечатнаяФорма Из КоллекцияПечатныхФорм Цикл
		Если ПечатнаяФорма.ТабличныйДокумент = Неопределено Тогда
			МассивСтрокДляУдаления.Добавить(ПечатнаяФорма);
		КонецЕсли;
	КонецЦикла;
	Для Каждого ПечатнаяФорма Из МассивСтрокДляУдаления Цикл
		КоллекцияПечатныхФорм.Удалить(ПечатнаяФорма);
	КонецЦикла;
	
	Возврат КоллекцияПечатныхФорм;
	
КонецФункции

// Процедура печати документа.
//
Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
	ИсходныеДанные = ПолучитьИзВременногоХранилища(ПараметрыПечати.Товары);
	
	НужноПечататьЭтикетки = Ложь;
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "ЭтикеткаТовары") Тогда
		НужноПечататьЭтикетки = Истина;
		КоллекцияПечатныхФорм.Удалить(КоллекцияПечатныхФорм.Найти(ВРег("ЭтикеткаТовары"), "ИмяВРЕГ"));
	КонецЕсли;
	
	НужноПечататьЦенники = Ложь;
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "ЦенникТовары") Тогда
		НужноПечататьЦенники = Истина;
		КоллекцияПечатныхФорм.Удалить(КоллекцияПечатныхФорм.Найти(ВРег("ЦенникТовары"), "ИмяВРЕГ"));
	КонецЕсли;
	
	НужноПечататьСтеллажныеКарточки = Ложь;
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "СтеллажнаяКарточкаТовары") Тогда
		НужноПечататьСтеллажныеКарточки = Истина;
		КоллекцияПечатныхФорм.Удалить(КоллекцияПечатныхФорм.Найти(ВРег("СтеллажнаяКарточкаТовары"), "ИмяВРЕГ"));
	КонецЕсли;
	
	СтруктураНастроек = ПолучитьПустуюСтруктуруНастроек();
	СтруктураНастроек.ОбязательныеПоля.Добавить("КоличествоЦенниковДляПечати");
	СтруктураНастроек.ОбязательныеПоля.Добавить("КоличествоЭтикетокДляПечати");
	СтруктураНастроек.ОбязательныеПоля.Добавить("КоличествоСтеллажныхКарточекДляПечати");
	СтруктураНастроек.ОбязательныеПоля.Добавить("КоличествоВДокументеДляПечати");
	СтруктураНастроек.ОбязательныеПоля.Добавить("ШаблонЦенникаДляПечати");
	СтруктураНастроек.ОбязательныеПоля.Добавить("ШаблонЭтикеткиДляПечати");
	СтруктураНастроек.ОбязательныеПоля.Добавить("ШаблонСтеллажнойКарточкиДляПечати");
	СтруктураНастроек.ОбязательныеПоля.Добавить("Номенклатура");
	СтруктураНастроек.ОбязательныеПоля.Добавить("СерияНоменклатуры");
	СтруктураНастроек.ОбязательныеПоля.Добавить("Партия");
	СтруктураНастроек.ОбязательныеПоля.Добавить("Упаковка");
	СтруктураНастроек.ОбязательныеПоля.Добавить("ДатаПоследнегоИзмененияЦены");
	СтруктураНастроек.ОбязательныеПоля.Добавить("ДатаДокумента");
	
	СтруктураНастроек.ОбязательныеПоля.Добавить("Организация");
	СтруктураНастроек.ОбязательныеПоля.Добавить("Склад");
	СтруктураНастроек.ОбязательныеПоля.Добавить("МестоХранения");
	СтруктураНастроек.ОбязательныеПоля.Добавить("Поставщик");
	
	СтруктураНастроек.ИмяМакетаСхемыКомпоновкиДанных = "ПоляШаблонаПечать";
	
	// Собираем используемые поля из шаблонов.
	СоответствиеШаблонов = Новый Соответствие;
	Для Каждого СтрокаТЧ Из ИсходныеДанные Цикл
		Если НужноПечататьЭтикетки И ЗначениеЗаполнено(СтрокаТЧ.ШаблонЭтикетки) И СтрокаТЧ.КоличествоЭтикеток > 0 Тогда
			СоответствиеШаблонов.Вставить(СтрокаТЧ.ШаблонЭтикетки);
		КонецЕсли;
		Если НужноПечататьЦенники И ЗначениеЗаполнено(СтрокаТЧ.ШаблонЦенника) И СтрокаТЧ.КоличествоЦенников > 0 Тогда
			СоответствиеШаблонов.Вставить(СтрокаТЧ.ШаблонЦенника);
		КонецЕсли;
		Если НужноПечататьСтеллажныеКарточки И ЗначениеЗаполнено(СтрокаТЧ.ШаблонСтеллажнойКарточки) И СтрокаТЧ.КоличествоСтеллажныхКарточек > 0 Тогда
			СоответствиеШаблонов.Вставить(СтрокаТЧ.ШаблонСтеллажнойКарточки);
		КонецЕсли;
	КонецЦикла;
	Если ПараметрыПечати.Свойство("СтруктураМакетаШаблона") И ЗначениеЗаполнено(ПараметрыПечати.СтруктураМакетаШаблона) Тогда
		СоответствиеШаблонов.Вставить(Справочники.ШаблоныЭтикетокИЦенников.ПустаяСсылка());
	КонецЕсли;
	
	// Заполняем коллекцию обязательных полей и формируем соответствие шаблонов.
	Для Каждого КлючИЗначение Из СоответствиеШаблонов Цикл
		
		ШаблонЭтикетокИЦенников = КлючИЗначение.Ключ;
		
		Если ЗначениеЗаполнено(ШаблонЭтикетокИЦенников) Тогда
			СтруктураШаблона = КлючИЗначение.Ключ.Шаблон.Получить();
		Иначе
			СтруктураШаблона = ПараметрыПечати.СтруктураМакетаШаблона;
		КонецЕсли;
		
		// Структура шаблонов.
		СтруктураНастроек.СоответствиеШаблоновИСтруктурыШаблонов.Вставить(КлючИЗначение.Ключ, СтруктураШаблона);
		
		// Добавляем в массив обязательных полей поля, присутствующие в печатной форме ценника.
		Для Каждого Элемент Из СтруктураШаблона.ПараметрыШаблона Цикл
			СтруктураНастроек.ОбязательныеПоля.Добавить(Элемент.Ключ);
		КонецЦикла;
		
	КонецЦикла;
	
	СтруктураНастроек.ПараметрыДанных.Вставить("ВидЦены", ПараметрыПечати.ВидЦены);
	СтруктураНастроек.ИсходныеДанные = ИсходныеДанные;
	
	// Вывод табличных документов в коллекцию.
	КоллекцияПечатныхФормВнутренняя = СформироватьПечатныеФормыЦенниковИЭтикеток(СтруктураНастроек);
	КоллекцияПечатныхФорм.Очистить();
	Для Каждого ПечатнаяФорма Из КоллекцияПечатныхФормВнутренняя Цикл
		
		НоваяФорма = КоллекцияПечатныхФорм.Добавить();
		НоваяФорма.ИмяМакета         = ПечатнаяФорма.ИмяМакета;
		НоваяФорма.СинонимМакета     = ПечатнаяФорма.ИмяМакета;
		НоваяФорма.ИмяВРЕГ           = ВРег(ПечатнаяФорма.ИмяМакета);
		НоваяФорма.ТабличныйДокумент = ПечатнаяФорма.ТабличныйДокумент;
		НоваяФорма.Экземпляров       = 1;
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти // ПечатьЭтикетокИЦенников

#КонецОбласти // ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЙ ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область СлужебныйПрограммныйИнтерфейс

Функция ПолучитьПустуюСтруктуруРезультата() Экспорт
	
	Структура = Новый Структура;
	Структура.Вставить("ТаблицаТоваров"                            , Неопределено);
	Структура.Вставить("СоответствиеПолейСКДКолонкамТаблицыТоваров", Новый Соответствие);
	
	Возврат Структура;
	
КонецФункции // ПолучитьПустуюСтруктуруРезультата()

Функция ПолучитьПустуюСтруктуруНастроек() Экспорт
	
	СтруктураНастроек = Новый Структура;
	СтруктураНастроек.Вставить("ИсходныеДанные"                        , Неопределено); // Таблица с произвольными данными
	СтруктураНастроек.Вставить("ОбязательныеПоля"                      , Новый Массив);
	СтруктураНастроек.Вставить("СоответствиеШаблоновИСтруктурыШаблонов", Новый Соответствие);
	СтруктураНастроек.Вставить("ПараметрыДанных"                       , Новый Структура);
	СтруктураНастроек.Вставить("КомпоновщикНастроек"                   , Неопределено); // Отбор
	СтруктураНастроек.Вставить("ИмяМакетаСхемыКомпоновкиДанных"        , Неопределено);
	
	Возврат СтруктураНастроек;
	
КонецФункции

#КонецОбласти // СлужебныйПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

Функция УстановитьЗначениеПараметраСКД(КомпоновщикНастроек, ИмяПараметра, ЗначениеПараметра, ИспользоватьНеЗаполненный = Истина)
	
	ПараметрУстановлен = Ложь;
	
	ПараметрВидЦены = Новый ПараметрКомпоновкиДанных(ИмяПараметра);
	ЗначениеПараметраВидЦены = КомпоновщикНастроек.Настройки.ПараметрыДанных.НайтиЗначениеПараметра(ПараметрВидЦены);
	Если ЗначениеПараметраВидЦены <> Неопределено Тогда
		
		ЗначениеПараметраВидЦены.Значение = ЗначениеПараметра;
		ЗначениеПараметраВидЦены.Использование = ?(ИспользоватьНеЗаполненный, Истина, ЗначениеЗаполнено(ЗначениеПараметраВидЦены.Значение));
		
		ПараметрУстановлен = Истина;
		
	КонецЕсли;
	
	Возврат ПараметрУстановлен;
	
КонецФункции

Функция СвернутьТаблицуЗначенийПоРеквизиту(ТаблицаРеквизитыДокументов, ИмяРеквизита) Экспорт
	
	Таблица = ТаблицаРеквизитыДокументов.Скопировать();
	Таблица.Свернуть(ИмяРеквизита);
	Возврат Таблица;
	
КонецФункции

#КонецОбласти // СлужебныеПроцедурыИФункции

#КонецЕсли

